# Periodic approximations and spectra of the Koopman operator

This public repository contains matlab code which allows one to compute Koopman spectra of certain measure-preserving transformations. The main source code is to be found in the directory core. Example scripts to use this code are provided in the scripts folder. Please note that this toolbox is dependent on other external toolboxes. These are included in the external-toolboxes directory. Make sure to include them in your path when running the scripts.

For more background on the theory behind the toolbox, see the papers:

* "On the approximation of koopman spectra for measure preserving transformations"
N Govindarajan, R Mohr, S Chandrasekaran, I Mezic - SIAM Journal on Applied Dynamical Systems, 2019
* "On the approximation of koopman spectra for measure preserving flows"
N Govindarajan, R Mohr, S Chandrasekaran, I Mezic - to appear in SIAM Journal on Applied Dynamical Systems
