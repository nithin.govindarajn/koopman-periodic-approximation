function [] = quadruplepoincare(N,M)


figure
hold on;
for l=1:N
    
    x = [rand(2,1);
          0];
    X = zeros(3,M);
    for k=1:M
         [~,XOUT] = ode45(@quadruplepoincareODE, [0 1],x);
         x = transpose(XOUT(end,:));
        X(:,k) = x;
    end
    plot(X(1,:),X(2,:),'k.','MarkerSize',0.1)
    
end
xlabel('x_1');ylabel('x_2')
axis square
axis([0 1 0 1])

end


function [ ydot ] = quadruplepoincareODE(t,y)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

epsilon = 0.05;
A = 1/(2*pi);

ydot =zeros(3,1);

f1 = 4*epsilon*sin(2*pi*y(3))*y(1)^2 + (2-4*epsilon*sin(2*pi*y(3)))*y(1);
f2 = 4*epsilon*sin(2*pi*y(3))*y(2)^2 + (2-4*epsilon*sin(2*pi*y(3)))*y(2);

f1der = 8*epsilon*sin(2*pi*y(3))*y(1) + (2-4*epsilon*sin(2*pi*y(3)));
f2der = 8*epsilon*sin(2*pi*y(3))*y(2) + (2-4*epsilon*sin(2*pi*y(3)));

ydot(1) = pi*A*sin(pi*f1)*cos(pi*f2)*f1der;
ydot(2) = -pi*A*cos(pi*f1)*sin(pi*f2)*f2der;
ydot(3) = 1;


end
