clc;
clear all;
close all;

global T dimension N


alpha = 1/3;
[ T, dimension ] = anzaitypemap(alpha);
N =1000;              % Spatial discretization level
alpha = 2*pi/500;       % Spectral resolution 

% compute periodic approximation
[DiscreteMap] = Periodicapproximation();


%%

g = @(x)(0.05*exp(1i*2*pi*(1*x(:,1))) + 0.05*exp(1i*2*pi*(2*x(:,1))) + 0.05*exp(1i*2*pi*(3*x(:,1))) +   exp(1i*2*pi*(1*x(:,2))) + 0.5*exp(1i*2*pi*(1*x(:,1)+1*x(:,2))));


%% Analytic solution of spectra

% grid
no_ofevaluations = floor(1.5*(2*pi/alpha));
theta = (0:(2*pi/no_ofevaluations):((no_ofevaluations-1)/no_ofevaluations)*2*pi);

%continuous spectrum part
rho = @(theta)((5/4+cos(theta))/(2*pi));
EnergyDensityCont_analytic = rho(theta);

%discrete part of the spectrum
c1 = 0.05^2;
c2 = 0.05^2;
c3 = 0.05^2;
eigfreq_thetas = [0; 2*pi/3; 4*pi/3]; c = [c1;c2;c3];


EnergyDensityDiscrete_analytic = func_mollify(eigfreq_thetas, c, theta, alpha );

% total spectrum 
EnergyDensity_analytic = EnergyDensityDiscrete_analytic + EnergyDensityCont_analytic;

theta = [theta,theta(1)];
EnergyDensity_analytic = [EnergyDensity_analytic,EnergyDensity_analytic(1)];



%%


[theta, energydensity] = compute_Koopmanmodeenergy_mollify( alpha, g, DiscreteMap );


z = energydensity;
x = real(exp(1i*theta)); 
y = imag(exp(1i*theta));


plot3(x,y,EnergyDensity_analytic,'Color','r','LineWidth',1)
hold on
plot3(x,y,z,'Color','k','LineWidth',1)

plot3([-1.8 1.8],[0 0], [0 0],'k')
plot3([0 0],[-1.8 1.8], [0 0],'k')
plot3(cos(0:0.01:2*pi), sin(0:0.01:2*pi), 0*cos((0:0.01:2*pi)),'k-.' )
text(1.1,0,0,'\theta = 0')
text(0,1.1,0,'\theta = \pi/2')
text(-1.4,0,0,'\theta = -\pi \equiv \pi')
text(0,-1.3,0,'\theta = -\pi/2')
text(1.9,0,0,'Re \lambda')
text(0,1.9,0,'Im \lambda')

grid off
axis off
view(20,35)
axis([-2 2 -2 2 0 0.6])
h = title(['$\tilde{n} =$',num2str(N) ],'Interpreter','LaTex');
set(h,'position',[0 0])

saveas(gcf,['anzai',num2str(N)],'epsc')
%close all;

%% II. Compute projections
close all


Projrange = [-0.01,0.01];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
text(-10, -10,'AAA')%'$D=[-0.01,0.01]$');
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 0.12,'\bf $D=[-0.01, 0.01]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')

%%

Projrange = [2*pi/3-0.01,2*pi/3+0.01];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar


ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 0.12,'\bf $D=\left[\frac{2\pi}{3}-0.01,\frac{2\pi}{3}+ 0.01\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')



%%

Projrange = [pi/2-(0.5/1)^5*pi,pi/2+(0.5/1)^5*pi]; %[2*pi*4/6-0.01,2*pi*4/6+0.01];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400])
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 0.12,'\bf $D=\left[\left(1-\frac{1}{32}\right) \frac{\pi}{2},\left(1+\frac{1}{32}\right) \frac{\pi}{2}\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


%text(0.5, 0.12,'$D=\left[\left(1-\frac{1}{16}\right)\frac{\pi}{2}, \left(1+\frac{1}{16}1right)\frac{\pi}{2} \right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')

