clc
clear all 
close all

global T dimension N


%% Select a map or create a map of your own

omega1 = 2*pi/3;
omega2 = pi/2;


T = Integrablemapon3sphere(omega1, omega2)

%% Core part of the algorithm: Periodic approximation


%%%% SETTINGS %%%%
N = 200;

x_grid = eq_point_set(3,N^3); x_grid= x_grid';


%%
% periodic approximation

%%%%  STEP 1: Construct the discrete map %%%%

% SNAPSHOT PAIRS
Xdata = x_grid; Ydata = T(x_grid);
graphsize = length(Xdata(:,1));

edge_sink = rangesearch(Ydata, Xdata, 0.025);

%%
edge_source = cell(size(edge_sink));
for j=1: length(edge_source);
   edge_source{j} = j*ones(length(edge_sink{j}),1); 
end
edge_source = cell2mat(edge_source); edge_sink= transpose(cell2mat(edge_sink'));

%%


[ Un ] = bipartitematch(edge_source, edge_sink, graphsize);


tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = Xdata;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));






%% Pick an observable

% Observable
l = 3; % degree
m = 2; % order

g = @(x)( x(:,1).^2 + x(:,2).^2 + sphericalharmonic_obs(2,1,x(:,1:3)) + sphericalharmonic_obs(l,m,x(:,2:4)));






%% RESULTS:
%%% There are a couple things you can do %%%
% I.   You can simulate the observable
% II. You can compute projections of an observable
% III.  You can generate a spectral energy density plot


%% I. simulate observable propagation


% Propagate observable
K = 1;
xsim = DiscreteMap.x;
Ug = g(xsim);
Ugdiscrete = g(DiscreteMap.x);
for k=1:K
    %One step propagation of the Koopman operator
    xsim = T(xsim); Ug = g(xsim);
    Ugdiscrete = PropagateObservable( DiscreteMap.Un, Ugdiscrete);
end



%upper and lower hemisphere points
up_hemisphere = DiscreteMap.x(:,4)>0;
lo_hemisphere = DiscreteMap.x(:,4)<0;



[xi,yi,zi] = meshgrid(-1:0.01:1, -1:0.01:1, -1:0.01:1);

figure(1)
subplot(1,3,1)
%plotting grid
wi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),DiscreteMap.x(up_hemisphere,3),...
                   Ug(up_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('upper hemisphere U^k g')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(1,3,2) 
wi2 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),DiscreteMap.x(up_hemisphere,3),...
                   Ugdiscrete(up_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi2,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);      
title('upper hemisphere U^k g_n')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(1,3,3) 
s = slice(xi,yi,zi,abs(wi1-wi2),[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);
title('error')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end



figure(2)
subplot(1,3,1)
%plotting grid
wi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),DiscreteMap.x(lo_hemisphere,3),...
                   Ug(lo_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('lower hemisphere U^k g')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(1,3,2) 
wi2 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),DiscreteMap.x(lo_hemisphere,3),...
                   Ugdiscrete(lo_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi2,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);      
title('lower hemisphere U^k g_n')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(1,3,3) 
s = slice(xi,yi,zi,abs(wi1-wi2),[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);
title('error')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end



%% II. Compute projections


% Projection
Projrange = [-0.05,+0.05];
tic
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );
toc
Phi_real = real(Phi); 
Phi_imag = imag(Phi);

%upper and lower hemisphere points
up_hemisphere = DiscreteMap.x(:,4)>0;
lo_hemisphere = DiscreteMap.x(:,4)<0;



[xi,yi,zi] = meshgrid(-1:0.01:1, -1:0.01:1, -1:0.01:1);

figure(1)
subplot(2,2,1)
%plotting grid
wi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),DiscreteMap.x(up_hemisphere,3),...
                   Phi_real(up_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('real part (upper hemisphere)')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(2,2,2)
%plotting grid
wi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),DiscreteMap.x(lo_hemisphere,3),...
                   Phi_real(lo_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('real part (lower hemisphere)')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(2,2,3)
%plotting grid
wi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),DiscreteMap.x(up_hemisphere,3),...
                   Phi_imag(up_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('imaginary part (upper hemisphere)')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end
subplot(2,2,4)
%plotting grid
wi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),DiscreteMap.x(lo_hemisphere,3),...
                   Phi_imag(lo_hemisphere),xi,yi,zi);
s = slice(xi,yi,zi,wi1,[-1:0.5:1],[-1:0.5:1],[-1:0.5:1]);    
title('imaginary part (lower hemisphere)')
view(70,30)
shading interp
axis equal off
for n=1:length(s)
    set(s(n),'alphadata',get(s(n),'cdata'),'facealpha','flat')
end



%% III. spectral density function and projections


alpha = 500;       % Spectral resolution 


%%%% Spectral content %%%%
tic
[EnergyDensity, theta, ranges ] = compute_Koopmanmodeenergy( alpha, g, DiscreteMap );
toc


plotKoopmanmodeenergy( ranges, EnergyDensity,alpha, [])
view(70,30)
  



