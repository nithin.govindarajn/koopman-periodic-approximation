clc;
clear all;
close all;



%% Simulation parameters

% Duffing osccilator model parameters
beta = -1;
alpha = 1;


%%%% Periodic approximation discretization  %%%%
tau = 0.02;
spacing = 0.001;

% tau = 0.05;
% spacing = 0.01;

%%%% Observable %%%%
domain_range = 2;  % domain of interest
%specified in function


%%%% Spectral plots %%%%
Proj_range = [2, 2.5];
spect_bandwith = 15;
spect_res = spect_bandwith/400; %


% -0.3 to 0.3
% 2 to 2.5
% 4.5 to 5
% 7.5 to 8

%% Perform calculations

% Run main routine
tic
[ Q,P, Proj_observable, omega, energydensity] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc

%% Spectral density plot

figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
%axis equal
axis([0 spect_bandwith min(energydensity) max(energydensity)])



%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(Q, P, real(Proj_observable)  ) ;
axis([-domain_range domain_range -domain_range domain_range])
axis square
shading interp
hold off;
xlabel('q','FontSize',18); ylabel('p','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(Q, P, imag(Proj_observable)  ) ;
axis([-domain_range domain_range -domain_range domain_range])
axis square
shading interp
hold off;
xlabel('q','FontSize',18); ylabel('p','FontSize',18)
title('imag')
view(0,90)
colorbar