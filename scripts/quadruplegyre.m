%% Quadruple gyre
clc
close all
clear all

global A epsilon Omega

% Model parameters
epsilon = 0.05;
Omega = 1;
A = 1/(2*pi);


%% Periodic approximation

%
N =700;  
dt = 1/100;




[DiscreteMap] = Periodicapproximation_quadruplegyre(N,dt);

%% Observable

%g = @(x)( 1 ./ (1+ (x(:,1)-0.5).^2 + (x(:,2)-0.5).^2 ) );
g = @(x)(4*bumpfunction(x(:,1:2),[0.5, 0.5], 0.4) +....
      1i*( sin(4*pi * x(:,1) ).* sin(4*pi * x(:,2) )  )  );

%g = @(x)( exp(2*pi*1i*x(:,3)) );


%% Spectral density plot

bandwith =20;


[omega, energydensity] = compute_Koopmanmodeenergy_mollify_flow(0.01, g, DiscreteMap, bandwith,dt );

figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
axis([0 bandwith min(energydensity) 1.2*max(energydensity)])



%% Spectral Projection

Projrange = [6.5,10];
[ Phi] = ProjObservable_flow( Projrange, g, DiscreteMap,dt );

N = DiscreteMap.N;
Nt = DiscreteMap.Nt;

%%

X1 = reshape(DiscreteMap.x(:,1),[N N Nt]);
X2 = reshape(DiscreteMap.x(:,2),[N N Nt]);
X3 = reshape(DiscreteMap.x(:,3),[N N Nt]);
PHI = reshape(Phi,[N N Nt]);


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(X1(:,:,1), X2(:,:,1), real(PHI(:,:,1))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(X1(:,:,1), X2(:,:,1), imag(PHI(:,:,1))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'65to100proj1','epsc')


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(X1(:,:,26), X2(:,:,26), real(PHI(:,:,26))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(X1(:,:,26), X2(:,:,26), imag(PHI(:,:,26))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'65to100proj2','epsc')

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(X1(:,:,51), X2(:,:,51), real(PHI(:,:,51))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(X1(:,:,51), X2(:,:,51), imag(PHI(:,:,51))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'65to100proj3','epsc')


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(X1(:,:,76), X2(:,:,76), real(PHI(:,:,76))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(X1(:,:,76), X2(:,:,76), imag(PHI(:,:,76))  ) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'65to100proj4','epsc')

