clc;
clear all;
close all;



%% Simulation parameters

% Duffing osccilator model parameters
alpha = 1;


%%%% Periodic approximation discretization  %%%%
tau = 0.025;
spacing = 0.001;

%%%% Observable %%%%
domain_range = 2;  % domain of interest
%specified in function


%%%% Spectral plots %%%%
Proj_range = [-0.25, 0.25];
spect_bandwith = 25;
spect_res = spect_bandwith/200; %




%% Perform calculations

% Run main routine
tic
beta = -0.05;
Proj_range = [-0.25, 0.25];
[ Q1,P1, Proj_observable1, omega1, energydensity1] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc

tic
beta = 0;
Proj_range = [-0.25, 0.25];
[ Q2,P2, Proj_observable2, omega2, energydensity2] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc


tic
beta = 0.05;
Proj_range = [-0.25, 0.25];
[ Q3,P3, Proj_observable3, omega3, energydensity3] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc

%% Spectral density plot

figure('position',[100 100 850 400]);
semilogy(omega1,(energydensity1),'k','LineWidth',1.5)
hold on
semilogy(omega2,(energydensity2),'Color',[0.5 0.5 0.5],'LineWidth',1)
hold on
semilogy(omega3,(energydensity3),'k-.','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
legend('a=1, b=-0.05','a=1, b=0','a=1, b=0.05')
%axis equal
%axis([0 spect_bandwith min(energydensity) max(energydensity)])
saveas(gcf,'spectdensityduffingbifurc','epsc')

% %% Perform calculations
% 
% % Run main routine
% tic
% beta = -1;
% Proj_range = [-0.25, 0.25];
% [ Q1,P1, Proj_observable1, omega1, energydensity1] = Duffing_mex(alpha,beta,tau, spacing,...
%     Proj_range, domain_range, spect_res, spect_bandwith);
% toc
% 
% tic
% beta = 1;
% Proj_range = [-0.25, 0.25];
% [ Q2,P2, Proj_observable2, omega2, energydensity2] = Duffing_mex(alpha,beta,tau, spacing,...
%     Proj_range, domain_range, spect_res, spect_bandwith);
% toc
% 
% %% Spectral density plot
% 
% figure('position',[100 100 850 400]);
% semilogy(omega1,(energydensity1),'k','LineWidth',1.5)
% hold on
% semilogy(omega2,(energydensity2),'k-.','LineWidth',1.5)
% xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
% legend('a=1, b=-1','a=1, b=1')
% %axis equal
% %axis([0 spect_bandwith min(energydensity) max(energydensity)])
% saveas(gcf,'spectdensityduffing','epsc')



%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(Q1, P1, real(Proj_observable1)  ) ;
axis([-1.5 1.5 -1.5 1.5])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(Q1, P1, imag(Proj_observable1)  ) ;
axis([-1.5 1.5 -1.5 1.5])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'duffingbetaneg1','epsc')



%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(Q2, P2, real(Proj_observable2)  ) ;
axis([-1.5 1.5 -1.5 1.5])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(Q2, P2, imag(Proj_observable2)  ) ;
axis([-1.5 1.5 -1.5 1.5])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
saveas(gcf,'duffingbetapos1','epsc')