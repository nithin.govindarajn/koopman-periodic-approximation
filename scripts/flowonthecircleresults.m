clc;
clear all;
close all;

global T tau dimension N 

%%%% SETTINGS %%%%
omega = 1/3;
tau = 0.01;                   % temporal dicscretization
N =1000000;                    % spatial discretization


[ T, dimension ] = flowonthecircle(tau,omega);

% compute periodic approximation
[DiscreteMap] = Periodicapproximation();

%% Analytic solution of spectra

% Observable
g = @(x)( min(x,1-x)  );


% To be computed...


%%

%window
spect_bandwith = 100;
spect_res = spect_bandwith/400; 

[omega, energydensity] = compute_Koopmanmodeenergy_mollify_flow(spect_res, g, DiscreteMap, spect_bandwith );


%% Spectral density plot

figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
%axis equal
axis([0 spect_bandwith max([min(energydensity), 1E-12]) max(energydensity)])

