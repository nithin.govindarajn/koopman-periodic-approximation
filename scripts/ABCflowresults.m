clc;
clear all;
close all;

global tau N


%% Periodic approximation

% ABC flow parameters
A = sqrt(3)/(2*pi);
B = sqrt(2)/(2*pi);
C = 1/(2*pi);

%%%% SETTINGS %%%%
tau = 0.025;            % temporal discretization
N =400;              % Spatial discretization level

% Perform the periodic approximation
[DiscreteMap] = Periodicapproximation_ABC(A,B,C);





%% Pick an observable

% 3-dimensional observable
 g = @(x)( exp(1i*4*pi*x(:,2)) + 2*exp(1i*6*pi*x(:,1)) +  exp(1i*2*pi*x(:,3))   );%



%% Spectral density plot


spect_bandwith = 10;
spect_res = 0.01; %

[omega, energydensity] = compute_Koopmanmodeenergy_mollify_flow( spect_res, g, DiscreteMap, spect_bandwith,tau );

figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
axis([0 spect_bandwith min(energydensity) max(energydensity)])
saveas(gcf,'spectdensitypendulum','epsc')

%% Spectral projection plot
 
Proj_range = [7.36, 7.56];
tic
[ Phi] = ProjObservable( Proj_range, g, DiscreteMap );
toc



%% Results: spectral density plot


figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
%axis equal
axis([0 spect_bandwith min(energydensity) max(energydensity)])



%% Results: projections

X1 = reshape(DiscreteMap.x(:,1),[N N N]);
X2 = reshape(DiscreteMap.x(:,2),[N N N]);
X3 = reshape(DiscreteMap.x(:,3),[N N N]);
PHI = reshape(Phi, [N N N]);


%% 

figure('position',[100 100 850 400]);
subplot(1,2,1)
hold on
slice(X1,X2,X3,reshape(real(Phi), [N N N]),[0.2],[],[]);
slice(X1,X2,X3,reshape(real(Phi), [N N N]),[],[0.6],[]);
slice(X1,X2,X3,reshape(real(Phi), [N N N]),[],[],[0.4]);
axis([0 1 0 1 0 1])
shading interp
grid off
hold off;
xlabel('x','FontSize',18); ylabel('y','FontSize',18);zlabel('z','FontSize',18)
title('real')
view(30,40)
colorbar
subplot(1,2,2)
hold on
slice(X1,X2,X3,reshape(imag(Phi), [N N N]),[0.2],[],[]);
slice(X1,X2,X3,reshape(imag(Phi), [N N N]),[],[0.6],[]);
slice(X1,X2,X3,reshape(imag(Phi), [N N N]),[],[],[0.4]);
axis([0 1 0 1 0 1])
shading interp
grid off
hold off;
xlabel('x','FontSize',18); ylabel('y','FontSize',18);zlabel('z','FontSize',18)
title('imag')
view(30,40)
colorbar
