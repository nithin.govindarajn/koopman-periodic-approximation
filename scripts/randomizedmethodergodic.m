clc
clear all
close all
%rangesearch


%%

[ T, dimension ] = Catmap();

N = 50000;
x = rand(1,2);

X = zeros(N,2);
for j = 1:1:N
  
    X(j,:) = x;
    x= T(x);
    
end

hold on
plot(X(:,1),X(:,2), '.k' )
%voronoi(X(:,1),X(:,2),'k')


%%%%  STEP 1: Construct the discrete map %%%%

% SNAPSHOT PAIRS
Xdata = X(1:N-1,:); Ydata = X(2:N,:);
graphsize = length(Xdata(:,1));

edge_sink = rangesearch(Ydata, Xdata, 0.007);

edge_source = cell(size(edge_sink));
for j=1: length(edge_source);
   edge_source{j} = j*ones(length(edge_sink{j}),1); 
end
edge_source = cell2mat(edge_source); edge_sink= transpose(cell2mat(edge_sink'));


[ Un ] = bipartitematch([edge_source edge_sink], graphsize);





tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = Xdata;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));

%% Pick observable
%g = @(x)( cos(6*pi*x(:,1)) +  sin(2*pi*x(:,2)) );
g = @(x)( exp(1i*2*pi*(2*x(:,1)+1*x(:,2))) + 0.5*exp(1i*2*pi*(5*x(:,1)+3*x(:,2)))+ 0.25*exp(1i*2*pi*(13*x(:,1)+8*x(:,2))));


%% simulate observable

gdiscrete = g(DiscreteMap.x);

figure
[xi,yi] = meshgrid(0:0.01:1, 0:0.01:1);
zi = griddata(DiscreteMap.x(:,1),DiscreteMap.x(:,2),gdiscrete,xi,yi);
surf(xi,yi,zi);
shading interp
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar

gdiscrete


gdiscrete = PropagateObservable( Un, gdiscrete );

figure
[xi,yi] = meshgrid(0:0.01:1, 0:0.01:1);
zi = griddata(DiscreteMap.x(:,1),DiscreteMap.x(:,2),gdiscrete,xi,yi);
surf(xi,yi,zi);
shading interp
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar



%% III. spectral density function and projections 

alpha = 500;       % Spectral resolution 



%%%% Spectral content %%%%
tic
[EnergyDensity, theta, ranges ] = compute_Koopmanmodeenergy( alpha, g, DiscreteMap );
toc

%%


plotKoopmanmodeenergy( ranges, EnergyDensity,alpha, [])
title({['Spatial discretization level N=', num2str(N)]; ...
      ['Spectral resolution \alpha =', num2str(alpha)]});
  








