clc;
clear all;
close all;

global tau N


%% Select a map or create a map of your own

% [ T, dimension ] = rotationonthecircle( 1/3 + 0.001/pi );
% [ T, dimension ] = ChirikovStandardmap(0.2);
% [ T,  dimension ] = Catmap();
% [ T,  dimension ] = Henononthetorus(0.1);
% [ T,  dimension ] = unfoldedbakermap();  
%[ T,  dimension ] = ABCmap(1.5,0.08,0.16);
%[ T,  dimension ] = anzaitypemap(1/pi);


%% Core part of the algorithm: Periodic approximation


%%%% SETTINGS %%%%
N =500;              % Spatial discretization level

% Perform the periodic approximation
[DiscreteMap] = Periodicapproximation_new();





%% Pick an observable

% 3-dimensional observable
 g = @(x)( sin(2*pi*x(:,2)) + cos(2*pi*x(:,1)) +  sin(2*pi*x(:,3) )  );%

