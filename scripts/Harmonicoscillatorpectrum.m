clc;
clear all;
close all;

global tau spacing Proj_range domain_range spect_res spect_bandwith g V T gradV gradT

%% Simulation parameters

% Duffing osccilator model parameters
omega_0 = 1;

% %%%% Define Duffing model functions %%%%
V = @(q)( 0.5*omega_0^2*q.^2);
T = @(p)( 0.5 * p.^2 );
gradV = @(q)( omega_0^2*q);
gradT = @(p)( p );

%%%% Periodic approximation discretization  %%%%
tau = 0.05;
spacing = 0.005;

%%%% Observable %%%%
domain_range = 1.5;  % domain of interest
g=@(q,p)( sin(p) + sin(4*q) +  4*exp(-abs(p))*(cos(4*q) + 1)  );


%%%% Spectral plots %%%%
Proj_range = [1.9, 2.1];
spect_bandwith = 8;
spect_res = spect_bandwith*2/300; %


%% Perform calculations

% Run main routine
[ Q,P, Proj_observable, omega, energydensity] = Hamiltonian1Dposition();

%% Spectral density plot

%
omega_bandwith = pi/tau; 
omega_min = -spect_bandwith;
omega_max = spect_bandwith;

figure('position',[100 100 850 300]);

z = energydensity;
x = zeros(size(omega)); 
y = omega;


%plot3(x,y,EnergyDensity_analytic,'r','LineWidth',1)
hold on
plot3(x,y,z,'k','LineWidth',1)

plot3([-2 2],[0 0], [0 0],'k')
plot3([0 0],[1.2*omega_min 1.2*omega_max], [0 0],'k')

%plot3([-0.5 0.5],[omega_bandwith omega_bandwith], [0 0],'k')
%plot3([-0.5 0.5],[-omega_bandwith -omega_bandwith], [0 0],'k')


text(2.2,0,0,'Re \lambda')
text(0,1.2*omega_max,0,'Im \lambda')

grid off
axis off
view(77,25)
pbaspect([20 200 40])

saveas(gcf,'spectrarotationtorus','epsc')
%close all;

%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(Q, P,real(Proj_observable)  ) ;
axis square
shading interp
hold off;
xlabel('q','FontSize',18); ylabel('p','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(Q, P, imag(Proj_observable)  ) ;
axis square
shading interp
hold off;
xlabel('q','FontSize',18); ylabel('p','FontSize',18)
title('imag')
view(0,90)
colorbar

saveas(gcf,'eig1','epsc')
%close all;





