clc;
clear all;
close all;

global T dimension N



[ T, dimension ] = rotationonthecircle(1/3);
g = @(x)(sin(4*pi*x) ./ ( 1 + cos(2*pi*x).^2 + sin(7*pi*x) )  );
alpha = 2*pi/500;       % Spectral resolution 


%% Analytic solution of spectra

x = 0:0.0001:1;
eig1 = (g(x) + g(T(x)) + g(T(T(x))))/3;
eig2 = (g(x) + exp(-1i*2*pi/3)*g(T(x)) + exp(-1i*4*pi/3)*g(T(T(x))))/3; 
eig3 = (g(x) + exp(-1i*4*pi/3)*g(T(x)) + exp(-1i*8*pi/3)*g(T(T(x))))/3; 

c1 = sum(abs(eig1).^2)*0.0001;
c2 = sum(abs(eig2).^2)*0.0001;
c3 = sum(abs(eig3).^2)*0.0001;
eigfreq_thetas = [0; 2*pi/3; 4*pi/3]; c = [c1;c2;c3];

% Set the resolution 
no_ofevaluations = floor(1.5*(2*pi/alpha));
theta = (0:(2*pi/no_ofevaluations):((no_ofevaluations-1)/no_ofevaluations)*2*pi);
EnergyDensity_analytic = func_mollify( eigfreq_thetas, c, theta, alpha );
theta = [theta,theta(1)];
EnergyDensity_analytic = [EnergyDensity_analytic,EnergyDensity_analytic(1)];



%%
discretization = [100, 1000, 10000, 100000];



for k=1:4;              % Spatial discretization level

N = discretization(k);  
% Perform the periodic approximation
[DiscreteMap] = Periodicapproximation();



[theta, energydensity] = compute_Koopmanmodeenergy_mollify(alpha, g, DiscreteMap );



z = energydensity;
x = real(exp(1i*theta)); 
y = imag(exp(1i*theta));


figure
plot3(x,y,EnergyDensity_analytic,'r','LineWidth',1)
hold on
plot3(x,y,z,'k','LineWidth',1)
plot3([-1.8 1.8],[0 0], [0 0],'k')
plot3([0 0],[-1.8 1.8], [0 0],'k')
plot3(cos(0:0.01:2*pi), sin(0:0.01:2*pi), 0*cos((0:0.01:2*pi)),'k-.' )
text(1.1,0,0,'\theta = 0')
text(0,1.1,0,'\theta = \pi/2')
text(-1.4,0,0,'\theta = -\pi \equiv \pi')
text(0,-1.3,0,'\theta = -\pi/2')
text(1.9,0,0,'Re \lambda')
text(0,1.9,0,'Im \lambda')

grid off
axis off
view(47,30)

h = title(['$\tilde{n} =$',num2str(N) ],'Interpreter','LaTex');
set(h,'position',[1.8 -1.5])

saveas(gcf,['n',num2str(N)],'epsc')

close all;

end  

