clc;
clear all;
close all;



%% Simulation parameters

% Duffing osccilator model parameters
beta = 1;
alpha = 0;



%%%% Observable %%%%
domain_range = 2;  % domain of interest
%specified in function


%%%% Spectral plots %%%%
Proj_range = [-0.5, 0.5];
spect_bandwith = 10;
spect_res = spect_bandwith/800; %




%% Perform calculations


tic 
tau =  0.1;
spacing = 0.1;

[ ~,~, ~, omega2, energydensity2] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc

tic
tau = 0.01;
spacing = 0.01;

[ ~,~, ~, omega3, energydensity3] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc


tic
tau = 0.001;
spacing = 0.001;

[ ~,~, ~, omega4, energydensity4] = Duffing_mex(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc

%% Spectral density plot

figure('position',[100 100 850 400]);
semilogy(omega2,energydensity2,'-.','Color',[0.5 0.5 0.5],'LineWidth',1)
hold on
semilogy(omega3,energydensity3,'k-.','LineWidth',1)
semilogy(omega4,energydensity4,'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
%axis equal
axis([0 spect_bandwith min(energydensity4) max(energydensity4)])
legend('\tau = 0.1, \Deltax = 0.1', '\tau = 0.01, \Deltax = 0.01', '\tau = 0.001, \Deltax = 0.001')

% 
% %% Spectral Projection plot
% 
% figure('position',[100 100 850 400]);
% subplot(1,2,1)
% surf(Q, P, real(Proj_observable)  ) ;
% axis([-domain_range domain_range -domain_range domain_range])
% axis square
% shading interp
% hold off;
% xlabel('q','FontSize',18); ylabel('p','FontSize',18)
% title('real')
% view(0,90)
% colorbar
% subplot(1,2,2)
% surf(Q, P, imag(Proj_observable)  ) ;
% axis([-domain_range domain_range -domain_range domain_range])
% axis square
% shading interp
% hold off;
% xlabel('q','FontSize',18); ylabel('p','FontSize',18)
% title('imag')
% view(0,90)
% colorbar