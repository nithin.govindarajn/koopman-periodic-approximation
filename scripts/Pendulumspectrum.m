clc;
clear all;
close all;


%% Simulation parameters



%%%% Periodic approximation discretization  %%%%
tau = 0.025;
spacing = 0.002;

%%%% Observable %%%%
domain_range = pi;  % domain of interest
%specified in function


%%%% Spectral plots %%%%
Proj_range = [7.5, 8.0];
spect_bandwith = 20;
spect_res = spect_bandwith/100; %


%% Perform calculations

% Run main routine
tic
[ Q,P, Proj_observable, omega, energydensity] = Pendulum_mex(tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith);
toc
%% Spectral density plot

figure('position',[100 100 850 400]);
semilogy(omega,(energydensity),'k','LineWidth',1.5)
xlabel('\omega','FontSize',18);ylabel('\rho_{\alpha}(\omega;g)','FontSize',18)
%axis equal
axis([0 spect_bandwith min(energydensity) max(energydensity)])
saveas(gcf,'spectdensitypendulum','epsc')


%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(Q, P, real(Proj_observable)  ) ;
axis([-domain_range domain_range -domain_range domain_range])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(Q, P, imag(Proj_observable)  ) ;
axis([-domain_range domain_range -domain_range domain_range])
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

saveas(gcf,'pend15to20','epsc')
%close all;





