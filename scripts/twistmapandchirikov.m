clc;
clear all;
close all;

global T dimension N


%Set K value
K=0.10;

[ T, dimension ] = ChirikovStandardmap(K);
N = 2000;          % Spatial discretization level
alpha = 500;       % Spectral resolution 


% compute periodic approximation
[DiscreteMap] = Periodicapproximation();




%% 

g = @(x)(  exp(1i*4*pi*x(:,1)) + exp(1i*3*pi*x(:,1)) + 0.01*exp(1i*2*pi*x(:,2))  );


%%

[theta, energydensity] = compute_Koopmanmodeenergy_mollify( 2*pi/500, g, DiscreteMap );


z = energydensity;
x = real(exp(1i*theta)); 
y = imag(exp(1i*theta));

hold on
plot3(x,y,z,'k','LineWidth',1)


plot3([-1.8 1.8],[0 0], [0 0],'k')
plot3([0 0],[-1.8 1.8], [0 0],'k')
plot3(cos(0:0.01:2*pi), sin(0:0.01:2*pi), 0*cos((0:0.01:2*pi)),'k-.' )
text(1.1,0,0,'\theta = 0')
text(0,1.1,0,'\theta = \pi/2')
text(-1.4,0,0,'\theta = -\pi \equiv \pi')
text(0,-1.3,0,'\theta = -\pi/2')
text(1.9,0,0,'Re \lambda')
text(0,1.9,0,'Im \lambda')

grid off
axis off
view(47,30)

axis([-2 2 -2 2 0 10])

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.52, 0.18,'\bf $K=0.10$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')



saveas(gcf,'spectraK10','epsc')
%close all;



%% II. Compute projections
close all


Projrange = [-0.02,0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $K=0.10$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'1periodK10','epsc')

Projrange = [pi-0.02,pi+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $K=0.10$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')

saveas(gcf,'2periodK10','epsc')

Projrange = [2*pi/3-0.02,2*pi/3+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $K=0.10$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')

saveas(gcf,'3periodK10','epsc')

Projrange = [pi/2-0.02,pi/2+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar
ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $K=0.10$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'4periodK10','epsc')



