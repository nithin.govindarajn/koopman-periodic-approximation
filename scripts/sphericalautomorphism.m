clc
clear all 
close all

global T dimension N


%% Select a map or create a map of your own

% % rotation on the sphere
% alpha = pi/3;
% beta = pi/3;
% omega = pi/2;
% T = sphericalrotationmap( alpha,beta, omega );


% % twist on the sphere
% alpha = pi/3;
% beta = pi/3;
% omegafunc = @(z)(sin(2*pi*z));
% 
% T = sphericaltwistmap( alpha,beta, omegafunc );

% % reflection on the sphere
alpha = pi/3;
beta = pi/3;
T = sphericalreflectionmap( alpha,beta)

%% Core part of the algorithm: Periodic approximation


%%%% SETTINGS %%%%
N = 400;

x_grid = eq_point_set(2,N^2); x_grid= x_grid';



% periodic approximation

%%%%  STEP 1: Construct the discrete map %%%%

% SNAPSHOT PAIRS
Xdata = x_grid; Ydata = T(x_grid);
graphsize = length(Xdata(:,1));

edge_sink = rangesearch(Ydata, Xdata, 0.01);

edge_source = cell(size(edge_sink));
for j=1: length(edge_source);
   edge_source{j} = j*ones(length(edge_sink{j}),1); 
end
edge_source = cell2mat(edge_source); edge_sink= transpose(cell2mat(edge_sink'));



[ Un ] = bipartitematch(edge_source, edge_sink, graphsize);





tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = Xdata;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));



%% Pick an observable

% Observable
l = 3; % degree
m = 2; % order

g = @(x)( sphericalharmonic_obs(l,m,x));

%% RESULTS:
%%% There are a couple things you can do %%%
% I.   You can simulate the observable
% II. You can compute projections of an observable
% III.  You can generate a spectral energy density plot

%% I. simulate observable propagation


% Propagate observable
K = 1;
xsim = DiscreteMap.x;
Ug = g(xsim);
Ugdiscrete = g(DiscreteMap.x);
for k=1:K
    %One step propagation of the Koopman operator
    xsim = T(xsim); Ug = g(xsim);
    Ugdiscrete = PropagateObservable( DiscreteMap.Un, Ugdiscrete);
end



%upper hemisphere points
up_hemisphere = DiscreteMap.x(:,3)>0;
lo_hemisphere = DiscreteMap.x(:,3)<0;

%plotting grid
[xi,yi] = meshgrid(-1:0.005:1, -1:0.005:1);


figure('position',[100 100 850 600]);
subplot(2,3,1)
zi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),Ug(up_hemisphere),xi,yi);
p1 = surf(xi,yi,zi1);
title('Upper hemisphere U^k g')
shading interp
view(2)
axis equal off
subplot(2,3,2)
zi2 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),Ugdiscrete(up_hemisphere),xi,yi);
p2 = surf(xi,yi,zi2);
title('Upper hemisphere U^k_n g')
shading interp
view(2)
axis equal off
subplot(2,3,3)
p3 = surf(xi,yi,abs(zi1-zi2));
title('Upper hemisphere error')
shading interp
view(2)
axis equal off
subplot(2,3,4)
zi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),Ug(lo_hemisphere),xi,yi);
p4 = surf(xi,yi,zi1);
title('Lower hemisphere U^k g')
shading interp
view(2)
axis equal off
subplot(2,3,5)
zi2 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),Ugdiscrete(lo_hemisphere),xi,yi);
p5 = surf(xi,yi,zi2);
title('Upper hemisphere U^k_n g')
shading interp
view(2)
axis equal off
subplot(2,3,6)
p6 = surf(xi,yi,abs(zi1-zi2));
title('Lower hemisphere error')
shading interp
view(2)
axis equal off



%% II. Compute projections

% Projection
Projrange = [-0.05,+0.05];
tic
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );
toc
Phi_real = real(Phi); 
Phi_imag = imag(Phi);


%upper hemisphere points
up_hemisphere = DiscreteMap.x(:,3)>0;
lo_hemisphere = DiscreteMap.x(:,3)<0;

%plotting grid
[xi,yi] = meshgrid(-1:0.005:1, -1:0.005:1);

figure('position',[100 100 850 600]);
subplot(2,2,1)
zi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),Phi_real(up_hemisphere),xi,yi);
p1 = surf(xi,yi, zi1);
title('Upper hemisphere (real part)')
shading interp
view(2)
axis equal off
subplot(2,2,2)
zi1 = griddata(DiscreteMap.x(up_hemisphere,1),DiscreteMap.x(up_hemisphere,2),Phi_imag(up_hemisphere),xi,yi);
p1 = surf(xi,yi, zi1);
title('Upper hemisphere (imaginary part)')
shading interp
view(2)
axis equal off
subplot(2,2,3)
zi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),Phi_real(lo_hemisphere),xi,yi);
p1 = surf(xi,yi, zi1);
title('Lower hemisphere (real part)')
shading interp
view(2)
axis equal off
subplot(2,2,4)
zi1 = griddata(DiscreteMap.x(lo_hemisphere,1),DiscreteMap.x(lo_hemisphere,2),Phi_imag(lo_hemisphere),xi,yi);
p1 = surf(xi,yi, zi1);
title('Lower hemisphere (imaginary part)')
shading interp
view(2)
axis equal off


%% III. spectral density function and projections


alpha = 1000;       % Spectral resolution 


%%%% Spectral content %%%%
tic
[EnergyDensity, theta, ranges ] = compute_Koopmanmodeenergy( alpha, g, DiscreteMap );
toc


plotKoopmanmodeenergy( ranges, EnergyDensity,alpha, [])
view(70,30)
  

