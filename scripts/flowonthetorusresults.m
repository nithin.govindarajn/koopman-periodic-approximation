clc;
clear all;
close all;

global T dt dimension N 

%%%% SETTINGS %%%%
omega1 = 1/2;
omega2 = pi/2;
dt = 0.05;                   % temporal dicscretization
N =1000;                    % spatial discretization


[ T, dimension ] = flowon2torus(dt,omega1,omega2);

% compute periodic approximation
[DiscreteMap] = Periodicapproximation();

%% Analytic solution of spectra

% Observable
g = @(x)( sin(2*pi*x(:,1)) .* cos(2*pi*x(:,2)) +  sin(pi*x(:,2)) +  1 ./ (sin(pi*x(:,1).^2)+1) -1 );



% To be computed...


%%


%window
bandwith = 100;
alpha = bandwith*2/1000; %
omega_bandwith = pi/dt; 
omega_min = -bandwith;
omega_max = bandwith;

[omega, energydensity] = compute_Koopmanmodeenergy_mollify_flow(alpha, g, DiscreteMap, [omega_min, omega_max] );



figure('position',[100 100 850 300]);

z = energydensity;
x = zeros(size(omega)); 
y = omega;


%plot3(x,y,EnergyDensity_analytic,'r','LineWidth',1)
hold on
plot3(x,y,z,'k','LineWidth',1)

plot3([-2 2],[0 0], [0 0],'k')
plot3([0 0],[1.2*omega_min 1.2*omega_max], [0 0],'k')

plot3([-0.5 0.5],[omega_bandwith omega_bandwith], [0 0],'k')
plot3([-0.5 0.5],[-omega_bandwith -omega_bandwith], [0 0],'k')


text(2.2,0,0,'Re \lambda')
text(0,1.2*omega_max,0,'Im \lambda')

grid off
axis off
view(77,25)
pbaspect([20 200 40])

saveas(gcf,'spectrarotationtorus','epsc')
%close all;
%% II. Compute projections



Projrange = [-0.02,0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

text(0.5, 0.12,'\bf $D=\left[\frac{2\pi}{3}-0.01,\frac{2\pi}{3}+ 0.01\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig1','epsc')
close all;
%%

Projrange = [pi/3-0.02,pi/3+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

saveas(gcf,'eig2','epsc')
close all;

%%

Projrange = [2*pi/3-0.02,2*pi/3+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar


saveas(gcf,'eig3','epsc')
close all;

%%

Projrange = [pi-0.02,pi+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar


saveas(gcf,'eig4','epsc')
close all;


%%

Projrange = [2*pi*4/6-0.02,2*pi*4/6+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

saveas(gcf,'eig5','epsc')
close all;
%%

Projrange = [2*pi*5/6-0.02,2*pi*5/6+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar


saveas(gcf,'eig6','epsc')
close all;











