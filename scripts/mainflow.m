clc;
clear all;
close all;

global T dimension N




%% Core part of the algorithm: Periodic approximation


%%%% SETTINGS %%%%
omega = 1/3;
dt = 1;                   % temporal dicscretization
N =10000;                    % spatial discretization
[ T, dimension ] = flowonthecircle(dt,omega);


% Perform the periodic approximation
[DiscreteMap] = Periodicapproximation_new();





%% Pick an observable

% 1-dimensional observable 
 g = @(x)(1 ./ ( 2 + cos(6*pi*x).^2 + sin(7*pi*x) )  );
% g = @(x)( sin(2*pi*x)  );

% 2-dimensional observable
%g = @(x)( sin(2*pi*x(:,1))  );

% g = @(x)( sin(2*pi*x(:,1)) .* cos(2*pi*x(:,2)) +  sin(pi*x(:,2)) +  1 ./ (sin(pi*x(:,1).^2)+1) -1 );
% g = @(x)(bumpfunction(x,[0.25, 0.25], 0.1));
% g = @(x)( exp(1i*2*pi*2*x(:,1))   );

% 3-dimensional observable
% g = @(x)( sin(2*pi*x(:,2)) + cos(2*pi*x(:,1)) +  sin(2*pi*x(:,3) )  );%
% g = @(x)( sin(2*pi*x(:,2)) + cos(2*pi*x(:,1)) +  sin(2*pi*x(:,3))  +sin(2*pi*x(:,2)) + cos(6*pi*x(:,1)) +  sin(4*pi*x(:,3) )  );


%% RESULTS:
%%% There are a couple things you can do %%%
% I.   You can simulate the observable propagation
% II. You can compute projections of an observable
% III.  You can generate a spectral energy density


%% I. simulate observable propagation


no_iter   =10;
pausetime = 2;


%Simulate
SimulateObservable(DiscreteMap, g, no_iter, pausetime )


%% II. Compute projections

Projrange = [-0.01,0.01];


tic
%range of projection
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );
toc


plotProjection(DiscreteMap.x, Phi)


Phi2 = PropagateObservable(DiscreteMap.Un, Phi);

plotProjection(DiscreteMap.x, Phi2)

%% III. spectral density function and projections 

alpha = 1000;       % Spectral resolution 



%%%% Spectral content %%%%
tic
[EnergyDensity, theta, ranges ] = compute_Koopmanmodeenergy( alpha, g, DiscreteMap );
toc

%%


plotKoopmanmodeenergy( ranges, EnergyDensity,alpha, Projrange)
title({['Spatial discretization level N=', num2str(N)]; ...
      ['Spectral resolution \alpha =', num2str(alpha)]});
  






