clc;
clear all;
close all;

global T dimension N


omega1 = 1/3;
omega2 = 1/2;
[ T, dimension ] = rotationon2torus(omega1,omega2);
N =1000;              % Spatial discretization level
alpha =2*pi/500;       % Spectral resolution 
g = @(x)( sin(2*pi*x(:,1)) .* cos(2*pi*x(:,2)) +  sin(pi*x(:,2)) +  1 ./ (sin(pi*x(:,1).^2)+1) -1 );


% compute periodic approximation
[DiscreteMap] = Periodicapproximation();

%% Analytic solution of spectra

[x1,x2] = meshgrid(0:0.01:1,0:0.01:1);
x = [x1(:),x2(:)];
eig1 = (g(x) + g(T(x)) + g(T(T(x))) + g(T(T(T(x))))  + g(T(T(T((x))))) + g(T(T(T(T(T(x)))))) )/6;
eig2 = (g(x) + exp(-1i*2*1*pi/6)*g(T(x)) + exp(-1i*2*2*pi/6)*g(T(T(x))) + exp(-1i*2*3*pi/6)*g(T(T(T(x))))  + exp(-1i*2*4*pi/6)*g(T(T(T((x))))) + exp(-1i*2*5*pi/6)*g(T(T(T(T(T(x)))))) )/6;
eig3 = (g(x) + exp(-1i*2*1*2*pi/6)*g(T(x)) + exp(-1i*2*2*2*pi/6)*g(T(T(x))) + exp(-1i*2*3*2*pi/6)*g(T(T(T(x))))  + exp(-1i*2*4*2*pi/6)*g(T(T(T((x))))) + exp(-1i*2*5*2*pi/6)*g(T(T(T(T(T(x)))))) )/6;
eig4 = (g(x) + exp(-1i*2*1*3*pi/6)*g(T(x)) + exp(-1i*2*2*3*pi/6)*g(T(T(x))) + exp(-1i*2*3*3*pi/6)*g(T(T(T(x))))  + exp(-1i*2*4*3*pi/6)*g(T(T(T((x))))) + exp(-1i*2*5*3*pi/6)*g(T(T(T(T(T(x)))))) )/6;
eig5 = (g(x) + exp(-1i*2*1*4*pi/6)*g(T(x)) + exp(-1i*2*2*4*pi/6)*g(T(T(x))) + exp(-1i*2*3*4*pi/6)*g(T(T(T(x))))  + exp(-1i*2*4*4*pi/6)*g(T(T(T((x))))) + exp(-1i*2*5*4*pi/6)*g(T(T(T(T(T(x)))))) )/6;
eig6 = (g(x) + exp(-1i*2*1*5*pi/6)*g(T(x)) + exp(-1i*2*2*5*pi/6)*g(T(T(x))) + exp(-1i*2*3*5*pi/6)*g(T(T(T(x))))  + exp(-1i*2*4*5*pi/6)*g(T(T(T((x))))) + exp(-1i*2*5*5*pi/6)*g(T(T(T(T(T(x)))))) )/6;




c1 = sum(abs(eig1).^2)*0.0001;
c2 = sum(abs(eig2).^2)*0.0001;
c3 = sum(abs(eig3).^2)*0.0001;
c4 = sum(abs(eig4).^2)*0.0001;
c5 = sum(abs(eig5).^2)*0.0001;
c6 = sum(abs(eig6).^2)*0.0001;

eigfreq_thetas = [0; 2*1*pi/6; 2*2*pi/6;2*3*pi/6;2*4*pi/6;2*5*pi/6]; c = [c1;c2;c3;c4;c5;c6];

% Set the resolution 
no_ofevaluations = floor(1.5*(2*pi/alpha));
theta = (0:(2*pi/no_ofevaluations):((no_ofevaluations-1)/no_ofevaluations)*2*pi);
EnergyDensity_analytic = func_mollify( eigfreq_thetas, c, theta, alpha );
theta = [theta,theta(1)];
EnergyDensity_analytic = [EnergyDensity_analytic,EnergyDensity_analytic(1)];


%%

[theta, energydensity] = compute_Koopmanmodeenergy_mollify(alpha, g, DiscreteMap );



z = energydensity;
x = real(exp(1i*theta)); 
y = imag(exp(1i*theta));


plot3(x,y,EnergyDensity_analytic,'r','LineWidth',1)
hold on
plot3(x,y,z,'k','LineWidth',1)

plot3([-1.8 1.8],[0 0], [0 0],'k')
plot3([0 0],[-1.8 1.8], [0 0],'k')
plot3(cos(0:0.01:2*pi), sin(0:0.01:2*pi), 0*cos((0:0.01:2*pi)),'k-.' )
text(1.1,0,0,'\theta = 0')
text(0,1.1,0,'\theta = \pi/2')
text(-1.4,0,0,'\theta = -\pi \equiv \pi')
text(0,-1.3,0,'\theta = -\pi/2')
text(1.9,0,0,'Re \lambda')
text(0,1.9,0,'Im \lambda')

grid off
axis off
view(47,30)

h = title(['$\tilde{n} =$',num2str(N) ],'Interpreter','LaTex');
set(h,'position',[1.8 -1.5])

saveas(gcf,'spectrarotationtorus','epsc')
close all;
%% II. Compute projections



Projrange = [-0.02,0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[-0.01,+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig1','epsc')
close all;
%%

Projrange = [pi/3-0.02,pi/3+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar


ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[\frac{\pi}{3}-0.01,\frac{\pi}{3}+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig2','epsc')
close all;

%%

Projrange = [2*pi/3-0.02,2*pi/3+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[\frac{2\pi}{3}-0.01,\frac{2\pi}{3}+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig3','epsc')
close all;

%%

Projrange = [pi-0.02,pi+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[\pi-0.01,\pi+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig4','epsc')
close all;


%%

Projrange = [2*pi*4/6-0.02,2*pi*4/6+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[\frac{4\pi}{3}-0.01,\frac{4\pi}{3}+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig5','epsc')
close all;
%%

Projrange = [2*pi*5/6-0.02,2*pi*5/6+0.02];
[ Phi] = ProjObservable( Projrange, g, DiscreteMap );


figure('position',[100 100 850 400]);
subplot(1,2,1)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('real')
view(0,90)
colorbar
subplot(1,2,2)
surf(reshape(DiscreteMap.x(:,1),[N,N]), reshape(DiscreteMap.x(:,2),[N,N]), reshape( imag(Phi), [N,N])) ;
axis square
shading interp
hold off;
xlabel('x_1','FontSize',18); ylabel('x_2','FontSize',18)
title('imag')
view(0,90)
colorbar

ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 0.12,'\bf $D=\left[\frac{5\pi}{3}-0.01,\frac{5\pi}{3}+ 0.02\right]$','Interpreter','LaTex','Fontsize',20,'HorizontalAlignment' ,'center','VerticalAlignment', 'top')


saveas(gcf,'eig6','epsc')
close all;











