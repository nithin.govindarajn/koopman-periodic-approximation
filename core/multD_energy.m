function [ c_selected_normalized ] = multD_energy( c, range)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here



n = length(c);
k= (1:n)';
theta_k  = 2*pi*(k-1)/n;


theta_left  =  range(1);
theta_right =  range(2);


if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected  =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected  =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end

c_selected_normalized = (1/sqrt(n)) * c(selected);

end