function [ EnergyDensity ] = func_mollify( eigfreq_thetas, c, theta, resolution )
%#codegen

EnergyDensity = zeros(size(theta));

for k=1:length(theta)
    
dist_norm = min(  abs(eigfreq_thetas-theta(k)), 2*pi-abs(eigfreq_thetas-theta(k)) )/resolution;
output =  2.2523*exp(-1./(1-(dist_norm(dist_norm<1)).^2))/resolution;
    
EnergyDensity(k) = dot( output , c(dist_norm<1) );
end



end
