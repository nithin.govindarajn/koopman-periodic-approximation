function [ x ] = FormGRID()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
global dimension N

switch dimension
    case 1
        x = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))'));
    case 2         
        x1 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))')); x2 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))'));
        [X1, X2] = meshgrid(x1,x2);
        x = [X1(:), X2(:)];
    case 3
        x1 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))')); x2 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))')); x3 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))'));
        [X1, X2, X3] = meshgrid(x1,x2,x3);
        x = [X1(:), X2(:), X3(:)];
        
end


end

