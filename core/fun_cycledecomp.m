function [ cycledecomp, inverse_cycledecomp, cycle_lengths  ] = fun_cycledecomp( perm )
%#codegen


visited = zeros(size(perm));
cycledecomp = zeros(size(perm));
cycle_lengths = zeros(size(perm));

k= 0;
s = 0;
for ii = 1:length(perm)
    
        if visited(ii) == 0 
            s = s+1;
            l = 0; 
            jj = ii; 
                while jj ~= ii || l == 0            
                    tmp = jj;
                    l = l + 1;
                    k = k + 1;
                    jj = perm(jj);
                    visited(tmp) = 1;      % indicate that this entry was visited
                    cycledecomp(k) = jj;
                end
            cycle_lengths(s) = l;
        end
        
end

cycle_lengths = cycle_lengths(cycle_lengths~=0);

inverse_cycledecomp = func_invperm( cycledecomp );




end

