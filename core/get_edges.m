function [ edges ] = get_edges(IDX_f, IDX_c, list, dimension, N,graphsize )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


source_cell = cell(size(list,1),1); sink_cell = cell(size(list,1),1); 
    for k=1:size(list,1)
        multi_index = cell(1,dimension); 
        for l=1:dimension
            if list(k,l) > 0
                    multi_index{1,l} = IDX_c(:,l)+list(k,l)-1;
                    
                    multi_index{1,l}( multi_index{1,l} > N) = N;
                    
            elseif list(k,l) < 0
                    multi_index{1,l} = IDX_f(:,l) +list(k,l)+1;
                    
                    multi_index{1,l}( multi_index{1,l} < 1) = 1;
            end
        end
        if dimension >1
            source_cell{k} = sub2ind(N*ones(1,dimension), multi_index{dimension:-1:1});
        else
            source_cell{k} =  multi_index{1};
        end
        sink_cell{k} = (1:graphsize)';
    end

   
    edges = [cell2mat(source_cell) cell2mat(sink_cell)];
    edges = unique(edges,'rows');
end

