function [ Phi ] = ProjObservable_core( gdiscrete, cycledecomp, inverse_cycledecomp, cycle_lengths, range )
%#codegen


% step 1: permute the observable
gdiscrete = gdiscrete(cycledecomp);


% Step 2: process every cycle block
y = complex(zeros(size(gdiscrete)), zeros(size(gdiscrete)));
a = 0;
for ii=1:length(cycle_lengths)
   a_new = a + cycle_lengths(ii);
   
   % the hard calculation
   y(a+1:a_new) = fft(gdiscrete(a+1:a_new)); 
   y(a+1:a_new) = multD_proj( y(a+1:a_new), range);
   y(a+1:a_new) = ifft(y(a+1:a_new)); 
   
   a = a_new;
end

% Step 3: inverse permute
Phi = y(inverse_cycledecomp);


end

