function [ DiscreteMap] = Periodicapproximation_quadruplegyre(N,dt)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here




%%%%  STEP 1: Construct the discrete map %%%%

% Form grid, evaluate map, project onto grid
x1 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))')); x2 = ((((1/(2*N)) : (1/N) : (1-1/(2*N)))'));
[X1, X2] = meshgrid(x1,x2);
x = [X1(:), X2(:)];

graphsize = length(x);

t = (0:dt:(1-dt))';
Nt = length(t);
Un = cell(size(t));
for k=1:length(t)
        % The discretized flow
        T = @(x)(symplecticeuler(x,t(k),dt));
        dimension =2;


        Tx = (mod(T(x)+1/(2*N),1)*N);
        % Describe the grid on which the discrete map is defined
        IDX_f = floor(Tx); IDX_f(IDX_f == N+1) = 1; IDX_f(IDX_f == 0)   = N;
        IDX_c = ceil(Tx); IDX_c(IDX_c == N+1) = 1; IDX_c(IDX_c == 0)   = N;

        matchingisperfect = 0; dilfact = 1;
        while ~matchingisperfect

            % Append edge list
            neigbors = [-dilfact:-1, 1:dilfact];
            list = permn(neigbors,dimension);
            edges = get_edges(IDX_f, IDX_c, list, dimension, N,graphsize );   

            % Solve the bipartite graph problem to form the discrete Koopman operator
            tic
            [ Un{k} ] = bipartitematch(edges, graphsize);
            disp('Time needed to solve max-flow problem:')
            toc

            if isempty(Un{k})        
                disp('Dilate bipartite graph')
                dilfact = dilfact+1;
            else
                matchingisperfect = 1;
            end

        end
        Un{k} = Un{k} + mod(k,Nt)*N^2;
        disp(k)
end

Un = cell2mat(Un);
tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc

time = kron(t, ones(N^2,1));
x = kron(ones(length(t),1),x);
x = [x time];

% The discrete map
DiscreteMap.time = time;
DiscreteMap.N = N;
DiscreteMap.Nt = Nt;
DiscreteMap.x = x;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));


end

