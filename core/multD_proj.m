function [ d ] = multD_proj( c, range)
%#codegen



n = length(c);
k= (1:n)';
theta_k  = 2*pi*(k-1)/n;


theta_left  =  range(1);
theta_right =  range(2);


if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected  =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected  =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end
c(not(selected)) = 0;

d = c;


end
