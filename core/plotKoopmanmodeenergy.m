function [] = plotKoopmanmodeenergy( ranges, energydensity, resolution , Projrange )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here


figure('position',[100 100 850 600]);


% to be written
if resolution < 50
    
    for ii = 1:resolution
        
    end
    
end


% to be written
if resolution< 100 && resolution>50
    
    for ii = 1:resolution

    end
    
end



if resolution > 100
    
    theta = ranges'; theta = theta(:);
    
    z = (1/max(energydensity))*repmat(energydensity,1,2)'; z= z(:);
    x = real(exp(1i*theta)); 
    y = imag(exp(1i*theta));
    
    
    z(end+1) = z(1);
    x(end+1) = x(1);
    y(end+1) = y(1);
    
    plot3(x,y,z,'r','LineWidth',1)
    hold on
    
    
end
if ~isempty(Projrange)
    
    thetastart = Projrange(1);
    thetaend   = Projrange(2);
    
    withinrange = (theta < thetaend & theta > thetastart); 
    x_withinrange = x(withinrange);
    y_withinrange = y(withinrange);
    z_withinrange = z(withinrange);
    plot3(x_withinrange,y_withinrange,z_withinrange,'b','LineWidth',1)
    for k= 1:length(x_withinrange)
        plot3([x_withinrange(k) x_withinrange(k)], [y_withinrange(k) y_withinrange(k)], [0 z_withinrange(k)], 'b' )  
    end
    plot3(cos(thetastart:0.000001:thetaend), sin(thetastart:0.000001:thetaend), 0*sin(thetastart:0.000001:thetaend), 'b', 'LineWidth', 1)
end


plot3([-1.5 1.5],[0 0], [0 0],'k')
plot3([0 0],[-1.5 1.5], [0 0],'k')
plot3(cos(0:0.01:2*pi), sin(0:0.01:2*pi), 0*sin(0:0.01:2*pi),'k' )
text(1.8,0,0,'\omega = 0')
text(0,1.8,0,'\omega = \pi/2')
text(-1.8,0,0,'\omega = \pi')
text(0,-1.8,0,'\omega = 3\pi/2')


grid off
axis off
view(30,45)
hold off


end

