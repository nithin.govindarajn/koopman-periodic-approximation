function [omega, EnergyDensity] = compute_Koopmanmodeenergy_mollify_flow( resolution, g, DiscreteMap, bandwith, dt)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here



% Set the resolution 
no_ofevaluations = floor(1.5*((bandwith)/resolution));



x = DiscreteMap.x;
cycledecomp = DiscreteMap.cycledecomp;
cycle_lengths = DiscreteMap.cycle_lengths;


% discrete representation of the observable
gdiscrete = g(x);

% Denote the eigendecomposition by  Un = V D V^(-1), define c = V^(-1) * gdiscrete
test =  mat2cell(gdiscrete(cycledecomp)*(1/sqrt(length(gdiscrete))), cycle_lengths,1); 

eigfreq_thetas = cellfun(@(n)((0:(2*pi/n):((n-1)/n)*2*pi)'), num2cell(cycle_lengths), 'UniformOutput', 0); 
c = cellfun(@(x)(abs(fft(x)/sqrt(length(x))).^2), test, 'UniformOutput', 0); 

eigfreq_thetas = cell2mat(eigfreq_thetas); 
eigfreq_thetas(eigfreq_thetas >= pi) = eigfreq_thetas(eigfreq_thetas >= pi) - 2*pi;

eigfreq_omega = eigfreq_thetas/dt;
c = cell2mat(c); 
omega = (0:2*bandwith/no_ofevaluations:bandwith)';

tic
EnergyDensity = func_mollify_realline( eigfreq_omega, c, omega, resolution );
toc

end
