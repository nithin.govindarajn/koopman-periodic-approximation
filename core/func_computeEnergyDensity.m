function [ EnergyDensity ] = func_computeEnergyDensity(c, ranges, resolution,cycle_lengths )
%#codegen



EnergyDensity = zeros(resolution,1);

for kk = 1:resolution;
    range = ranges(kk,:);
    
    a = 0;
    edensity = 0;
    for ii=1:length(cycle_lengths)
       a_new = a + cycle_lengths(ii);
       %------------------------------------------------------------------------------
            
            theta_left  =  range(1);
            theta_right =  range(2);

            if theta_left < 0
                kright = ceil( ((2*pi-theta_left)*cycle_lengths(ii))/(2*pi) + 1 );
                edensity = edensity + sum(abs(c((a+kright):(a+cycle_lengths(ii)))).^2);
                
                check =(theta_right*cycle_lengths(ii))/(2*pi) + 1;
                if mod(check,1) ==0
                    kleft = check-1;
                else
                kleft = floor(check);
                end
                edensity = edensity +  sum(abs(c((a+1):(a+kleft))).^2);
            else
                kleft = ceil( (theta_left*cycle_lengths(ii))/(2*pi) + 1 );
                check = (theta_right*cycle_lengths(ii))/(2*pi) + 1;
                if mod(check,1) ==0
                    if kleft < check
                    kright = check-1;
                    else
                       kright=kleft; 
                    end
                else
                kright = floor(check);
                end
                edensity = edensity + sum(abs(c((a+kleft):(a+kright))).^2);
            end
           
        %------------------------------------------------------------------------------
       a = a_new;
    end
    EnergyDensity(kk) = (edensity);

end 




%%%%%%%%

% range_cell = cell(length(cycle_lengths),1);
% 
% EnergyDensity = zeros(resolution,1);
% for k = 1:resolution;
%     [range_cell{1:length(cycle_lengths)}] = deal(ranges(k,:));
%     c_selected = cellfun(@multD_energy, c, range_cell, 'UniformOutput', 0);
%     EnergyDensity(k) = (2*pi/resolution)*norm(cell2mat(c_selected));
% end 

 

end

