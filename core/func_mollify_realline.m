function [ EnergyDensity ] = func_mollify_realline( eigfreq_omega, c, omega, resolution )
%#codegen

EnergyDensity = zeros(size(omega));

for k=1:length(omega)
    
dist_norm = abs(eigfreq_omega-omega(k))/resolution;
output =  2.2523*exp(-1./(1-(dist_norm(dist_norm<1)).^2))/resolution;
    
EnergyDensity(k) = dot( output , c(dist_norm<1) );
end



end