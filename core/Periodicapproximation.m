function [ DiscreteMap] = Periodicapproximation()
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

global T  N

%%%%  STEP 1: Construct the discrete map %%%%


% Describe the grid on which the discrete map is defined
x = FormGRID(); graphsize = length(x);
Tx = (mod(T(x)+1/(2*N),1)*N); 
Un = []; dilfact = 0;
edges = [];
while isempty(Un)
  
    % Append edge list
    tic
    [edges] = formBIPARTITEGRPH(Tx,graphsize, dilfact, edges);
    disp('Time needed to construct list of edges:')
    toc
    
    tic
    % Solve the bipartite graph problem to form the discrete Koopman operator
    [ Un ] = bipartitematch(edges, graphsize);
    disp('Time needed to solve max-flow problem:')
    toc
    
    if isempty(Un)        
        disp('Dilate bipartite graph')
        dilfact = dilfact+1;
    end

end



tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = x;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));


end

