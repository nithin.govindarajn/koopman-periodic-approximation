function [ Phi] = ProjObservable_flow( range, g, DiscreteMap ,dt)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here



range_circle = dt*range;


cycledecomp         = DiscreteMap.cycledecomp;
inverse_cycledecomp = DiscreteMap.inverse_cycledecomp;
cycle_lengths       = DiscreteMap.cycle_lengths;
x                   = DiscreteMap.x;


% discrete representation of the observable
gdiscrete = g(x);





% the core part of the code
%[ Phi ] = ProjObservable_core( gdiscrete, cycledecomp, inverse_cycledecomp, cycle_lengths, range );




% Denote the eigendecomposition by  Un = V D V^(-1), define c = V^(-1) * gdiscrete
test =  mat2cell(gdiscrete(cycledecomp), cycle_lengths,1); 
c = cellfun(@fft, test, 'UniformOutput', 0);


range_cell = cell(length(cycle_lengths),1);    
[range_cell{1:length(cycle_lengths)}] = deal(range_circle);

test3 = cellfun(@multD_proj, c, range_cell, 'UniformOutput', 0);
test4 = cellfun(@ifft, test3, 'UniformOutput', 0);
test5 = cell2mat(test4);

Phi = test5(inverse_cycledecomp);



%%%




end





