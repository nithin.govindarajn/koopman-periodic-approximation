function [theta, EnergyDensity] = compute_Koopmanmodeenergy_mollify( resolution, g, DiscreteMap )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here



% Set the resolution 
no_ofevaluations = floor(1.5*(2*pi/resolution));


x = DiscreteMap.x;
cycledecomp = DiscreteMap.cycledecomp;
cycle_lengths = DiscreteMap.cycle_lengths;


% discrete representation of the observable
gdiscrete = g(x);

% Denote the eigendecomposition by  Un = V D V^(-1), define c = V^(-1) * gdiscrete
test =  mat2cell(gdiscrete(cycledecomp)*(1/sqrt(length(gdiscrete))), cycle_lengths,1); 

eigfreq_thetas = cellfun(@(n)((0:(2*pi/n):((n-1)/n)*2*pi)'), num2cell(cycle_lengths), 'UniformOutput', 0); 
c = cellfun(@(x)(abs(fft(x)/sqrt(length(x))).^2), test, 'UniformOutput', 0); 


% 
eigfreq_thetas = cell2mat(eigfreq_thetas); 
c = cell2mat(c); 
theta = (0:(2*pi/no_ofevaluations):((no_ofevaluations-1)/no_ofevaluations)*2*pi);

tic
EnergyDensity = func_mollify( eigfreq_thetas, c, theta, resolution );
toc

theta = [theta,theta(1)];
EnergyDensity = [EnergyDensity,EnergyDensity(1)];
end