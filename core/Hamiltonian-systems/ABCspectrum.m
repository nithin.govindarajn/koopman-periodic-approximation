clc;
clear all;
close all;


%% Simulation parameters


%%%% Arnold-Beltrami-Childress flow %%%%
A = sqrt(3);
B = sqrt(2);
C = 1;


%%%% Periodic approximation discretization  %%%%
tau = 0.1;
n = 300;

%%%% Observable %%%%
%specified in function


%%%% Spectral plots %%%%
Proj_range = [-0.1, 0.1];
spect_bandwith = 6;
spect_res = spect_bandwith*2/200; %


%% Perform calculations

% Run main routine
tic
[ X,Y,Z, Proj_observable, omega, energydensity] = ABCflow(A,B,C,tau, n, ...
    Proj_range, spect_res, spect_bandwith);
toc
%% Spectral density plot

%
omega_bandwith = pi/tau; 
omega_min = -spect_bandwith;
omega_max = spect_bandwith;

figure('position',[100 100 850 300]);

z = energydensity;
x = zeros(size(omega)); 
y = omega;


%plot3(x,y,EnergyDensity_analytic,'r','LineWidth',1)
hold on
plot3(x,y,z,'k','LineWidth',1)

plot3([-2 2],[0 0], [0 0],'k')
plot3([0 0],[1.2*omega_min 1.2*omega_max], [0 0],'k')

%plot3([-0.5 0.5],[omega_bandwith omega_bandwith], [0 0],'k')
%plot3([-0.5 0.5],[-omega_bandwith -omega_bandwith], [0 0],'k')


text(2.2,0,0,'Re \lambda')
text(0,1.2*omega_max,0,'Im \lambda')

grid off
axis off
view(77,25)
pbaspect([20 200 40])

saveas(gcf,'spectrarotationtorus','epsc')
%close all;

%% Spectral Projection plot

figure('position',[100 100 850 400]);
subplot(1,2,1)
slice(X, Y,Z,real(Proj_observable),[0.2, 0.4],[],[])
axis([0 1 0 1 0 1])
shading interp
grid off
hold off;
xlabel('x','FontSize',18); ylabel('y','FontSize',18);zlabel('z','FontSize',18)
title('real')
view(30,40)
colorbar
subplot(1,2,2)
slice(X, Y,Z,imag(Proj_observable),[0.2, 0.4],[],[])
axis([0 1 0 1 0 1])
shading interp
grid off
hold off;
xlabel('x','FontSize',18); ylabel('y','FontSize',18);zlabel('z','FontSize',18)
title('imag')
view(30,40)
colorbar

saveas(gcf,'eig1','epsc')
%close all;





