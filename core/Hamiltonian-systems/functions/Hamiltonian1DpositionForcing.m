function [ Q,P, S, Proj_observable, omega, Energydensity ] = Hamiltonian1DpositionForcing()
%#codegen

global tau spacing Proj_range domain_range spect_res spect_bandwith g ForcingPeriod 



%%%% Set up grid %%%%%
translate = round(domain_range/spacing +1);
n = round(2*domain_range/spacing +1);
no_gridpoints = n^2;

[Q,P, S] = meshgrid(-domain_range:spacing:domain_range,-domain_range:spacing:domain_range,0:tau:(ForcingPeriod-tau)); 
time_length = size(Q,3); 

Pnat = round(round(P/spacing)+translate); Qnat = round(round(Q/spacing)+translate); 
Snat = round(round(S/tau)+1);


%%%% Define spectral variables %%%%
Proj_observable = zeros(n,n,time_length);
range_circle = tau*Proj_range;
no_ofevaluations = floor(1.5*((2*spect_bandwith))/spect_res);
omega = (-spect_bandwith:(2*spect_bandwith)/no_ofevaluations:spect_bandwith)';
Energydensity = zeros(size(omega));
 
%%%% allocate memory %%%%
traj_trace = zeros(no_gridpoints,3);
trace_insidedomain = zeros(no_gridpoints,1);
observ_trace = zeros(no_gridpoints,1);
c = zeros(no_gridpoints,1);

Flagged = zeros(n,n,time_length);
no_cycles = 0;
no_flagged = 0;
for l= 1:length(Q(:))
    if ~Flagged(Qnat(l),Pnat(l),Snat(l))
        
        % Simulate trajectory until it hits starting point
        qnat = Qnat(l);
        pnat =  Pnat(l);
        snat =  Snat(l);
        
        m = 1;
        for k= 1:length(Q(:))
            
            if qnat>0 && qnat<=n && pnat>0 && pnat<=n
                Flagged(qnat,pnat,snat) = 1;
                no_flagged = no_flagged +1;
                disp(['q0= ',num2str(Q(l)),'p0= ',num2str(P(l)),'k = ',num2str(k),'no_flagged = ' ,num2str(no_flagged),' of ',num2str(length(Q(:))),' total cycles = ',num2str(no_cycles)])
                trace_insidedomain(m) =k;
                m = m+1;
            end
            
           
            % update trajectory trace
                 traj_trace(k,:) = [qnat, pnat, snat];
            
            % Propagate forward with periodic approximation
            [qnat,pnat,snat] = periodic_apprx(qnat,pnat,snat,translate,time_length);
            
            
            % update trace
            observ_trace(k) = g((qnat-translate)*spacing, (pnat-translate)*spacing, (snat-1)*tau);
            
            if pnat == Pnat(l) && qnat == Qnat(l) && snat == Snat(l)
              
                %%% Cycle has completed: perform spectral calculations %%%
                no_cycles = no_cycles +1;
                % map cycle into frequency domain
                c(1:k) = fft(observ_trace(1:k));
                
                % update spectral projection plot
                Proj_observable = update_SpectralProj(Proj_observable, c(1:k), traj_trace(1:k,:), range_circle, trace_insidedomain(1:(m-1)),n,time_length);
                
                % update spectral density plot
                [Energydensity] = update_spectraldensity(Energydensity, c(1:k), k, omega, spect_res, tau);
                
                
                
                break;
            end
        end
    end
end


end



%%%%%%  Periodic approximation  functions  %%%%%%
function [qnat_update,pnat_update,snat_update] = periodic_apprx(qnat,pnat,snat,translate,time_length)
    
    global gradV gradT ForcingFunc tau spacing
    
    pnat = round(pnat - round((tau*gradV((qnat-translate)*spacing))/spacing));
    qnat = round(qnat + round((tau/spacing)*( gradT( (pnat-translate)*spacing)  + ForcingFunc((snat-1)*tau) )  ) );
    snat = mod(snat,time_length)+1;
    
    qnat_update =  qnat;
    pnat_update =  pnat;
    snat_update =  snat;
end


%%%%%%%% Spectral calculation functions %%%%%%%%%


%%%% Spectral Projection

function [ d ] = multD_proj( c, range)
%#codegen


n = length(c);
k= (1:n)';
theta_k  = 2*pi*(k-1)/n;

theta_left  =  range(1);
theta_right =  range(2);

if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected  =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected  =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end
c(not(selected)) = 0;

d = c;

end

function [Proj_observable] = update_SpectralProj(Proj_observable, c, traj_trace, range_circle, trace_insidedomain,n,time_length)
   
     %%% compute projection on trajectory %%%
     
     Proj_traj = ifft(multD_proj( c, range_circle));
     
     
     
     %%% update entries %%%
     
     % find those entries which lie inside
     linindex = sub2ind( [n,n,time_length], traj_trace(trace_insidedomain,2), traj_trace(trace_insidedomain,1),traj_trace(trace_insidedomain,3));
     
     %update entries
     Proj_observable( linindex )  =  Proj_traj(trace_insidedomain);

     
     
end


%%%%% Spectral density

function [omega_l] = findomega(l, cyclelength, tau)

omega_l = (-pi + (2*pi/cyclelength)*(l-1) + mod(cyclelength,2) * pi/cyclelength)/tau;     
     
end

function [l] = findl(omega, cyclelength, tau)

l = 1 + cyclelength/(2*pi) * (omega*tau + pi -  mod(cyclelength,2) * pi/cyclelength);  

end

function [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau)

l_left = ceil(findl(omega - spect_res,cyclelength, tau));
l_right = floor(findl(omega + spect_res,cyclelength, tau));

if l_left>cyclelength || l_right<1
    l_left = [];
    l_right = [];
elseif l_left<1
    l_left = 1;
elseif l_right>cyclelength
    l_right = cyclelength;
end

end

function [out] = convolvewithbump(c,omega, spect_res, cyclelength, tau)

    [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau);

    if isempty(l_left)
        out = 0;
    else

        eig_omega = findomega(l_left:l_right, cyclelength, tau);
        dist_norm = abs(eig_omega-omega(1))/spect_res;
        output =  (2.2523*exp(-1./(1-(dist_norm).^2))/spect_res)';

        out = dot( output , c(l_left:l_right) );
    end

end

function [Energydensity] = update_spectraldensity(Energydensity, c, cyclelength, omega, spect_res, tau)

% shift all entries so that range is from [-pi,pi) (or equivalently [-pi*tau,pi*tau)) instead of [0,2pi)
c = circshift(c,floor(length(c)/2));
% normalize and square the entries
c = (abs(c/sqrt(cyclelength))).^2;

% update entries by excecuting convolution
for l=1:length(omega)
    Energydensity(l) = Energydensity(l) + convolvewithbump(c,omega(l), spect_res, cyclelength, tau);
end


     
     
end

