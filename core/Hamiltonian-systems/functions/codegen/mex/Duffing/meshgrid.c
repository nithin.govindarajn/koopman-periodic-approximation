/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * meshgrid.c
 *
 * Code generation for function 'meshgrid'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "meshgrid.h"
#include "Duffing_emxutil.h"
#include "eml_int_forloop_overflow_check.h"
#include "Duffing_data.h"

/* Variable Definitions */
static emlrtRSInfo p_emlrtRSI = { 31,  /* lineNo */
  "meshgrid",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\meshgrid.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 32,  /* lineNo */
  "meshgrid",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\meshgrid.m"/* pathName */
};

static emlrtRTEInfo y_emlrtRTEI = { 1, /* lineNo */
  23,                                  /* colNo */
  "meshgrid",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\meshgrid.m"/* pName */
};

/* Function Definitions */
void meshgrid(const emlrtStack *sp, const emxArray_real_T *x, const
              emxArray_real_T *y, emxArray_real_T *xx, emxArray_real_T *yy)
{
  int32_T nx;
  int32_T ny;
  int32_T unnamed_idx_0;
  int32_T unnamed_idx_1;
  int32_T i3;
  boolean_T overflow;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  nx = x->size[1];
  ny = y->size[1];
  unnamed_idx_0 = y->size[1];
  unnamed_idx_1 = x->size[1];
  i3 = xx->size[0] * xx->size[1];
  xx->size[0] = unnamed_idx_0;
  xx->size[1] = unnamed_idx_1;
  emxEnsureCapacity_real_T(sp, xx, i3, &y_emlrtRTEI);
  unnamed_idx_0 = y->size[1];
  unnamed_idx_1 = x->size[1];
  i3 = yy->size[0] * yy->size[1];
  yy->size[0] = unnamed_idx_0;
  yy->size[1] = unnamed_idx_1;
  emxEnsureCapacity_real_T(sp, yy, i3, &y_emlrtRTEI);
  if ((x->size[1] == 0) || (y->size[1] == 0)) {
  } else {
    st.site = &p_emlrtRSI;
    overflow = (x->size[1] > 2147483646);
    if (overflow) {
      b_st.site = &o_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (unnamed_idx_0 = 0; unnamed_idx_0 < nx; unnamed_idx_0++) {
      st.site = &q_emlrtRSI;
      if ((1 <= ny) && (ny > 2147483646)) {
        b_st.site = &o_emlrtRSI;
        check_forloop_overflow_error(&b_st);
      }

      for (unnamed_idx_1 = 0; unnamed_idx_1 < ny; unnamed_idx_1++) {
        xx->data[unnamed_idx_1 + xx->size[0] * unnamed_idx_0] = x->
          data[unnamed_idx_0];
        yy->data[unnamed_idx_1 + yy->size[0] * unnamed_idx_0] = y->
          data[unnamed_idx_1];
      }
    }
  }
}

/* End of code generation (meshgrid.c) */
