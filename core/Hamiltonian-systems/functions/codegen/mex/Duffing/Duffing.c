/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Duffing.c
 *
 * Code generation for function 'Duffing'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "Duffing_emxutil.h"
#include "indexShapeCheck.h"
#include "eml_int_forloop_overflow_check.h"
#include "circshift.h"
#include "sub2ind.h"
#include "ifft.h"
#include "mod.h"
#include "fft.h"
#include "toLogicalCheck.h"
#include "round.h"
#include "meshgrid.h"
#include "Duffing_data.h"
#include "blas.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 74,    /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 71,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 68,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 57,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 37,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 26,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 19,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 16,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 14,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 13,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 12,  /* lineNo */
  "Duffing",                           /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 98,  /* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 282, /* lineNo */
  "eml_float_colon",                   /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 290, /* lineNo */
  "eml_float_colon",                   /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 45,  /* lineNo */
  "mpower",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\mpower.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 55,  /* lineNo */
  "power",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 142,/* lineNo */
  "update_SpectralProj",               /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 149,/* lineNo */
  "update_SpectralProj",               /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo hb_emlrtRSI = { 16, /* lineNo */
  "sub2ind",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pathName */
};

static emlrtRSInfo kb_emlrtRSI = { 217,/* lineNo */
  "update_spectraldensity",            /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo lb_emlrtRSI = { 219,/* lineNo */
  "update_spectraldensity",            /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo mb_emlrtRSI = { 223,/* lineNo */
  "update_spectraldensity",            /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo ac_emlrtRSI = { 65, /* lineNo */
  "applyBinaryScalarFunction",         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\applyBinaryScalarFunction.m"/* pathName */
};

static emlrtRSInfo bc_emlrtRSI = { 187,/* lineNo */
  "flatIter",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\applyBinaryScalarFunction.m"/* pathName */
};

static emlrtRSInfo cc_emlrtRSI = { 16, /* lineNo */
  "abs",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elfun\\abs.m"/* pathName */
};

static emlrtRSInfo dc_emlrtRSI = { 74, /* lineNo */
  "applyScalarFunction",               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\applyScalarFunction.m"/* pathName */
};

static emlrtRSInfo ec_emlrtRSI = { 64, /* lineNo */
  "fltpower",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 203,/* lineNo */
  "convolvewithbump",                  /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 204,/* lineNo */
  "convolvewithbump",                  /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo hc_emlrtRSI = { 205,/* lineNo */
  "convolvewithbump",                  /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo ic_emlrtRSI = { 207,/* lineNo */
  "convolvewithbump",                  /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

static emlrtRSInfo jc_emlrtRSI = { 25, /* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 9,  /* lineNo */
  "exp",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elfun\\exp.m"/* pathName */
};

static emlrtRSInfo lc_emlrtRSI = { 25, /* lineNo */
  "dot",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\specfun\\dot.m"/* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 86, /* lineNo */
  "vdot",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\specfun\\dot.m"/* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 32, /* lineNo */
  "xdotc",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdotc.m"/* pathName */
};

static emlrtRSInfo oc_emlrtRSI = { 49, /* lineNo */
  "xdot",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdot.m"/* pathName */
};

static emlrtRSInfo pc_emlrtRSI = { 50, /* lineNo */
  "xdot",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdot.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 12,  /* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 19,/* lineNo */
  14,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo d_emlrtRTEI = { 19,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 98,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 19,/* lineNo */
  56,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 19,/* lineNo */
  43,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo h_emlrtRTEI = { 23,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo i_emlrtRTEI = { 80,/* lineNo */
  5,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo j_emlrtRTEI = { 26,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo k_emlrtRTEI = { 86,/* lineNo */
  5,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo l_emlrtRTEI = { 84,/* lineNo */
  5,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo m_emlrtRTEI = { 91,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo n_emlrtRTEI = { 27,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo o_emlrtRTEI = { 30,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo p_emlrtRTEI = { 31,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo q_emlrtRTEI = { 32,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo r_emlrtRTEI = { 33,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo s_emlrtRTEI = { 35,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo t_emlrtRTEI = { 68,/* lineNo */
  30,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo u_emlrtRTEI = { 71,/* lineNo */
  72,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo v_emlrtRTEI = { 71,/* lineNo */
  80,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo w_emlrtRTEI = { 1, /* lineNo */
  59,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo x_emlrtRTEI = { 74,/* lineNo */
  73,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo cb_emlrtRTEI = { 136,/* lineNo */
  30,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo db_emlrtRTEI = { 25,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo eb_emlrtRTEI = { 204,/* lineNo */
  25,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo fb_emlrtRTEI = { 16,/* lineNo */
  5,                                   /* colNo */
  "abs",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elfun\\abs.m"/* pName */
};

static emlrtRTEInfo gb_emlrtRTEI = { 204,/* lineNo */
  9,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo hb_emlrtRTEI = { 64,/* lineNo */
  5,                                   /* colNo */
  "power",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pName */
};

static emlrtRTEInfo ib_emlrtRTEI = { 205,/* lineNo */
  31,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo jb_emlrtRTEI = { 205,/* lineNo */
  9,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo kb_emlrtRTEI = { 207,/* lineNo */
  29,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo lb_emlrtRTEI = { 203,/* lineNo */
  31,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo nb_emlrtRTEI = { 142,/* lineNo */
  35,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo ob_emlrtRTEI = { 118,/* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo pb_emlrtRTEI = { 124,/* lineNo */
  24,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo qb_emlrtRTEI = { 128,/* lineNo */
  25,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo rb_emlrtRTEI = { 126,/* lineNo */
  24,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo sb_emlrtRTEI = { 124,/* lineNo */
  59,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo tb_emlrtRTEI = { 128,/* lineNo */
  50,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo ub_emlrtRTEI = { 126,/* lineNo */
  49,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo vb_emlrtRTEI = { 124,/* lineNo */
  5,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo wb_emlrtRTEI = { 128,/* lineNo */
  5,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo xb_emlrtRTEI = { 126,/* lineNo */
  5,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo yb_emlrtRTEI = { 142,/* lineNo */
  23,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo ac_emlrtRTEI = { 149,/* lineNo */
  33,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo bc_emlrtRTEI = { 149,/* lineNo */
  67,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo cc_emlrtRTEI = { 152,/* lineNo */
  23,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo dc_emlrtRTEI = { 142,/* lineNo */
  6,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo ec_emlrtRTEI = { 117,/* lineNo */
  5,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo fc_emlrtRTEI = { 219,/* lineNo */
  10,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo gc_emlrtRTEI = { 212,/* lineNo */
  66,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo hc_emlrtRTEI = { 219,/* lineNo */
  6,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  74,                                  /* lineNo */
  77,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  74,                                  /* lineNo */
  75,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  135,                                 /* colNo */
  "trace_insidedomain",                /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  132,                                 /* colNo */
  "trace_insidedomain",                /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo e_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  93,                                  /* colNo */
  "traj_trace",                        /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo f_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  91,                                  /* colNo */
  "traj_trace",                        /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo g_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  54,                                  /* lineNo */
  27,                                  /* colNo */
  "traj_trace",                        /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo h_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  76,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo i_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  71,                                  /* lineNo */
  74,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  68,                                  /* lineNo */
  17,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtBCInfo j_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  68,                                  /* lineNo */
  21,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo k_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  68,                                  /* lineNo */
  19,                                  /* colNo */
  "c",                                 /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo l_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  68,                                  /* lineNo */
  45,                                  /* colNo */
  "observ_trace",                      /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo m_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  68,                                  /* lineNo */
  43,                                  /* colNo */
  "observ_trace",                      /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo emlrtDCI = { 33,    /* lineNo */
  19,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo b_emlrtDCI = { 32,  /* lineNo */
  30,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtECInfo b_emlrtECI = { 2,   /* nDims */
  23,                                  /* lineNo */
  19,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtRTEInfo jc_emlrtRTEI = { 388,/* lineNo */
  15,                                  /* colNo */
  "assert_pmaxsize",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtDCInfo c_emlrtDCI = { 30,  /* lineNo */
  20,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo d_emlrtDCI = { 30,  /* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo e_emlrtDCI = { 31,  /* lineNo */
  1,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo n_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  37,                                  /* lineNo */
  9,                                   /* colNo */
  "Flagged",                           /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo f_emlrtDCI = { 37,  /* lineNo */
  9,                                   /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo o_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  37,                                  /* lineNo */
  9,                                   /* colNo */
  "Qnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo p_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  37,                                  /* lineNo */
  9,                                   /* colNo */
  "Pnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo q_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  40,                                  /* lineNo */
  16,                                  /* colNo */
  "Qnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo r_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  41,                                  /* lineNo */
  16,                                  /* colNo */
  "Pnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo s_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  47,                                  /* lineNo */
  17,                                  /* colNo */
  "Flagged",                           /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo g_emlrtDCI = { 47,  /* lineNo */
  17,                                  /* colNo */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo t_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  48,                                  /* lineNo */
  17,                                  /* colNo */
  "trace_insidedomain",                /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo u_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  61,                                  /* lineNo */
  13,                                  /* colNo */
  "observ_trace",                      /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo v_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  63,                                  /* lineNo */
  24,                                  /* colNo */
  "Pnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo w_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  63,                                  /* lineNo */
  43,                                  /* colNo */
  "Qnat",                              /* aName */
  "Duffing",                           /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo h_emlrtDCI = { 207, /* lineNo */
  31,                                  /* colNo */
  "convolvewithbump",                  /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo x_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  207,                                 /* lineNo */
  31,                                  /* colNo */
  "c",                                 /* aName */
  "convolvewithbump",                  /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo i_emlrtDCI = { 207, /* lineNo */
  38,                                  /* colNo */
  "convolvewithbump",                  /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo y_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  207,                                 /* lineNo */
  38,                                  /* colNo */
  "c",                                 /* aName */
  "convolvewithbump",                  /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo nc_emlrtRTEI = { 13,/* lineNo */
  15,                                  /* colNo */
  "dot",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\specfun\\dot.m"/* pName */
};

static emlrtRTEInfo oc_emlrtRTEI = { 41,/* lineNo */
  19,                                  /* colNo */
  "eml_sub2ind",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtRTEInfo pc_emlrtRTEI = { 31,/* lineNo */
  23,                                  /* colNo */
  "eml_sub2ind",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pName */
};

static emlrtECInfo c_emlrtECI = { -1,  /* nDims */
  128,                                 /* lineNo */
  20,                                  /* colNo */
  "multD_proj",                        /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtECInfo d_emlrtECI = { -1,  /* nDims */
  126,                                 /* lineNo */
  20,                                  /* colNo */
  "multD_proj",                        /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtECInfo e_emlrtECI = { -1,  /* nDims */
  124,                                 /* lineNo */
  20,                                  /* colNo */
  "multD_proj",                        /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtECInfo f_emlrtECI = { -1,  /* nDims */
  152,                                 /* lineNo */
  6,                                   /* colNo */
  "update_SpectralProj",               /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pName */
};

static emlrtBCInfo ab_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  130,                                 /* lineNo */
  1,                                   /* colNo */
  "c",                                 /* aName */
  "multD_proj",                        /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  44,                                  /* colNo */
  "traj_trace",                        /* aName */
  "update_SpectralProj",               /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  78,                                  /* colNo */
  "traj_trace",                        /* aName */
  "update_SpectralProj",               /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo db_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  23,                                  /* colNo */
  "Proj_observable",                   /* aName */
  "update_SpectralProj",               /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  48,                                  /* colNo */
  "Proj_traj",                         /* aName */
  "update_SpectralProj",               /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  223,                                 /* lineNo */
  24,                                  /* colNo */
  "Energydensity",                     /* aName */
  "update_spectraldensity",            /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  223,                                 /* lineNo */
  62,                                  /* colNo */
  "omega",                             /* aName */
  "update_spectraldensity",            /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  223,                                 /* lineNo */
  5,                                   /* colNo */
  "Energydensity",                     /* aName */
  "update_spectraldensity",            /* fName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m",/* pName */
  0                                    /* checkKind */
};

/* Function Declarations */
static real_T convolvewithbump(const emlrtStack *sp, const emxArray_real_T *c,
  real_T omega, real_T spect_res, real_T cyclelength, real_T tau);
static void periodic_apprx(real_T alpha, real_T beta, real_T qnat, real_T pnat,
  real_T translate, real_T tau, real_T spacing, real_T *qnat_update, real_T
  *pnat_update);
static void update_SpectralProj(const emlrtStack *sp, emxArray_creal_T
  *Proj_observable, const emxArray_creal_T *c, const emxArray_real_T *traj_trace,
  const real_T range_circle[2], const emxArray_real_T *trace_insidedomain,
  real_T n);
static void update_spectraldensity(const emlrtStack *sp, emxArray_real_T
  *Energydensity, emxArray_creal_T *c, real_T cyclelength, const emxArray_real_T
  *omega, real_T spect_res, real_T tau);

/* Function Definitions */
static real_T convolvewithbump(const emlrtStack *sp, const emxArray_real_T *c,
  real_T omega, real_T spect_res, real_T cyclelength, real_T tau)
{
  real_T out;
  real_T ndbl;
  real_T l_left;
  real_T l_right;
  emxArray_real_T *y;
  int32_T k;
  real_T apnd;
  int32_T nm1d2;
  real_T cdiff;
  int32_T n;
  emxArray_real_T *dist_norm;
  boolean_T overflow;
  emxArray_real_T *output;
  emxArray_real_T *b;
  int32_T iv0[2];
  uint32_T varargin_1[2];
  int32_T varargin_2[2];
  boolean_T p;
  boolean_T exitg1;
  ptrdiff_t n_t;
  ptrdiff_t incx_t;
  ptrdiff_t incy_t;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  if (muDoubleScalarIsNaN(cyclelength) || muDoubleScalarIsInf(cyclelength)) {
    ndbl = rtNaN;
  } else {
    ndbl = muDoubleScalarRem(cyclelength, 2.0);
    if (ndbl == 0.0) {
      ndbl = 0.0;
    }
  }

  l_left = muDoubleScalarCeil(1.0 + cyclelength / 6.2831853071795862 * (((omega
    - spect_res) * tau + 3.1415926535897931) - ndbl * 3.1415926535897931 /
    cyclelength));
  if (muDoubleScalarIsNaN(cyclelength) || muDoubleScalarIsInf(cyclelength)) {
    ndbl = rtNaN;
  } else {
    ndbl = muDoubleScalarRem(cyclelength, 2.0);
    if (ndbl == 0.0) {
      ndbl = 0.0;
    }
  }

  l_right = muDoubleScalarFloor(1.0 + cyclelength / 6.2831853071795862 *
    (((omega + spect_res) * tau + 3.1415926535897931) - ndbl *
     3.1415926535897931 / cyclelength));
  if ((l_left > cyclelength) || (l_right < 1.0)) {
    out = 0.0;
  } else {
    if (l_left < 1.0) {
      l_left = 1.0;
    } else {
      if (l_right > cyclelength) {
        l_right = cyclelength;
      }
    }

    emxInit_real_T(sp, &y, 2, &lb_emlrtRTEI, true);
    st.site = &fc_emlrtRSI;
    b_st.site = &jc_emlrtRSI;
    if (muDoubleScalarIsNaN(l_left) || muDoubleScalarIsNaN(l_right)) {
      k = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = 1;
      emxEnsureCapacity_real_T(&b_st, y, k, &db_emlrtRTEI);
      y->data[0] = rtNaN;
    } else if (l_right < l_left) {
      y->size[0] = 1;
      y->size[1] = 0;
    } else if ((muDoubleScalarIsInf(l_left) || muDoubleScalarIsInf(l_right)) &&
               (l_left == l_right)) {
      k = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = 1;
      emxEnsureCapacity_real_T(&b_st, y, k, &db_emlrtRTEI);
      y->data[0] = rtNaN;
    } else if (l_left == l_left) {
      k = y->size[0] * y->size[1];
      y->size[0] = 1;
      nm1d2 = (int32_T)muDoubleScalarFloor(l_right - l_left);
      y->size[1] = nm1d2 + 1;
      emxEnsureCapacity_real_T(&b_st, y, k, &db_emlrtRTEI);
      for (k = 0; k <= nm1d2; k++) {
        y->data[k] = l_left + (real_T)k;
      }
    } else {
      c_st.site = &l_emlrtRSI;
      ndbl = muDoubleScalarFloor((l_right - l_left) + 0.5);
      apnd = l_left + ndbl;
      cdiff = apnd - l_right;
      if (muDoubleScalarAbs(cdiff) < 4.4408920985006262E-16 * muDoubleScalarMax
          (l_left, muDoubleScalarAbs(l_right))) {
        ndbl++;
        apnd = l_right;
      } else if (cdiff > 0.0) {
        apnd = l_left + (ndbl - 1.0);
      } else {
        ndbl++;
      }

      if (ndbl >= 0.0) {
        n = (int32_T)ndbl;
      } else {
        n = 0;
      }

      d_st.site = &m_emlrtRSI;
      if (ndbl > 2.147483647E+9) {
        emlrtErrorWithMessageIdR2018a(&d_st, &jc_emlrtRTEI,
          "Coder:MATLAB:pmaxsize", "Coder:MATLAB:pmaxsize", 0);
      }

      k = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = n;
      emxEnsureCapacity_real_T(&c_st, y, k, &e_emlrtRTEI);
      if (n > 0) {
        y->data[0] = l_left;
        if (n > 1) {
          y->data[n - 1] = apnd;
          nm1d2 = (n - 1) / 2;
          d_st.site = &n_emlrtRSI;
          for (k = 0; k <= nm1d2 - 2; k++) {
            y->data[1 + k] = l_left + (1.0 + (real_T)k);
            y->data[(n - k) - 2] = apnd - (1.0 + (real_T)k);
          }

          if (nm1d2 << 1 == n - 1) {
            y->data[nm1d2] = (l_left + apnd) / 2.0;
          } else {
            y->data[nm1d2] = l_left + (real_T)nm1d2;
            y->data[nm1d2 + 1] = apnd - (real_T)nm1d2;
          }
        }
      }
    }

    /* %%%% Spectral density */
    apnd = 6.2831853071795862 / cyclelength;
    if (muDoubleScalarIsNaN(cyclelength) || muDoubleScalarIsInf(cyclelength)) {
      ndbl = rtNaN;
    } else {
      ndbl = muDoubleScalarRem(cyclelength, 2.0);
      if (ndbl == 0.0) {
        ndbl = 0.0;
      }
    }

    emxInit_real_T(sp, &dist_norm, 2, &gb_emlrtRTEI, true);
    st.site = &gc_emlrtRSI;
    k = y->size[0] * y->size[1];
    n = y->size[0] * y->size[1];
    y->size[0] = 1;
    emxEnsureCapacity_real_T(&st, y, n, &eb_emlrtRTEI);
    ndbl = ndbl * 3.1415926535897931 / cyclelength;
    nm1d2 = k - 1;
    for (k = 0; k <= nm1d2; k++) {
      y->data[k] = ((-3.1415926535897931 + apnd * (y->data[k] - 1.0)) + ndbl) /
        tau - omega;
    }

    b_st.site = &cc_emlrtRSI;
    nm1d2 = y->size[1];
    k = dist_norm->size[0] * dist_norm->size[1];
    dist_norm->size[0] = 1;
    dist_norm->size[1] = y->size[1];
    emxEnsureCapacity_real_T(&b_st, dist_norm, k, &fb_emlrtRTEI);
    c_st.site = &dc_emlrtRSI;
    overflow = ((1 <= y->size[1]) && (y->size[1] > 2147483646));
    if (overflow) {
      d_st.site = &o_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (k = 0; k < nm1d2; k++) {
      dist_norm->data[k] = muDoubleScalarAbs(y->data[k]);
    }

    k = dist_norm->size[0] * dist_norm->size[1];
    n = dist_norm->size[0] * dist_norm->size[1];
    dist_norm->size[0] = 1;
    emxEnsureCapacity_real_T(sp, dist_norm, n, &gb_emlrtRTEI);
    nm1d2 = k - 1;
    for (k = 0; k <= nm1d2; k++) {
      dist_norm->data[k] /= spect_res;
    }

    st.site = &hc_emlrtRSI;
    b_st.site = &s_emlrtRSI;
    c_st.site = &ec_emlrtRSI;
    k = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = dist_norm->size[1];
    emxEnsureCapacity_real_T(&c_st, y, k, &hb_emlrtRTEI);
    d_st.site = &ac_emlrtRSI;
    nm1d2 = dist_norm->size[1];
    e_st.site = &bc_emlrtRSI;
    overflow = ((1 <= y->size[1]) && (y->size[1] > 2147483646));
    if (overflow) {
      f_st.site = &o_emlrtRSI;
      check_forloop_overflow_error(&f_st);
    }

    for (k = 0; k < nm1d2; k++) {
      y->data[k] = dist_norm->data[k] * dist_norm->data[k];
    }

    emxFree_real_T(&dist_norm);
    k = y->size[0] * y->size[1];
    n = y->size[0] * y->size[1];
    y->size[0] = 1;
    emxEnsureCapacity_real_T(sp, y, n, &ib_emlrtRTEI);
    nm1d2 = k - 1;
    for (k = 0; k <= nm1d2; k++) {
      y->data[k] = -1.0 / (1.0 - y->data[k]);
    }

    st.site = &hc_emlrtRSI;
    b_st.site = &kc_emlrtRSI;
    nm1d2 = y->size[1];
    c_st.site = &u_emlrtRSI;
    overflow = ((1 <= y->size[1]) && (y->size[1] > 2147483646));
    if (overflow) {
      d_st.site = &o_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (k = 0; k < nm1d2; k++) {
      y->data[k] = muDoubleScalarExp(y->data[k]);
    }

    emxInit_real_T(&b_st, &output, 1, &jb_emlrtRTEI, true);
    k = output->size[0];
    output->size[0] = y->size[1];
    emxEnsureCapacity_real_T(sp, output, k, &jb_emlrtRTEI);
    nm1d2 = y->size[1];
    for (k = 0; k < nm1d2; k++) {
      output->data[k] = 2.2523 * y->data[k] / spect_res;
    }

    emxFree_real_T(&y);
    if (l_left > l_right) {
      k = 1;
      nm1d2 = 0;
    } else {
      k = (int32_T)l_left;
      if (l_left != k) {
        emlrtIntegerCheckR2012b(l_left, &h_emlrtDCI, sp);
      }

      n = c->size[0];
      if ((k < 1) || (k > n)) {
        emlrtDynamicBoundsCheckR2012b(k, 1, n, &x_emlrtBCI, sp);
      }

      if (l_right != (int32_T)muDoubleScalarFloor(l_right)) {
        emlrtIntegerCheckR2012b(l_right, &i_emlrtDCI, sp);
      }

      n = c->size[0];
      nm1d2 = (int32_T)l_right;
      if ((nm1d2 < 1) || (nm1d2 > n)) {
        emlrtDynamicBoundsCheckR2012b(nm1d2, 1, n, &y_emlrtBCI, sp);
      }
    }

    emxInit_real_T(sp, &b, 1, &kb_emlrtRTEI, true);
    iv0[0] = 1;
    nm1d2 = (nm1d2 - k) + 1;
    iv0[1] = nm1d2;
    st.site = &ic_emlrtRSI;
    indexShapeCheck(&st, c->size[0], iv0);
    st.site = &ic_emlrtRSI;
    n = b->size[0];
    b->size[0] = nm1d2;
    emxEnsureCapacity_real_T(&st, b, n, &kb_emlrtRTEI);
    for (n = 0; n < nm1d2; n++) {
      b->data[n] = c->data[(k + n) - 1];
    }

    varargin_1[0] = (uint32_T)output->size[0];
    varargin_1[1] = 1U;
    varargin_2[0] = nm1d2;
    varargin_2[1] = 1;
    overflow = false;
    p = true;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k < 2)) {
      if ((int32_T)varargin_1[k] != varargin_2[k]) {
        p = false;
        exitg1 = true;
      } else {
        k++;
      }
    }

    if (p) {
      overflow = true;
    }

    if (overflow || (output->size[0] == nm1d2)) {
    } else {
      emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
        "MATLAB:dot:InputSizeMismatch", "MATLAB:dot:InputSizeMismatch", 0);
    }

    b_st.site = &lc_emlrtRSI;
    c_st.site = &mc_emlrtRSI;
    d_st.site = &nc_emlrtRSI;
    if (output->size[0] < 1) {
      out = 0.0;
    } else {
      e_st.site = &oc_emlrtRSI;
      e_st.site = &pc_emlrtRSI;
      n_t = (ptrdiff_t)output->size[0];
      incx_t = (ptrdiff_t)1;
      incy_t = (ptrdiff_t)1;
      out = ddot(&n_t, &output->data[0], &incx_t, &b->data[0], &incy_t);
    }

    emxFree_real_T(&b);
    emxFree_real_T(&output);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
  return out;
}

static void periodic_apprx(real_T alpha, real_T beta, real_T qnat, real_T pnat,
  real_T translate, real_T tau, real_T spacing, real_T *qnat_update, real_T
  *pnat_update)
{
  real_T pnat_tmp;

  /* %%%%%  Periodic approximation  functions  %%%%%% */
  pnat_tmp = (qnat - translate) * spacing;
  pnat -= muDoubleScalarRound(tau * (beta * pnat_tmp + alpha *
    muDoubleScalarPower(pnat_tmp, 3.0)) / spacing);
  pnat = muDoubleScalarRound(pnat);
  qnat += muDoubleScalarRound(tau * ((pnat - translate) * spacing) / spacing);
  *qnat_update = muDoubleScalarRound(qnat);
  *pnat_update = pnat;
}

static void update_SpectralProj(const emlrtStack *sp, emxArray_creal_T
  *Proj_observable, const emxArray_creal_T *c, const emxArray_real_T *traj_trace,
  const real_T range_circle[2], const emxArray_real_T *trace_insidedomain,
  real_T n)
{
  emxArray_creal_T *b_c;
  int32_T i6;
  int32_T loop_ub;
  emxArray_real_T *y;
  int32_T i7;
  emxArray_real_T *theta_k;
  int32_T c_c;
  emxArray_boolean_T *selected;
  emxArray_boolean_T *r1;
  real_T d0;
  int32_T i;
  emxArray_int32_T *r2;
  emxArray_creal_T *Proj_traj;
  emxArray_real_T *varargin_2;
  uint32_T varargin_1[2];
  uint32_T b_varargin_2[2];
  boolean_T p;
  boolean_T b_p;
  boolean_T exitg1;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_creal_T(sp, &b_c, 1, &nb_emlrtRTEI, true);

  /* %% compute projection on trajectory %%% */
  st.site = &eb_emlrtRSI;
  i6 = b_c->size[0];
  b_c->size[0] = c->size[0];
  emxEnsureCapacity_creal_T(&st, b_c, i6, &nb_emlrtRTEI);
  loop_ub = c->size[0];
  for (i6 = 0; i6 < loop_ub; i6++) {
    b_c->data[i6] = c->data[i6];
  }

  emxInit_real_T(&st, &y, 2, &ec_emlrtRTEI, true);

  /* %%%%%%% Spectral calculation functions %%%%%%%%% */
  /* %%% Spectral Projection */
  i6 = c->size[0];
  i7 = y->size[0] * y->size[1];
  y->size[0] = 1;
  loop_ub = (int32_T)((real_T)i6 - 1.0);
  y->size[1] = loop_ub + 1;
  emxEnsureCapacity_real_T(&st, y, i7, &db_emlrtRTEI);
  for (i6 = 0; i6 <= loop_ub; i6++) {
    y->data[i6] = 1.0 + (real_T)i6;
  }

  emxInit_real_T(&st, &theta_k, 1, &ob_emlrtRTEI, true);
  c_c = c->size[0];
  i6 = theta_k->size[0];
  theta_k->size[0] = y->size[1];
  emxEnsureCapacity_real_T(&st, theta_k, i6, &ob_emlrtRTEI);
  loop_ub = y->size[1];
  for (i6 = 0; i6 < loop_ub; i6++) {
    theta_k->data[i6] = 6.2831853071795862 * (y->data[i6] - 1.0) / (real_T)c_c;
  }

  emxFree_real_T(&y);
  emxInit_boolean_T(&st, &selected, 1, &vb_emlrtRTEI, true);
  emxInit_boolean_T(&st, &r1, 1, &cb_emlrtRTEI, true);
  if (range_circle[0] < 0.0) {
    d0 = b_mod(range_circle[0]);
    i6 = selected->size[0];
    selected->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &pb_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (theta_k->data[i6] >= d0);
    }

    i6 = r1->size[0];
    r1->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, r1, i6, &sb_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      r1->data[i6] = (theta_k->data[i6] <= range_circle[1]);
    }

    i6 = selected->size[0];
    i7 = r1->size[0];
    if (i6 != i7) {
      emlrtSizeEqCheck1DR2012b(i6, i7, &e_emlrtECI, &st);
    }

    i6 = selected->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &vb_emlrtRTEI);
    loop_ub = selected->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (selected->data[i6] || r1->data[i6]);
    }
  } else if (range_circle[1] > 6.2831853071795862) {
    i6 = selected->size[0];
    selected->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &rb_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (theta_k->data[i6] >= range_circle[0]);
    }

    d0 = b_mod(range_circle[1]);
    i6 = r1->size[0];
    r1->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, r1, i6, &ub_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      r1->data[i6] = (theta_k->data[i6] <= d0);
    }

    i6 = selected->size[0];
    i7 = r1->size[0];
    if (i6 != i7) {
      emlrtSizeEqCheck1DR2012b(i6, i7, &d_emlrtECI, &st);
    }

    i6 = selected->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &xb_emlrtRTEI);
    loop_ub = selected->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (selected->data[i6] || r1->data[i6]);
    }
  } else {
    i6 = selected->size[0];
    selected->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &qb_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (theta_k->data[i6] >= range_circle[0]);
    }

    i6 = r1->size[0];
    r1->size[0] = theta_k->size[0];
    emxEnsureCapacity_boolean_T(&st, r1, i6, &tb_emlrtRTEI);
    loop_ub = theta_k->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      r1->data[i6] = (theta_k->data[i6] <= range_circle[1]);
    }

    i6 = selected->size[0];
    i7 = r1->size[0];
    if (i6 != i7) {
      emlrtSizeEqCheck1DR2012b(i6, i7, &c_emlrtECI, &st);
    }

    i6 = selected->size[0];
    emxEnsureCapacity_boolean_T(&st, selected, i6, &wb_emlrtRTEI);
    loop_ub = selected->size[0];
    for (i6 = 0; i6 < loop_ub; i6++) {
      selected->data[i6] = (selected->data[i6] && r1->data[i6]);
    }
  }

  emxFree_boolean_T(&r1);
  loop_ub = selected->size[0] - 1;
  c_c = 0;
  for (i = 0; i <= loop_ub; i++) {
    if (!selected->data[i]) {
      c_c++;
    }
  }

  emxInit_int32_T(&st, &r2, 1, &cb_emlrtRTEI, true);
  i6 = r2->size[0];
  r2->size[0] = c_c;
  emxEnsureCapacity_int32_T(&st, r2, i6, &yb_emlrtRTEI);
  c_c = 0;
  for (i = 0; i <= loop_ub; i++) {
    if (!selected->data[i]) {
      r2->data[c_c] = i + 1;
      c_c++;
    }
  }

  emxFree_boolean_T(&selected);
  c_c = c->size[0];
  loop_ub = r2->size[0];
  for (i6 = 0; i6 < loop_ub; i6++) {
    i7 = r2->data[i6];
    if ((i7 < 1) || (i7 > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i7, 1, c_c, &ab_emlrtBCI, &st);
    }

    b_c->data[i7 - 1].re = 0.0;
    i7 = r2->data[i6];
    if ((i7 < 1) || (i7 > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i7, 1, c_c, &ab_emlrtBCI, &st);
    }

    b_c->data[i7 - 1].im = 0.0;
  }

  emxInit_creal_T(&st, &Proj_traj, 1, &dc_emlrtRTEI, true);
  st.site = &eb_emlrtRSI;
  ifft(&st, b_c, Proj_traj);

  /* %% update entries %%% */
  /* find those entries which lie inside */
  st.site = &fb_emlrtRSI;
  c_c = traj_trace->size[0];
  i6 = theta_k->size[0];
  theta_k->size[0] = trace_insidedomain->size[0];
  emxEnsureCapacity_real_T(&st, theta_k, i6, &ac_emlrtRTEI);
  loop_ub = trace_insidedomain->size[0];
  emxFree_creal_T(&b_c);
  for (i6 = 0; i6 < loop_ub; i6++) {
    i7 = (int32_T)trace_insidedomain->data[i6];
    if ((i7 < 1) || (i7 > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i7, 1, c_c, &bb_emlrtBCI, &st);
    }

    theta_k->data[i6] = traj_trace->data[(i7 + traj_trace->size[0]) - 1];
  }

  emxInit_real_T(&st, &varargin_2, 1, &bc_emlrtRTEI, true);
  c_c = traj_trace->size[0];
  i6 = varargin_2->size[0];
  varargin_2->size[0] = trace_insidedomain->size[0];
  emxEnsureCapacity_real_T(&st, varargin_2, i6, &bc_emlrtRTEI);
  loop_ub = trace_insidedomain->size[0];
  for (i6 = 0; i6 < loop_ub; i6++) {
    i7 = (int32_T)trace_insidedomain->data[i6];
    if ((i7 < 1) || (i7 > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i7, 1, c_c, &cb_emlrtBCI, &st);
    }

    varargin_2->data[i6] = traj_trace->data[i7 - 1];
  }

  b_st.site = &hb_emlrtRSI;
  i6 = (int32_T)n;
  if (!allinrange(theta_k, i6)) {
    emlrtErrorWithMessageIdR2018a(&b_st, &oc_emlrtRTEI,
      "MATLAB:sub2ind:IndexOutOfRange", "MATLAB:sub2ind:IndexOutOfRange", 0);
  }

  varargin_1[0] = (uint32_T)trace_insidedomain->size[0];
  varargin_1[1] = 1U;
  b_varargin_2[0] = (uint32_T)trace_insidedomain->size[0];
  b_varargin_2[1] = 1U;
  p = false;
  b_p = true;
  c_c = 0;
  exitg1 = false;
  while ((!exitg1) && (c_c < 2)) {
    if ((int32_T)varargin_1[c_c] != (int32_T)b_varargin_2[c_c]) {
      b_p = false;
      exitg1 = true;
    } else {
      c_c++;
    }
  }

  if (b_p) {
    p = true;
  }

  if (!p) {
    emlrtErrorWithMessageIdR2018a(&b_st, &pc_emlrtRTEI,
      "MATLAB:sub2ind:SubscriptVectorSize", "MATLAB:sub2ind:SubscriptVectorSize",
      0);
  }

  if (!allinrange(varargin_2, i6)) {
    emlrtErrorWithMessageIdR2018a(&b_st, &oc_emlrtRTEI,
      "MATLAB:sub2ind:IndexOutOfRange", "MATLAB:sub2ind:IndexOutOfRange", 0);
  }

  /* update entries */
  c_c = Proj_observable->size[0] * Proj_observable->size[1];
  i7 = r2->size[0];
  r2->size[0] = theta_k->size[0];
  emxEnsureCapacity_int32_T(sp, r2, i7, &cc_emlrtRTEI);
  loop_ub = theta_k->size[0];
  for (i7 = 0; i7 < loop_ub; i7++) {
    i = (int32_T)theta_k->data[i7] + i6 * ((int32_T)varargin_2->data[i7] - 1);
    if ((i < 1) || (i > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i, 1, c_c, &db_emlrtBCI, sp);
    }

    r2->data[i7] = i;
  }

  emxFree_real_T(&varargin_2);
  emxFree_real_T(&theta_k);
  c_c = Proj_traj->size[0];
  loop_ub = trace_insidedomain->size[0];
  for (i6 = 0; i6 < loop_ub; i6++) {
    i7 = (int32_T)trace_insidedomain->data[i6];
    if ((i7 < 1) || (i7 > c_c)) {
      emlrtDynamicBoundsCheckR2012b(i7, 1, c_c, &eb_emlrtBCI, sp);
    }
  }

  i6 = r2->size[0];
  i7 = trace_insidedomain->size[0];
  if (i6 != i7) {
    emlrtSubAssignSizeCheck1dR2017a(i6, i7, &f_emlrtECI, sp);
  }

  loop_ub = trace_insidedomain->size[0];
  for (i6 = 0; i6 < loop_ub; i6++) {
    Proj_observable->data[r2->data[i6] - 1] = Proj_traj->data[(int32_T)
      trace_insidedomain->data[i6] - 1];
  }

  emxFree_int32_T(&r2);
  emxFree_creal_T(&Proj_traj);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

static void update_spectraldensity(const emlrtStack *sp, emxArray_real_T
  *Energydensity, emxArray_creal_T *c, real_T cyclelength, const emxArray_real_T
  *omega, real_T spect_res, real_T tau)
{
  emxArray_creal_T *x;
  int32_T i8;
  real_T y;
  int32_T loop_ub;
  emxArray_real_T *a;
  real_T c_re;
  real_T c_im;
  int32_T nx;
  boolean_T overflow;
  emxArray_real_T *b_c;
  uint32_T a_idx_0;
  int32_T i9;
  int32_T i10;
  int32_T i11;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_creal_T(sp, &x, 1, &fc_emlrtRTEI, true);

  /*  shift all entries so that range is from [-pi,pi) (or equivalently [-pi*tau,pi*tau)) instead of [0,2pi) */
  i8 = (int32_T)muDoubleScalarFloor((real_T)c->size[0] / 2.0);
  st.site = &kb_emlrtRSI;
  circshift(&st, c, i8);

  /*  normalize and square the entries */
  st.site = &lb_emlrtRSI;
  y = muDoubleScalarSqrt(cyclelength);
  i8 = x->size[0];
  x->size[0] = c->size[0];
  emxEnsureCapacity_creal_T(sp, x, i8, &fc_emlrtRTEI);
  loop_ub = c->size[0];
  for (i8 = 0; i8 < loop_ub; i8++) {
    c_re = c->data[i8].re;
    c_im = c->data[i8].im;
    if (c_im == 0.0) {
      x->data[i8].re = c_re / y;
      x->data[i8].im = 0.0;
    } else if (c_re == 0.0) {
      x->data[i8].re = 0.0;
      x->data[i8].im = c_im / y;
    } else {
      x->data[i8].re = c_re / y;
      x->data[i8].im = c_im / y;
    }
  }

  emxInit_real_T(sp, &a, 1, &hc_emlrtRTEI, true);
  st.site = &lb_emlrtRSI;
  b_st.site = &cc_emlrtRSI;
  nx = x->size[0];
  loop_ub = x->size[0];
  i8 = a->size[0];
  a->size[0] = loop_ub;
  emxEnsureCapacity_real_T(&b_st, a, i8, &fb_emlrtRTEI);
  c_st.site = &dc_emlrtRSI;
  overflow = ((1 <= x->size[0]) && (x->size[0] > 2147483646));
  if (overflow) {
    d_st.site = &o_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  for (loop_ub = 0; loop_ub < nx; loop_ub++) {
    a->data[loop_ub] = muDoubleScalarHypot(x->data[loop_ub].re, x->data[loop_ub]
      .im);
  }

  emxFree_creal_T(&x);
  emxInit_real_T(&b_st, &b_c, 1, &gc_emlrtRTEI, true);
  st.site = &lb_emlrtRSI;
  b_st.site = &s_emlrtRSI;
  c_st.site = &ec_emlrtRSI;
  a_idx_0 = (uint32_T)a->size[0];
  i8 = b_c->size[0];
  b_c->size[0] = (int32_T)a_idx_0;
  emxEnsureCapacity_real_T(&c_st, b_c, i8, &hb_emlrtRTEI);
  d_st.site = &ac_emlrtRSI;
  a_idx_0 = (uint32_T)a->size[0];
  nx = (int32_T)a_idx_0;
  e_st.site = &bc_emlrtRSI;
  overflow = ((1 <= b_c->size[0]) && (b_c->size[0] > 2147483646));
  if (overflow) {
    f_st.site = &o_emlrtRSI;
    check_forloop_overflow_error(&f_st);
  }

  for (loop_ub = 0; loop_ub < nx; loop_ub++) {
    b_c->data[loop_ub] = a->data[loop_ub] * a->data[loop_ub];
  }

  emxFree_real_T(&a);

  /*  update entries by excecuting convolution */
  i8 = omega->size[0];
  for (loop_ub = 0; loop_ub < i8; loop_ub++) {
    nx = Energydensity->size[0];
    i9 = 1 + loop_ub;
    if ((i9 < 1) || (i9 > nx)) {
      emlrtDynamicBoundsCheckR2012b(i9, 1, nx, &fb_emlrtBCI, sp);
    }

    nx = omega->size[0];
    i10 = 1 + loop_ub;
    if ((i10 < 1) || (i10 > nx)) {
      emlrtDynamicBoundsCheckR2012b(i10, 1, nx, &gb_emlrtBCI, sp);
    }

    nx = Energydensity->size[0];
    i11 = 1 + loop_ub;
    if ((i11 < 1) || (i11 > nx)) {
      emlrtDynamicBoundsCheckR2012b(i11, 1, nx, &hb_emlrtBCI, sp);
    }

    st.site = &mb_emlrtRSI;
    Energydensity->data[i11 - 1] = Energydensity->data[i9 - 1] +
      convolvewithbump(&st, b_c, omega->data[i10 - 1], spect_res, cyclelength,
                       tau);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&b_c);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void Duffing(const emlrtStack *sp, real_T alpha, real_T beta, real_T tau, real_T
             spacing, const real_T Proj_range[2], real_T domain_range, real_T
             spect_res, real_T spect_bandwith, emxArray_real_T *Q,
             emxArray_real_T *P, emxArray_creal_T *Proj_observable,
             emxArray_real_T *omega, emxArray_real_T *Energydensity)
{
  real_T translate;
  emxArray_real_T *qdirection;
  int32_T i0;
  real_T ndbl;
  real_T apnd;
  int32_T loop_ub;
  emxArray_real_T *pdirection;
  real_T cdiff;
  emxArray_real_T *Pnat;
  int32_T n;
  real_T no_gridpoints;
  int32_T nm1d2;
  int32_T k;
  int32_T i1;
  emxArray_real_T *Qnat;
  int32_T b_Q[2];
  int32_T c_Q[2];
  real_T range_circle[2];
  real_T d;
  uint32_T m;
  emxArray_real_T *traj_trace;
  emxArray_uint32_T *trace_insidedomain;
  emxArray_creal_T *observ_trace;
  emxArray_creal_T *c;
  emxArray_int8_T *Flagged;
  emxArray_creal_T *r0;
  emxArray_creal_T *b_observ_trace;
  emxArray_real_T *b_traj_trace;
  emxArray_real_T *b_trace_insidedomain;
  int32_T l;
  int32_T i2;
  boolean_T exitg1;
  boolean_T guard1 = false;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /* %%% Set up grid %%%%% */
  translate = muDoubleScalarRound(domain_range / spacing + 1.0);
  st.site = &k_emlrtRSI;
  emxInit_real_T(&st, &qdirection, 2, &emlrtRTEI, true);
  if (muDoubleScalarIsNaN(-domain_range) || muDoubleScalarIsNaN(spacing) ||
      muDoubleScalarIsNaN(domain_range)) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &emlrtRTEI);
    qdirection->data[0] = rtNaN;
  } else if ((spacing == 0.0) || ((-domain_range < domain_range) && (spacing <
               0.0)) || ((domain_range < -domain_range) && (spacing > 0.0))) {
    qdirection->size[0] = 1;
    qdirection->size[1] = 0;
  } else if ((muDoubleScalarIsInf(-domain_range) || muDoubleScalarIsInf
              (domain_range)) && (muDoubleScalarIsInf(spacing) || (-domain_range
    == domain_range))) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &emlrtRTEI);
    qdirection->data[0] = rtNaN;
  } else if (muDoubleScalarIsInf(spacing)) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &emlrtRTEI);
    qdirection->data[0] = -domain_range;
  } else if ((muDoubleScalarFloor(-domain_range) == -domain_range) &&
             (muDoubleScalarFloor(spacing) == spacing)) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    loop_ub = (int32_T)muDoubleScalarFloor((domain_range - (-domain_range)) /
      spacing);
    qdirection->size[1] = loop_ub + 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &emlrtRTEI);
    for (i0 = 0; i0 <= loop_ub; i0++) {
      qdirection->data[i0] = -domain_range + spacing * (real_T)i0;
    }
  } else {
    b_st.site = &l_emlrtRSI;
    ndbl = muDoubleScalarFloor((domain_range - (-domain_range)) / spacing + 0.5);
    apnd = -domain_range + ndbl * spacing;
    if (spacing > 0.0) {
      cdiff = apnd - domain_range;
    } else {
      cdiff = domain_range - apnd;
    }

    if (muDoubleScalarAbs(cdiff) < 4.4408920985006262E-16 * muDoubleScalarMax
        (muDoubleScalarAbs(-domain_range), muDoubleScalarAbs(domain_range))) {
      ndbl++;
      apnd = domain_range;
    } else if (cdiff > 0.0) {
      apnd = -domain_range + (ndbl - 1.0) * spacing;
    } else {
      ndbl++;
    }

    if (ndbl >= 0.0) {
      n = (int32_T)ndbl;
    } else {
      n = 0;
    }

    c_st.site = &m_emlrtRSI;
    if (ndbl > 2.147483647E+9) {
      emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
        "Coder:MATLAB:pmaxsize", "Coder:MATLAB:pmaxsize", 0);
    }

    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = n;
    emxEnsureCapacity_real_T(&b_st, qdirection, i0, &e_emlrtRTEI);
    if (n > 0) {
      qdirection->data[0] = -domain_range;
      if (n > 1) {
        qdirection->data[n - 1] = apnd;
        nm1d2 = (n - 1) / 2;
        for (k = 0; k <= nm1d2 - 2; k++) {
          ndbl = (1.0 + (real_T)k) * spacing;
          qdirection->data[1 + k] = -domain_range + ndbl;
          qdirection->data[(n - k) - 2] = apnd - ndbl;
        }

        if (nm1d2 << 1 == n - 1) {
          qdirection->data[nm1d2] = (-domain_range + apnd) / 2.0;
        } else {
          ndbl = (real_T)nm1d2 * spacing;
          qdirection->data[nm1d2] = -domain_range + ndbl;
          qdirection->data[nm1d2 + 1] = apnd - ndbl;
        }
      }
    }
  }

  st.site = &j_emlrtRSI;
  emxInit_real_T(&st, &pdirection, 2, &b_emlrtRTEI, true);
  if (muDoubleScalarIsNaN(-domain_range) || muDoubleScalarIsNaN(spacing) ||
      muDoubleScalarIsNaN(domain_range)) {
    i0 = pdirection->size[0] * pdirection->size[1];
    pdirection->size[0] = 1;
    pdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, pdirection, i0, &b_emlrtRTEI);
    pdirection->data[0] = rtNaN;
  } else if ((spacing == 0.0) || ((-domain_range < domain_range) && (spacing <
               0.0)) || ((domain_range < -domain_range) && (spacing > 0.0))) {
    pdirection->size[0] = 1;
    pdirection->size[1] = 0;
  } else if ((muDoubleScalarIsInf(-domain_range) || muDoubleScalarIsInf
              (domain_range)) && (muDoubleScalarIsInf(spacing) || (-domain_range
    == domain_range))) {
    i0 = pdirection->size[0] * pdirection->size[1];
    pdirection->size[0] = 1;
    pdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, pdirection, i0, &b_emlrtRTEI);
    pdirection->data[0] = rtNaN;
  } else if (muDoubleScalarIsInf(spacing)) {
    i0 = pdirection->size[0] * pdirection->size[1];
    pdirection->size[0] = 1;
    pdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, pdirection, i0, &b_emlrtRTEI);
    pdirection->data[0] = -domain_range;
  } else if ((muDoubleScalarFloor(-domain_range) == -domain_range) &&
             (muDoubleScalarFloor(spacing) == spacing)) {
    i0 = pdirection->size[0] * pdirection->size[1];
    pdirection->size[0] = 1;
    pdirection->size[1] = (int32_T)muDoubleScalarFloor((domain_range -
      (-domain_range)) / spacing) + 1;
    emxEnsureCapacity_real_T(&st, pdirection, i0, &b_emlrtRTEI);
    loop_ub = (int32_T)muDoubleScalarFloor((domain_range - (-domain_range)) /
      spacing);
    for (i0 = 0; i0 <= loop_ub; i0++) {
      pdirection->data[i0] = -domain_range + spacing * (real_T)i0;
    }
  } else {
    b_st.site = &l_emlrtRSI;
    ndbl = muDoubleScalarFloor((domain_range - (-domain_range)) / spacing + 0.5);
    apnd = -domain_range + ndbl * spacing;
    if (spacing > 0.0) {
      cdiff = apnd - domain_range;
    } else {
      cdiff = domain_range - apnd;
    }

    if (muDoubleScalarAbs(cdiff) < 4.4408920985006262E-16 * muDoubleScalarMax
        (muDoubleScalarAbs(-domain_range), muDoubleScalarAbs(domain_range))) {
      ndbl++;
      apnd = domain_range;
    } else if (cdiff > 0.0) {
      apnd = -domain_range + (ndbl - 1.0) * spacing;
    } else {
      ndbl++;
    }

    if (ndbl >= 0.0) {
      n = (int32_T)ndbl;
    } else {
      n = 0;
    }

    c_st.site = &m_emlrtRSI;
    if (ndbl > 2.147483647E+9) {
      emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
        "Coder:MATLAB:pmaxsize", "Coder:MATLAB:pmaxsize", 0);
    }

    i0 = pdirection->size[0] * pdirection->size[1];
    pdirection->size[0] = 1;
    pdirection->size[1] = n;
    emxEnsureCapacity_real_T(&b_st, pdirection, i0, &e_emlrtRTEI);
    if (n > 0) {
      pdirection->data[0] = -domain_range;
      if (n > 1) {
        pdirection->data[n - 1] = apnd;
        nm1d2 = (n - 1) / 2;
        for (k = 0; k <= nm1d2 - 2; k++) {
          ndbl = (1.0 + (real_T)k) * spacing;
          pdirection->data[1 + k] = -domain_range + ndbl;
          pdirection->data[(n - k) - 2] = apnd - ndbl;
        }

        if (nm1d2 << 1 == n - 1) {
          pdirection->data[nm1d2] = (-domain_range + apnd) / 2.0;
        } else {
          ndbl = (real_T)nm1d2 * spacing;
          pdirection->data[nm1d2] = -domain_range + ndbl;
          pdirection->data[nm1d2 + 1] = apnd - ndbl;
        }
      }
    }
  }

  emxInit_real_T(&st, &Pnat, 2, &d_emlrtRTEI, true);
  st.site = &i_emlrtRSI;
  meshgrid(&st, qdirection, pdirection, Q, P);
  n = Q->size[0];
  st.site = &h_emlrtRSI;
  b_st.site = &r_emlrtRSI;
  no_gridpoints = (real_T)Q->size[0] * (real_T)Q->size[0];
  i0 = Pnat->size[0] * Pnat->size[1];
  Pnat->size[0] = P->size[0];
  Pnat->size[1] = P->size[1];
  emxEnsureCapacity_real_T(sp, Pnat, i0, &c_emlrtRTEI);
  loop_ub = P->size[0] * P->size[1];
  emxFree_real_T(&pdirection);
  for (i0 = 0; i0 < loop_ub; i0++) {
    Pnat->data[i0] = P->data[i0] / spacing;
  }

  st.site = &g_emlrtRSI;
  b_round(&st, Pnat);
  i0 = Pnat->size[0] * Pnat->size[1];
  i1 = Pnat->size[0] * Pnat->size[1];
  emxEnsureCapacity_real_T(sp, Pnat, i1, &d_emlrtRTEI);
  loop_ub = i0 - 1;
  for (i0 = 0; i0 <= loop_ub; i0++) {
    Pnat->data[i0] += translate;
  }

  emxInit_real_T(sp, &Qnat, 2, &g_emlrtRTEI, true);
  st.site = &g_emlrtRSI;
  b_round(&st, Pnat);
  i0 = Qnat->size[0] * Qnat->size[1];
  Qnat->size[0] = Q->size[0];
  Qnat->size[1] = Q->size[1];
  emxEnsureCapacity_real_T(sp, Qnat, i0, &f_emlrtRTEI);
  loop_ub = Q->size[0] * Q->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    Qnat->data[i0] = Q->data[i0] / spacing;
  }

  st.site = &g_emlrtRSI;
  b_round(&st, Qnat);
  i0 = Qnat->size[0] * Qnat->size[1];
  i1 = Qnat->size[0] * Qnat->size[1];
  emxEnsureCapacity_real_T(sp, Qnat, i1, &g_emlrtRTEI);
  loop_ub = i0 - 1;
  for (i0 = 0; i0 <= loop_ub; i0++) {
    Qnat->data[i0] += translate;
  }

  st.site = &g_emlrtRSI;
  b_round(&st, Qnat);

  /* %%% Define spectral variables %%%% */
  b_Q[0] = Q->size[0];
  b_Q[1] = Q->size[0];
  c_Q[0] = Q->size[0];
  c_Q[1] = Q->size[0];
  if ((b_Q[0] != c_Q[0]) || (b_Q[1] != c_Q[1])) {
    emlrtSizeEqCheckNDR2012b(&b_Q[0], &c_Q[0], &b_emlrtECI, sp);
  }

  i0 = Proj_observable->size[0] * Proj_observable->size[1];
  Proj_observable->size[0] = Q->size[0];
  Proj_observable->size[1] = Q->size[0];
  emxEnsureCapacity_creal_T(sp, Proj_observable, i0, &h_emlrtRTEI);
  loop_ub = Q->size[0] * Q->size[0];
  for (i0 = 0; i0 < loop_ub; i0++) {
    Proj_observable->data[i0].re = 0.0;
    Proj_observable->data[i0].im = 0.0;
  }

  range_circle[0] = tau * Proj_range[0];
  range_circle[1] = tau * Proj_range[1];
  st.site = &f_emlrtRSI;
  d = spect_bandwith / muDoubleScalarFloor(1.5 * spect_bandwith / spect_res);
  if (muDoubleScalarIsNaN(d) || muDoubleScalarIsNaN(spect_bandwith)) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &i_emlrtRTEI);
    qdirection->data[0] = rtNaN;
  } else if ((d == 0.0) || ((0.0 < spect_bandwith) && (d < 0.0)) ||
             ((spect_bandwith < 0.0) && (d > 0.0))) {
    qdirection->size[0] = 1;
    qdirection->size[1] = 0;
  } else if (muDoubleScalarIsInf(spect_bandwith) && (muDoubleScalarIsInf(d) ||
              (0.0 == spect_bandwith))) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &l_emlrtRTEI);
    qdirection->data[0] = rtNaN;
  } else if (muDoubleScalarIsInf(d)) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &k_emlrtRTEI);
    qdirection->data[0] = 0.0;
  } else if (muDoubleScalarFloor(d) == d) {
    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    loop_ub = (int32_T)muDoubleScalarFloor(spect_bandwith / d);
    qdirection->size[1] = loop_ub + 1;
    emxEnsureCapacity_real_T(&st, qdirection, i0, &m_emlrtRTEI);
    for (i0 = 0; i0 <= loop_ub; i0++) {
      qdirection->data[i0] = d * (real_T)i0;
    }
  } else {
    b_st.site = &l_emlrtRSI;
    ndbl = muDoubleScalarFloor(spect_bandwith / d + 0.5);
    apnd = ndbl * d;
    if (d > 0.0) {
      cdiff = apnd - spect_bandwith;
    } else {
      cdiff = spect_bandwith - apnd;
    }

    if (muDoubleScalarAbs(cdiff) < 4.4408920985006262E-16 * muDoubleScalarAbs
        (spect_bandwith)) {
      ndbl++;
      apnd = spect_bandwith;
    } else if (cdiff > 0.0) {
      apnd = (ndbl - 1.0) * d;
    } else {
      ndbl++;
    }

    if (ndbl >= 0.0) {
      loop_ub = (int32_T)ndbl;
    } else {
      loop_ub = 0;
    }

    c_st.site = &m_emlrtRSI;
    if (ndbl > 2.147483647E+9) {
      emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
        "Coder:MATLAB:pmaxsize", "Coder:MATLAB:pmaxsize", 0);
    }

    i0 = qdirection->size[0] * qdirection->size[1];
    qdirection->size[0] = 1;
    qdirection->size[1] = loop_ub;
    emxEnsureCapacity_real_T(&b_st, qdirection, i0, &e_emlrtRTEI);
    if (loop_ub > 0) {
      qdirection->data[0] = 0.0;
      if (loop_ub > 1) {
        qdirection->data[loop_ub - 1] = apnd;
        nm1d2 = (loop_ub - 1) / 2;
        for (k = 0; k <= nm1d2 - 2; k++) {
          ndbl = (1.0 + (real_T)k) * d;
          qdirection->data[1 + k] = ndbl;
          qdirection->data[(loop_ub - k) - 2] = apnd - ndbl;
        }

        if (nm1d2 << 1 == loop_ub - 1) {
          qdirection->data[nm1d2] = apnd / 2.0;
        } else {
          ndbl = (real_T)nm1d2 * d;
          qdirection->data[nm1d2] = ndbl;
          qdirection->data[nm1d2 + 1] = apnd - ndbl;
        }
      }
    }
  }

  i0 = omega->size[0];
  omega->size[0] = qdirection->size[1];
  emxEnsureCapacity_real_T(sp, omega, i0, &j_emlrtRTEI);
  loop_ub = qdirection->size[1];
  for (i0 = 0; i0 < loop_ub; i0++) {
    omega->data[i0] = qdirection->data[i0];
  }

  emxFree_real_T(&qdirection);
  m = (uint32_T)omega->size[0];
  i0 = Energydensity->size[0];
  Energydensity->size[0] = (int32_T)m;
  emxEnsureCapacity_real_T(sp, Energydensity, i0, &n_emlrtRTEI);
  loop_ub = (int32_T)m;
  for (i0 = 0; i0 < loop_ub; i0++) {
    Energydensity->data[i0] = 0.0;
  }

  emxInit_real_T(sp, &traj_trace, 2, &o_emlrtRTEI, true);

  /* %%% allocate memory %%%% */
  i0 = traj_trace->size[0] * traj_trace->size[1];
  loop_ub = (int32_T)no_gridpoints;
  if (no_gridpoints != loop_ub) {
    emlrtIntegerCheckR2012b(no_gridpoints, &c_emlrtDCI, sp);
  }

  traj_trace->size[0] = loop_ub;
  traj_trace->size[1] = 2;
  emxEnsureCapacity_real_T(sp, traj_trace, i0, &o_emlrtRTEI);
  if (no_gridpoints != (int32_T)no_gridpoints) {
    emlrtIntegerCheckR2012b(no_gridpoints, &d_emlrtDCI, sp);
  }

  nm1d2 = loop_ub << 1;
  for (i0 = 0; i0 < nm1d2; i0++) {
    traj_trace->data[i0] = 0.0;
  }

  emxInit_uint32_T(sp, &trace_insidedomain, 1, &p_emlrtRTEI, true);
  i0 = trace_insidedomain->size[0];
  if (no_gridpoints != (int32_T)no_gridpoints) {
    emlrtIntegerCheckR2012b(no_gridpoints, &e_emlrtDCI, sp);
  }

  trace_insidedomain->size[0] = loop_ub;
  emxEnsureCapacity_uint32_T(sp, trace_insidedomain, i0, &p_emlrtRTEI);
  if (no_gridpoints != (int32_T)no_gridpoints) {
    emlrtIntegerCheckR2012b(no_gridpoints, &e_emlrtDCI, sp);
  }

  for (i0 = 0; i0 < loop_ub; i0++) {
    trace_insidedomain->data[i0] = 0U;
  }

  emxInit_creal_T(sp, &observ_trace, 1, &q_emlrtRTEI, true);
  if (no_gridpoints != (int32_T)no_gridpoints) {
    emlrtIntegerCheckR2012b(no_gridpoints, &b_emlrtDCI, sp);
  }

  i0 = observ_trace->size[0];
  observ_trace->size[0] = loop_ub;
  emxEnsureCapacity_creal_T(sp, observ_trace, i0, &q_emlrtRTEI);
  for (i0 = 0; i0 < loop_ub; i0++) {
    observ_trace->data[i0].re = 0.0;
    observ_trace->data[i0].im = 0.0;
  }

  emxInit_creal_T(sp, &c, 1, &r_emlrtRTEI, true);
  if (no_gridpoints != (int32_T)no_gridpoints) {
    emlrtIntegerCheckR2012b(no_gridpoints, &emlrtDCI, sp);
  }

  i0 = c->size[0];
  c->size[0] = loop_ub;
  emxEnsureCapacity_creal_T(sp, c, i0, &r_emlrtRTEI);
  for (i0 = 0; i0 < loop_ub; i0++) {
    c->data[i0].re = 0.0;
    c->data[i0].im = 0.0;
  }

  emxInit_int8_T(sp, &Flagged, 2, &s_emlrtRTEI, true);
  i0 = Flagged->size[0] * Flagged->size[1];
  Flagged->size[0] = Q->size[0];
  Flagged->size[1] = Q->size[0];
  emxEnsureCapacity_int8_T(sp, Flagged, i0, &s_emlrtRTEI);
  loop_ub = Q->size[0] * Q->size[0];
  for (i0 = 0; i0 < loop_ub; i0++) {
    Flagged->data[i0] = 0;
  }

  i0 = Q->size[0] * Q->size[1];
  emxInit_creal_T(sp, &r0, 1, &w_emlrtRTEI, true);
  emxInit_creal_T(sp, &b_observ_trace, 1, &t_emlrtRTEI, true);
  emxInit_real_T(sp, &b_traj_trace, 2, &v_emlrtRTEI, true);
  emxInit_real_T(sp, &b_trace_insidedomain, 1, &w_emlrtRTEI, true);
  for (l = 0; l < i0; l++) {
    i1 = Flagged->size[0];
    loop_ub = Qnat->size[0] * Qnat->size[1];
    i2 = 1 + l;
    if ((i2 < 1) || (i2 > loop_ub)) {
      emlrtDynamicBoundsCheckR2012b(i2, 1, loop_ub, &o_emlrtBCI, sp);
    }

    ndbl = Qnat->data[i2 - 1];
    if (ndbl != (int32_T)muDoubleScalarFloor(ndbl)) {
      emlrtIntegerCheckR2012b(ndbl, &f_emlrtDCI, sp);
    }

    loop_ub = (int32_T)ndbl;
    if ((loop_ub < 1) || (loop_ub > i1)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &n_emlrtBCI, sp);
    }

    i1 = Flagged->size[1];
    i2 = Pnat->size[0] * Pnat->size[1];
    nm1d2 = 1 + l;
    if ((nm1d2 < 1) || (nm1d2 > i2)) {
      emlrtDynamicBoundsCheckR2012b(nm1d2, 1, i2, &p_emlrtBCI, sp);
    }

    ndbl = Pnat->data[nm1d2 - 1];
    if (ndbl != (int32_T)muDoubleScalarFloor(ndbl)) {
      emlrtIntegerCheckR2012b(ndbl, &f_emlrtDCI, sp);
    }

    i2 = (int32_T)ndbl;
    if ((i2 < 1) || (i2 > i1)) {
      emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &n_emlrtBCI, sp);
    }

    i1 = Flagged->data[(loop_ub + Flagged->size[0] * (i2 - 1)) - 1];
    st.site = &e_emlrtRSI;
    toLogicalCheck(&st, i1);
    if (i1 == 0) {
      /*  Simulate trajectory until it hits starting point */
      i1 = Qnat->size[0] * Qnat->size[1];
      loop_ub = 1 + l;
      if ((loop_ub < 1) || (loop_ub > i1)) {
        emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &q_emlrtBCI, sp);
      }

      ndbl = Qnat->data[loop_ub - 1];
      i1 = Pnat->size[0] * Pnat->size[1];
      loop_ub = 1 + l;
      if ((loop_ub < 1) || (loop_ub > i1)) {
        emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &r_emlrtBCI, sp);
      }

      cdiff = Pnat->data[loop_ub - 1];
      m = 1U;
      k = 1;
      exitg1 = false;
      while ((!exitg1) && (k - 1 <= Q->size[0] * Q->size[1] - 1)) {
        if ((ndbl > 0.0) && (ndbl <= n) && (cdiff > 0.0) && (cdiff <= n)) {
          i1 = Flagged->size[0];
          if (ndbl != muDoubleScalarFloor(ndbl)) {
            emlrtIntegerCheckR2012b(ndbl, &g_emlrtDCI, sp);
          }

          loop_ub = (int32_T)ndbl;
          if ((loop_ub < 1) || (loop_ub > i1)) {
            emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &s_emlrtBCI, sp);
          }

          i1 = Flagged->size[1];
          if (cdiff != muDoubleScalarFloor(cdiff)) {
            emlrtIntegerCheckR2012b(cdiff, &g_emlrtDCI, sp);
          }

          i2 = (int32_T)cdiff;
          if ((i2 < 1) || (i2 > i1)) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &s_emlrtBCI, sp);
          }

          Flagged->data[(loop_ub + Flagged->size[0] * (i2 - 1)) - 1] = 1;
          i1 = trace_insidedomain->size[0];
          if (((int32_T)m < 1) || ((int32_T)m > i1)) {
            emlrtDynamicBoundsCheckR2012b((int32_T)m, 1, i1, &t_emlrtBCI, sp);
          }

          trace_insidedomain->data[(int32_T)m - 1] = (uint32_T)k;
          m++;
        }

        /*  update trajectory trace */
        i1 = traj_trace->size[0];
        if ((k < 1) || (k > i1)) {
          emlrtDynamicBoundsCheckR2012b(k, 1, i1, &g_emlrtBCI, sp);
        }

        traj_trace->data[k - 1] = ndbl;
        traj_trace->data[(k + traj_trace->size[0]) - 1] = cdiff;

        /*  Propagate forward with periodic approximation */
        st.site = &d_emlrtRSI;
        periodic_apprx(alpha, beta, ndbl, cdiff, translate, tau, spacing, &apnd,
                       &no_gridpoints);
        ndbl = apnd;
        cdiff = no_gridpoints;

        /*  update trace */
        /* %%%%%  Observable  %%%%%% */
        /*  + 0.25*q^4; */
        i1 = observ_trace->size[0];
        if (k > i1) {
          emlrtDynamicBoundsCheckR2012b(k, 1, i1, &u_emlrtBCI, sp);
        }

        observ_trace->data[k - 1].re = (no_gridpoints - translate) * spacing;
        i1 = observ_trace->size[0];
        if (k > i1) {
          emlrtDynamicBoundsCheckR2012b(k, 1, i1, &u_emlrtBCI, sp);
        }

        observ_trace->data[k - 1].im = 0.0;
        i1 = Pnat->size[0] * Pnat->size[1];
        loop_ub = 1 + l;
        if ((loop_ub < 1) || (loop_ub > i1)) {
          emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &v_emlrtBCI, sp);
        }

        guard1 = false;
        if (no_gridpoints == Pnat->data[loop_ub - 1]) {
          i1 = Qnat->size[0] * Qnat->size[1];
          loop_ub = 1 + l;
          if ((loop_ub < 1) || (loop_ub > i1)) {
            emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &w_emlrtBCI, sp);
          }

          if (apnd == Qnat->data[loop_ub - 1]) {
            /* %% Cycle has completed: perform spectral calculations %%% */
            /*  map cycle into frequency domain */
            i1 = observ_trace->size[0];
            if (1 > i1) {
              emlrtDynamicBoundsCheckR2012b(1, 1, i1, &m_emlrtBCI, sp);
            }

            i1 = observ_trace->size[0];
            if (k > i1) {
              emlrtDynamicBoundsCheckR2012b(k, 1, i1, &l_emlrtBCI, sp);
            }

            b_Q[0] = 1;
            b_Q[1] = k;
            st.site = &c_emlrtRSI;
            indexShapeCheck(&st, observ_trace->size[0], b_Q);
            i1 = c->size[0];
            if (1 > i1) {
              emlrtDynamicBoundsCheckR2012b(1, 1, i1, &k_emlrtBCI, sp);
            }

            i1 = c->size[0];
            if (k > i1) {
              emlrtDynamicBoundsCheckR2012b(k, 1, i1, &j_emlrtBCI, sp);
            }

            i1 = b_observ_trace->size[0];
            b_observ_trace->size[0] = k;
            emxEnsureCapacity_creal_T(sp, b_observ_trace, i1, &t_emlrtRTEI);
            for (i1 = 0; i1 < k; i1++) {
              b_observ_trace->data[i1] = observ_trace->data[i1];
            }

            st.site = &c_emlrtRSI;
            fft(&st, b_observ_trace, r0);
            i1 = r0->size[0];
            if (k != i1) {
              emlrtSubAssignSizeCheck1dR2017a(k, i1, &emlrtECI, sp);
            }

            for (i1 = 0; i1 < k; i1++) {
              c->data[i1] = r0->data[i1];
            }

            /*  update spectral projection plot */
            i1 = c->size[0];
            if (1 > i1) {
              emlrtDynamicBoundsCheckR2012b(1, 1, i1, &i_emlrtBCI, sp);
            }

            i1 = c->size[0];
            if (k > i1) {
              emlrtDynamicBoundsCheckR2012b(k, 1, i1, &h_emlrtBCI, sp);
            }

            b_Q[0] = 1;
            b_Q[1] = k;
            st.site = &b_emlrtRSI;
            indexShapeCheck(&st, c->size[0], b_Q);
            i1 = traj_trace->size[0];
            if (1 > i1) {
              emlrtDynamicBoundsCheckR2012b(1, 1, i1, &f_emlrtBCI, sp);
            }

            i1 = traj_trace->size[0];
            if (k > i1) {
              emlrtDynamicBoundsCheckR2012b(k, 1, i1, &e_emlrtBCI, sp);
            }

            if (1 > (int32_T)(m - 1U)) {
              loop_ub = 0;
            } else {
              i1 = trace_insidedomain->size[0];
              if (1 > i1) {
                emlrtDynamicBoundsCheckR2012b(1, 1, i1, &d_emlrtBCI, sp);
              }

              i1 = trace_insidedomain->size[0];
              loop_ub = (int32_T)(m - 1U);
              if ((loop_ub < 1) || (loop_ub > i1)) {
                emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &c_emlrtBCI, sp);
              }
            }

            b_Q[0] = 1;
            b_Q[1] = loop_ub;
            st.site = &b_emlrtRSI;
            indexShapeCheck(&st, trace_insidedomain->size[0], b_Q);
            i1 = b_observ_trace->size[0];
            b_observ_trace->size[0] = k;
            emxEnsureCapacity_creal_T(sp, b_observ_trace, i1, &u_emlrtRTEI);
            for (i1 = 0; i1 < k; i1++) {
              b_observ_trace->data[i1] = c->data[i1];
            }

            i1 = b_traj_trace->size[0] * b_traj_trace->size[1];
            b_traj_trace->size[0] = k;
            b_traj_trace->size[1] = 2;
            emxEnsureCapacity_real_T(sp, b_traj_trace, i1, &v_emlrtRTEI);
            for (i1 = 0; i1 < k; i1++) {
              b_traj_trace->data[i1] = traj_trace->data[i1];
            }

            for (i1 = 0; i1 < k; i1++) {
              b_traj_trace->data[i1 + b_traj_trace->size[0]] = traj_trace->
                data[i1 + traj_trace->size[0]];
            }

            i1 = b_trace_insidedomain->size[0];
            b_trace_insidedomain->size[0] = loop_ub;
            emxEnsureCapacity_real_T(sp, b_trace_insidedomain, i1, &w_emlrtRTEI);
            for (i1 = 0; i1 < loop_ub; i1++) {
              b_trace_insidedomain->data[i1] = trace_insidedomain->data[i1];
            }

            st.site = &b_emlrtRSI;
            update_SpectralProj(&st, Proj_observable, b_observ_trace,
                                b_traj_trace, range_circle, b_trace_insidedomain,
                                n);

            /*  update spectral density plot */
            i1 = c->size[0];
            if (1 > i1) {
              emlrtDynamicBoundsCheckR2012b(1, 1, i1, &b_emlrtBCI, sp);
            }

            i1 = c->size[0];
            if (k > i1) {
              emlrtDynamicBoundsCheckR2012b(k, 1, i1, &emlrtBCI, sp);
            }

            b_Q[0] = 1;
            b_Q[1] = k;
            st.site = &emlrtRSI;
            indexShapeCheck(&st, c->size[0], b_Q);
            i1 = b_observ_trace->size[0];
            b_observ_trace->size[0] = k;
            emxEnsureCapacity_creal_T(sp, b_observ_trace, i1, &x_emlrtRTEI);
            for (i1 = 0; i1 < k; i1++) {
              b_observ_trace->data[i1] = c->data[i1];
            }

            st.site = &emlrtRSI;
            update_spectraldensity(&st, Energydensity, b_observ_trace, 1.0 +
              ((real_T)k - 1.0), omega, spect_res, tau);
            exitg1 = true;
          } else {
            guard1 = true;
          }
        } else {
          guard1 = true;
        }

        if (guard1) {
          k++;
          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&b_trace_insidedomain);
  emxFree_real_T(&b_traj_trace);
  emxFree_creal_T(&b_observ_trace);
  emxFree_creal_T(&r0);
  emxFree_int8_T(&Flagged);
  emxFree_creal_T(&c);
  emxFree_creal_T(&observ_trace);
  emxFree_uint32_T(&trace_insidedomain);
  emxFree_real_T(&traj_trace);
  emxFree_real_T(&Qnat);
  emxFree_real_T(&Pnat);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (Duffing.c) */
