/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Duffing_data.c
 *
 * Code generation for function 'Duffing_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "Duffing_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131482U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "Duffing",                           /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo o_emlrtRSI = { 21,         /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pathName */
};

emlrtRSInfo u_emlrtRSI = { 31,         /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\applyScalarFunctionInPlace.m"/* pathName */
};

emlrtRSInfo v_emlrtRSI = { 98,         /* lineNo */
  "periodic_apprx",                    /* fcnName */
  "C:\\Users\\ngovinda\\Documents\\koopman-periodic-approximation\\core\\Hamiltonian-systems\\functions\\Duffing.m"/* pathName */
};

emlrtRSInfo y_emlrtRSI = { 102,        /* lineNo */
  "fft",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\fft.m"/* pathName */
};

emlrtRSInfo ab_emlrtRSI = { 18,        /* lineNo */
  "fftw",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\datafun\\fftw.m"/* pathName */
};

emlrtRSInfo bb_emlrtRSI = { 28,        /* lineNo */
  "FFTWApi/fft1d",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+fftw\\FFTWApi.m"/* pathName */
};

emlrtRSInfo db_emlrtRSI = { 17,        /* lineNo */
  "MATLABFFTWCallback/fft1d",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+fftw\\MATLABFFTWCallback.m"/* pathName */
};

emlrtRSInfo ib_emlrtRSI = { 39,        /* lineNo */
  "eml_sub2ind",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pathName */
};

emlrtRSInfo jb_emlrtRSI = { 71,        /* lineNo */
  "prodsub",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pathName */
};

emlrtRSInfo qc_emlrtRSI = { 83,        /* lineNo */
  "ceval_xdot",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdot.m"/* pathName */
};

emlrtRSInfo rc_emlrtRSI = { 84,        /* lineNo */
  "ceval_xdot",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdot.m"/* pathName */
};

emlrtRSInfo sc_emlrtRSI = { 85,        /* lineNo */
  "ceval_xdot",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+blas\\xdot.m"/* pathName */
};

/* End of code generation (Duffing_data.c) */
