/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ifft.c
 *
 * Code generation for function 'ifft'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "ifft.h"
#include "FFTWApi.h"
#include "Duffing_data.h"

/* Variable Definitions */
static emlrtRSInfo gb_emlrtRSI = { 19, /* lineNo */
  "ifft",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\datafun\\ifft.m"/* pathName */
};

/* Function Definitions */
void ifft(const emlrtStack *sp, const emxArray_creal_T *x, emxArray_creal_T *y)
{
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gb_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &y_emlrtRSI;
  b_FFTWApi_fft1d(&b_st, x, x->size[0], y);
}

/* End of code generation (ifft.c) */
