/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Duffing.h
 *
 * Code generation for function 'Duffing'
 *
 */

#ifndef DUFFING_H
#define DUFFING_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Duffing_types.h"

/* Function Declarations */
extern void Duffing(const emlrtStack *sp, real_T alpha, real_T beta, real_T tau,
                    real_T spacing, const real_T Proj_range[2], real_T
                    domain_range, real_T spect_res, real_T spect_bandwith,
                    emxArray_real_T *Q, emxArray_real_T *P, emxArray_creal_T
                    *Proj_observable, emxArray_real_T *omega, emxArray_real_T
                    *Energydensity);

#endif

/* End of code generation (Duffing.h) */
