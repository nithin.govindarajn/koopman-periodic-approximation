/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * circshift.c
 *
 * Code generation for function 'circshift'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "circshift.h"
#include "Duffing_emxutil.h"
#include "eml_int_forloop_overflow_check.h"
#include "Duffing_data.h"

/* Variable Definitions */
static emlrtRSInfo nb_emlrtRSI = { 32, /* lineNo */
  "circshift",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 43, /* lineNo */
  "circshift_single_dim",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo pb_emlrtRSI = { 56, /* lineNo */
  "circshift_single_dim",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 59, /* lineNo */
  "circshift_single_dim",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 49, /* lineNo */
  "prodsize",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\prodsize.m"/* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 96, /* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 98, /* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 102,/* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 110,/* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo wb_emlrtRSI = { 115,/* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo xb_emlrtRSI = { 119,/* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRSInfo yb_emlrtRSI = { 123,/* lineNo */
  "circshift_core",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pathName */
};

static emlrtRTEInfo ic_emlrtRTEI = { 54,/* lineNo */
  1,                                   /* colNo */
  "circshift",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pName */
};

static emlrtRTEInfo qc_emlrtRTEI = { 29,/* lineNo */
  48,                                  /* colNo */
  "circshift",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\circshift.m"/* pName */
};

static emlrtRSInfo tc_emlrtRSI = { 133,/* lineNo */
  "eml_scalar_rdivide",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\idivide.m"/* pathName */
};

/* Function Declarations */
static int32_T _s32_u32_(const emlrtStack *sp, uint32_T b);
static int32_T divv_s32_sat(const emlrtStack *sp, int32_T numerator, int32_T
  denominator);

/* Function Definitions */
static int32_T _s32_u32_(const emlrtStack *sp, uint32_T b)
{
  int32_T a;
  a = (int32_T)b;
  if (a < 0) {
    emlrtIntegerOverflowErrorR2012b(NULL, sp);
  }

  return a;
}

static int32_T divv_s32_sat(const emlrtStack *sp, int32_T numerator, int32_T
  denominator)
{
  int32_T quotient;
  boolean_T quotientNeedsNegation;
  uint32_T b_numerator;
  uint32_T b_denominator;
  uint32_T tempAbsQuotient;
  if (denominator == 0) {
    emlrtDivisionByZeroErrorR2012b(NULL, sp);
  } else {
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    if (numerator < 0) {
      b_numerator = ~(uint32_T)numerator + 1U;
    } else {
      b_numerator = (uint32_T)numerator;
    }

    if (denominator < 0) {
      b_denominator = ~(uint32_T)denominator + 1U;
    } else {
      b_denominator = (uint32_T)denominator;
    }

    tempAbsQuotient = b_numerator / b_denominator;
    if ((!quotientNeedsNegation) && (tempAbsQuotient >= 2147483647U)) {
      quotient = MAX_int32_T;
    } else if (quotientNeedsNegation && (tempAbsQuotient > 2147483647U)) {
      quotient = MIN_int32_T;
    } else if (quotientNeedsNegation) {
      quotient = -(int32_T)tempAbsQuotient;
    } else {
      quotient = _s32_u32_(sp, tempAbsQuotient);
    }
  }

  return quotient;
}

void circshift(const emlrtStack *sp, emxArray_creal_T *a, real_T p)
{
  int32_T dim;
  boolean_T pok;
  int32_T ns;
  int32_T i12;
  emxArray_creal_T *buffer;
  int32_T loop_ub;
  int32_T nv;
  int32_T stride;
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  dim = 2;
  if (a->size[0] != 1) {
    dim = 1;
  }

  pok = true;
  ns = (int32_T)p;
  if (ns != p) {
    pok = false;
  }

  if (!pok) {
    emlrtErrorWithMessageIdR2018a(sp, &qc_emlrtRTEI,
      "Coder:toolbox:circshift_InvalidShiftType",
      "Coder:toolbox:circshift_InvalidShiftType", 6, 4, 5, "int32", 4, 5,
      "int32");
  }

  if (a->size[0] != 1) {
    st.site = &nb_emlrtRSI;
    b_st.site = &ob_emlrtRSI;
    pok = true;
    if (dim <= 1) {
      i12 = a->size[0];
    } else {
      i12 = 1;
    }

    if (ns > i12) {
      b_st.site = &tc_emlrtRSI;
      ns -= divv_s32_sat(&b_st, ns, i12) * i12;
    }

    if (ns > (i12 >> 1)) {
      ns = i12 - ns;
      pok = false;
    }

    emxInit_creal_T(&st, &buffer, 2, &ic_emlrtRTEI, true);
    loop_ub = (int32_T)muDoubleScalarFloor((real_T)a->size[0] / 2.0);
    i12 = buffer->size[0] * buffer->size[1];
    buffer->size[0] = 1;
    buffer->size[1] = loop_ub;
    emxEnsureCapacity_creal_T(&st, buffer, i12, &ic_emlrtRTEI);
    for (i12 = 0; i12 < loop_ub; i12++) {
      buffer->data[i12].re = 0.0;
      buffer->data[i12].im = 0.0;
    }

    if (dim <= 1) {
      nv = a->size[0];
    } else {
      nv = 1;
    }

    b_st.site = &pb_emlrtRSI;
    stride = 1;
    c_st.site = &rb_emlrtRSI;
    for (k = 0; k <= dim - 2; k++) {
      stride *= a->size[0];
    }

    b_st.site = &qb_emlrtRSI;
    if ((nv > 1) && (ns > 0)) {
      c_st.site = &sb_emlrtRSI;
      c_st.site = &tb_emlrtRSI;
      if ((1 <= stride) && (stride > 2147483646)) {
        d_st.site = &o_emlrtRSI;
        check_forloop_overflow_error(&d_st);
      }

      for (dim = 0; dim < stride; dim++) {
        if (pok) {
          c_st.site = &ub_emlrtRSI;
          if (ns > 2147483646) {
            d_st.site = &o_emlrtRSI;
            check_forloop_overflow_error(&d_st);
          }

          for (k = 0; k < ns; k++) {
            buffer->data[k] = a->data[dim + ((k + nv) - ns) * stride];
          }

          i12 = ns + 1;
          for (k = nv; k >= i12; k--) {
            a->data[dim + (k - 1) * stride] = a->data[dim + ((k - ns) - 1) *
              stride];
          }

          c_st.site = &vb_emlrtRSI;
          for (k = 0; k < ns; k++) {
            a->data[dim + k * stride] = buffer->data[k];
          }
        } else {
          c_st.site = &wb_emlrtRSI;
          if (ns > 2147483646) {
            d_st.site = &o_emlrtRSI;
            check_forloop_overflow_error(&d_st);
          }

          for (k = 0; k < ns; k++) {
            buffer->data[k] = a->data[dim + k * stride];
          }

          loop_ub = nv - ns;
          c_st.site = &xb_emlrtRSI;
          for (k = 0; k < loop_ub; k++) {
            a->data[dim + k * stride] = a->data[dim + (k + ns) * stride];
          }

          c_st.site = &yb_emlrtRSI;
          for (k = 0; k < ns; k++) {
            a->data[dim + ((k + nv) - ns) * stride] = buffer->data[k];
          }
        }
      }
    }

    emxFree_creal_T(&buffer);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (circshift.c) */
