/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Duffing_initialize.c
 *
 * Code generation for function 'Duffing_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "Duffing_initialize.h"
#include "_coder_Duffing_mex.h"
#include "Duffing_data.h"

/* Function Definitions */
void Duffing_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (Duffing_initialize.c) */
