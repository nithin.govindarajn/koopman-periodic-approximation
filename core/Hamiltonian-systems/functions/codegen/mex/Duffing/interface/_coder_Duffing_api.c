/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Duffing_api.c
 *
 * Code generation for function '_coder_Duffing_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "_coder_Duffing_api.h"
#include "Duffing_emxutil.h"
#include "Duffing_data.h"

/* Variable Definitions */
static emlrtRTEInfo mb_emlrtRTEI = { 1,/* lineNo */
  1,                                   /* colNo */
  "_coder_Duffing_api",                /* fName */
  ""                                   /* pName */
};

/* Function Declarations */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_creal_T *u);
static real_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *Proj_range, const char_T *identifier))[2];
static const mxArray *c_emlrt_marshallOut(const emxArray_real_T *u);
static real_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[2];
static real_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *alpha, const
  char_T *identifier);
static const mxArray *emlrt_marshallOut(const emxArray_real_T *u);
static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[2];

/* Function Definitions */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = e_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_creal_T *u)
{
  const mxArray *y;
  const mxArray *m1;
  y = NULL;
  m1 = emlrtCreateNumericArray(2, *(int32_T (*)[2])u->size, mxDOUBLE_CLASS,
    mxCOMPLEX);
  emlrtExportNumericArrayR2013b(sp, m1, &u->data[0], 8);
  emlrtAssign(&y, m1);
  return y;
}

static real_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *Proj_range, const char_T *identifier))[2]
{
  real_T (*y)[2];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(Proj_range), &thisId);
  emlrtDestroyArray(&Proj_range);
  return y;
}
  static const mxArray *c_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv2[1] = { 0 };

  y = NULL;
  m2 = emlrtCreateNumericArray(1, iv2, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m2, &u->data[0]);
  emlrtSetDimensions((mxArray *)m2, u->size, 1);
  emlrtAssign(&y, m2);
  return y;
}

static real_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[2]
{
  real_T (*y)[2];
  y = f_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *alpha, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(alpha), &thisId);
  emlrtDestroyArray(&alpha);
  return y;
}

static const mxArray *emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv1[2] = { 0, 0 };

  y = NULL;
  m0 = emlrtCreateNumericArray(2, iv1, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m0, &u->data[0]);
  emlrtSetDimensions((mxArray *)m0, u->size, 2);
  emlrtAssign(&y, m0);
  return y;
}

static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[2]
{
  real_T (*ret)[2];
  static const int32_T dims[2] = { 1, 2 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  ret = (real_T (*)[2])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  void Duffing_api(const mxArray * const prhs[8], int32_T nlhs, const mxArray
                   *plhs[5])
{
  emxArray_real_T *Q;
  emxArray_real_T *P;
  emxArray_creal_T *Proj_observable;
  emxArray_real_T *omega;
  emxArray_real_T *Energydensity;
  real_T alpha;
  real_T beta;
  real_T tau;
  real_T spacing;
  real_T (*Proj_range)[2];
  real_T domain_range;
  real_T spect_res;
  real_T spect_bandwith;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_real_T(&st, &Q, 2, &mb_emlrtRTEI, true);
  emxInit_real_T(&st, &P, 2, &mb_emlrtRTEI, true);
  emxInit_creal_T(&st, &Proj_observable, 2, &mb_emlrtRTEI, true);
  emxInit_real_T(&st, &omega, 1, &mb_emlrtRTEI, true);
  emxInit_real_T(&st, &Energydensity, 1, &mb_emlrtRTEI, true);

  /* Marshall function inputs */
  alpha = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "alpha");
  beta = emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "beta");
  tau = emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "tau");
  spacing = emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "spacing");
  Proj_range = c_emlrt_marshallIn(&st, emlrtAlias(prhs[4]), "Proj_range");
  domain_range = emlrt_marshallIn(&st, emlrtAliasP(prhs[5]), "domain_range");
  spect_res = emlrt_marshallIn(&st, emlrtAliasP(prhs[6]), "spect_res");
  spect_bandwith = emlrt_marshallIn(&st, emlrtAliasP(prhs[7]), "spect_bandwith");

  /* Invoke the target function */
  Duffing(&st, alpha, beta, tau, spacing, *Proj_range, domain_range, spect_res,
          spect_bandwith, Q, P, Proj_observable, omega, Energydensity);

  /* Marshall function outputs */
  Q->canFreeData = false;
  plhs[0] = emlrt_marshallOut(Q);
  emxFree_real_T(&Q);
  if (nlhs > 1) {
    P->canFreeData = false;
    plhs[1] = emlrt_marshallOut(P);
  }

  emxFree_real_T(&P);
  if (nlhs > 2) {
    plhs[2] = b_emlrt_marshallOut(&st, Proj_observable);
  }

  emxFree_creal_T(&Proj_observable);
  if (nlhs > 3) {
    omega->canFreeData = false;
    plhs[3] = c_emlrt_marshallOut(omega);
  }

  emxFree_real_T(&omega);
  if (nlhs > 4) {
    Energydensity->canFreeData = false;
    plhs[4] = c_emlrt_marshallOut(Energydensity);
  }

  emxFree_real_T(&Energydensity);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

/* End of code generation (_coder_Duffing_api.c) */
