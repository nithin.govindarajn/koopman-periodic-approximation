/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Duffing_terminate.h
 *
 * Code generation for function 'Duffing_terminate'
 *
 */

#ifndef DUFFING_TERMINATE_H
#define DUFFING_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Duffing_types.h"

/* Function Declarations */
extern void Duffing_atexit(void);
extern void Duffing_terminate(void);

#endif

/* End of code generation (Duffing_terminate.h) */
