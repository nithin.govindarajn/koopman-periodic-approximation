/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fft.c
 *
 * Code generation for function 'fft'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "fft.h"
#include "FFTWApi.h"
#include "Duffing_data.h"

/* Variable Definitions */
static emlrtRSInfo x_emlrtRSI = { 8,   /* lineNo */
  "fft",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\datafun\\fft.m"/* pathName */
};

/* Function Definitions */
void fft(const emlrtStack *sp, const emxArray_creal_T *x, emxArray_creal_T *y)
{
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &x_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &y_emlrtRSI;
  FFTWApi_fft1d(&b_st, x, x->size[0], y);
}

/* End of code generation (fft.c) */
