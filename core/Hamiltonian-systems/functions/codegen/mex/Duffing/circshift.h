/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * circshift.h
 *
 * Code generation for function 'circshift'
 *
 */

#ifndef CIRCSHIFT_H
#define CIRCSHIFT_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Duffing_types.h"

/* Function Declarations */
extern void circshift(const emlrtStack *sp, emxArray_creal_T *a, real_T p);

#endif

/* End of code generation (circshift.h) */
