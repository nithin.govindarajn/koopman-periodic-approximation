/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * FFTWApi.c
 *
 * Code generation for function 'FFTWApi'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Duffing.h"
#include "FFTWApi.h"
#include "Duffing_emxutil.h"
#include "emlrt.h"

/* Variable Definitions */
static emlrtRSInfo cb_emlrtRSI = { 31, /* lineNo */
  "FFTWApi/fft1d",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+fftw\\FFTWApi.m"/* pathName */
};

static emlrtRTEInfo ab_emlrtRTEI = { 21,/* lineNo */
  17,                                  /* colNo */
  "MATLABFFTWCallback",                /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+fftw\\MATLABFFTWCallback.m"/* pName */
};

static emlrtRTEInfo bb_emlrtRTEI = { 31,/* lineNo */
  17,                                  /* colNo */
  "FFTWApi",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\+fftw\\FFTWApi.m"/* pName */
};

/* Function Definitions */
void FFTWApi_fft1d(const emlrtStack *sp, const emxArray_creal_T *data, int32_T
                   fftlen, emxArray_creal_T *y)
{
  int32_T i4;
  int32_T loop_ub;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &cb_emlrtRSI;
  emlrtFFTWSetNumThreads(8);
  i4 = y->size[0];
  y->size[0] = fftlen;
  emxEnsureCapacity_creal_T(&st, y, i4, &ab_emlrtRTEI);
  if (fftlen > data->size[0]) {
    loop_ub = y->size[0];
    i4 = y->size[0];
    y->size[0] = loop_ub;
    emxEnsureCapacity_creal_T(&st, y, i4, &bb_emlrtRTEI);
    for (i4 = 0; i4 < loop_ub; i4++) {
      y->data[i4].re = 0.0;
      y->data[i4].im = 0.0;
    }
  }

  emlrtFFTW_1D_C2C((real_T *)&data->data[0], (real_T *)&y->data[0], 1, fftlen,
                   data->size[0], 1, -1);
}

void b_FFTWApi_fft1d(const emlrtStack *sp, const emxArray_creal_T *data, int32_T
                     fftlen, emxArray_creal_T *y)
{
  int32_T i5;
  int32_T loop_ub;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &cb_emlrtRSI;
  emlrtFFTWSetNumThreads(8);
  i5 = y->size[0];
  y->size[0] = fftlen;
  emxEnsureCapacity_creal_T(&st, y, i5, &ab_emlrtRTEI);
  if (fftlen > data->size[0]) {
    loop_ub = y->size[0];
    i5 = y->size[0];
    y->size[0] = loop_ub;
    emxEnsureCapacity_creal_T(&st, y, i5, &bb_emlrtRTEI);
    for (i5 = 0; i5 < loop_ub; i5++) {
      y->data[i5].re = 0.0;
      y->data[i5].im = 0.0;
    }
  }

  emlrtFFTW_1D_C2C((real_T *)&data->data[0], (real_T *)&y->data[0], 1, fftlen,
                   data->size[0], 1, 1);
}

/* End of code generation (FFTWApi.c) */
