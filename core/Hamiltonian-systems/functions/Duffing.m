function [ Q,P, Proj_observable, omega, Energydensity ] = Duffing(alpha,beta,tau, spacing,...
    Proj_range, domain_range, spect_res, spect_bandwith)
%#codegen





%%%% Set up grid %%%%%
translate = round(domain_range/spacing +1);

qdirection = -domain_range:spacing:domain_range;
pdirection = -domain_range:spacing:domain_range;
[Q, P] = meshgrid(qdirection,pdirection); 
n = size(Q,1);
no_gridpoints = n^2;


Pnat = round(round(P/spacing)+translate); Qnat = round(round(Q/spacing)+translate);


%%%% Define spectral variables %%%%
Proj_observable = complex(zeros(n,n),zeros(n,n));
range_circle = tau*Proj_range;
no_ofevaluations = floor(1.5*((spect_bandwith))/spect_res);
omega = (0:(spect_bandwith)/no_ofevaluations:spect_bandwith)';
Energydensity = zeros(size(omega));
 
%%%% allocate memory %%%%
traj_trace = zeros(no_gridpoints,2);
trace_insidedomain = zeros(no_gridpoints,1);
observ_trace = complex(zeros(no_gridpoints,1),zeros(no_gridpoints,1));
c = complex(zeros(no_gridpoints,1),zeros(no_gridpoints,1));

Flagged = zeros(n,n);
for l= 1:length(Q(:))
    if ~Flagged(Qnat(l),Pnat(l))
        
        % Simulate trajectory until it hits starting point
        qnat = Qnat(l);
        pnat = Pnat(l);

        m = 1;
        for k= 1:length(Q(:))
            
            if qnat>0 && qnat<=n && pnat>0 && pnat<=n
                Flagged(qnat,pnat) = 1;
                trace_insidedomain(m) =k;
                m = m+1;
            end
            
           
            % update trajectory trace
               traj_trace(k,:) = [qnat, pnat];
            
            % Propagate forward with periodic approximation
            [qnat,pnat] = periodic_apprx(alpha,beta,qnat,pnat,translate, tau, spacing);
            
           
            % update trace
            observ_trace(k) = g((qnat-translate)*spacing, (pnat-translate)*spacing);
            
            if pnat == Pnat(l) && qnat == Qnat(l)
              
                %%% Cycle has completed: perform spectral calculations %%%
                
                % map cycle into frequency domain
                c(1:k) = fft(observ_trace(1:k),[],1);
                
                % update spectral projection plot
                Proj_observable = update_SpectralProj(Proj_observable, c(1:k), traj_trace(1:k,:), range_circle, trace_insidedomain(1:(m-1)),n);
                
                % update spectral density plot
                [Energydensity] = update_spectraldensity(Energydensity, c(1:k), k, omega, spect_res, tau);
                
                
                break;
            end
        end
    end
end


end

%%%%%%  Observable  %%%%%%
function out = g(q,p)
%#codegen

out = p; % + 0.25*q^4;
end

%%%%%%  Periodic approximation  functions  %%%%%%
function [qnat_update,pnat_update] = periodic_apprx(alpha,beta,qnat,pnat,translate, tau, spacing)
%#codegen
    
    pnat = round(pnat - round((tau*(beta*((qnat-translate)*spacing) + ...
        alpha* ((qnat-translate)*spacing).^3))/spacing));
    qnat = round(qnat + round(tau*((pnat-translate)*spacing)/spacing));
    
    qnat_update =  qnat;
    pnat_update =  pnat;
    
end


%%%%%%%% Spectral calculation functions %%%%%%%%%


%%%% Spectral Projection

function [ d ] = multD_proj( c, range)
%#codegen


n = length(c);
k= (1:n)';
theta_k  = 2*pi*(k-1)/n;

theta_left  =  range(1);
theta_right =  range(2);

if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected  =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected  =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end
c(not(selected)) = 0;

d = c;

end

function [Proj_observable] = update_SpectralProj(Proj_observable, c, traj_trace, range_circle, trace_insidedomain,n)
%#codegen


     %%% compute projection on trajectory %%%
     
     Proj_traj = ifft(multD_proj( c, range_circle),[],1);
     
     
     
     %%% update entries %%%
     
     %find those entries which lie inside
     linindex = sub2ind( [n,n], traj_trace(trace_insidedomain,2), traj_trace(trace_insidedomain,1));
     
     %update entries
     Proj_observable( linindex )  =  Proj_traj(trace_insidedomain);

     
     
end


%%%%% Spectral density

function [omega_l] = findomega(l, cyclelength, tau)
%#codegen

omega_l = (-pi + (2*pi/cyclelength)*(l-1) + mod(cyclelength,2) * pi/cyclelength)/tau;     
     
end

function [l] = findl(omega, cyclelength, tau)
%#codegen

l = 1 + cyclelength/(2*pi) * (omega*tau + pi -  mod(cyclelength,2) * pi/cyclelength);  

end

function [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau)
%#codegen


l_left = ceil(findl(omega - spect_res,cyclelength, tau));
l_right = floor(findl(omega + spect_res,cyclelength, tau));

if l_left>cyclelength || l_right<1
    l_left = -1;
    l_right = -1;
elseif l_left<1
    l_left = 1;
elseif l_right>cyclelength
    l_right = cyclelength;
end

end

function [out] = convolvewithbump(c,omega, spect_res, cyclelength, tau)
%#codegen


    [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau);

    if l_left < 0
        out = 0;
    else

        eig_omega = findomega(l_left:l_right, cyclelength, tau);
        dist_norm = abs(eig_omega-omega(1))/spect_res;
        output =  (2.2523*exp(-1./(1-(dist_norm).^2))/spect_res)';

        out = dot( output , c(l_left:l_right) );
    end

end

function [Energydensity] = update_spectraldensity(Energydensity, c, cyclelength, omega, spect_res, tau)
%#codegen


% shift all entries so that range is from [-pi,pi) (or equivalently [-pi*tau,pi*tau)) instead of [0,2pi)
c = circshift(c,floor(length(c)/2));
% normalize and square the entries
c = (abs(c/sqrt(cyclelength))).^2;

% update entries by excecuting convolution
for l=1:length(omega)
    Energydensity(l) = Energydensity(l) + convolvewithbump(c,omega(l), spect_res, cyclelength, tau);
end


     
     
end



