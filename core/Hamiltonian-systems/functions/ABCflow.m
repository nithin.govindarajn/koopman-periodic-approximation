function [ X,Y,Z, Proj_observable, omega, Energydensity] = ABCflow(A,B,C,tau, n,...
    Proj_range, spect_res, spect_bandwith)
%#codegen



%%%% Set up grid %%%%
[Xnat, Ynat,Znat] = meshgrid(1:n,1:n,1:n); 
X = (Xnat-1)*(1/n); Y = (Ynat-1)*(1/n); Z = (Znat-1)*(1/n);
no_gridpoints = n^3;

%%%% Define spectral variables %%%%
Proj_observable = complex(zeros(n,n,n),zeros(n,n,n));
range_circle = tau*Proj_range;
no_ofevaluations = floor(1.5*((2*spect_bandwith))/spect_res);
omega = (-spect_bandwith:(2*spect_bandwith)/no_ofevaluations:spect_bandwith)';
Energydensity = zeros(size(omega));
 
%%%% allocate memory %%%%
traj_trace = zeros(no_gridpoints,3);
trace_insidedomain = zeros(no_gridpoints,1);
observ_trace = complex(zeros(no_gridpoints,1),zeros(no_gridpoints,1));
c = complex(zeros(no_gridpoints,1),zeros(no_gridpoints,1));

Flagged = zeros(n,n,n);
for l= 1:length(X(:))
    if ~Flagged(Xnat(l),Ynat(l),Znat(l))
        
        
        % Simulate trajectory until it hits starting point
        xnat = Xnat(l);
        ynat = Ynat(l);
        znat = Znat(l);
        
        for k= 1:length(X(:))
            
            % register visited trajectories
            Flagged(xnat,ynat,znat) = 1;
           
            % update trajectory trace
            traj_trace(k,:) = [xnat, ynat, znat];
            
            % Propagate forward with periodic approximation
            [xnat,ynat,znat] = periodic_apprx(A,B,C,xnat,ynat,znat, tau, n);
            
            % update trace
            observ_trace(k) = g(X(xnat,ynat,znat), Y(xnat,ynat,znat), Z(xnat,ynat,znat));
            
            if xnat == Xnat(l) && ynat == Ynat(l) && znat == Znat(l)
              
                %%% Cycle has completed: perform spectral calculations %%%
                
                % map cycle into frequency domain
                c(1:k) = fft(observ_trace(1:k),[],1);
                
                % update spectral projection plot
                Proj_observable = update_SpectralProj(Proj_observable, c(1:k), traj_trace(1:k,:), range_circle, n);
                
                % update spectral density plot
                [Energydensity] = update_spectraldensity(Energydensity, c(1:k), k, omega, spect_res, tau);
                
                
                break;
            end
        end
    end
end


end

%%%%%%  Observable  %%%%%%
function out = g(x,y,z)
%#codegen

out = cos(2*pi*y)+ sin(4*pi*z);
end

%%%%%%  Periodic approximation  functions  %%%%%%
function [xnat_update,ynat_update,znat_update] = periodic_apprx(A,B,C,xnat,ynat,znat, tau, n)
%#codegen
    
    % put everything to 0 zero for convenience
    xnat = xnat-1; ynat= ynat-1; znat = znat-1;
    
    % first shear
    xnat = mod(xnat + round((tau*(A*sin(2*pi*znat/n) + C*cos(2*pi*ynat/n)))*n), n);
    %ynat = ynat;
    %znat = znat;
    
    
    % second shear
    %xnat = xnat;
    ynat = mod(ynat + round((tau*(B*sin(2*pi*xnat/n) + A*cos(2*pi*znat/n)))*n), n); 
    %znat = znat;
    
    % third shear
    %xnat = xnat;
    %ynat = ynat;
    znat = mod(znat + round((tau*(C*sin(2*pi*ynat/n) + B*cos(2*pi*xnat/n)))*n), n);
    
    % put everything to 1 zero for convenience
    xnat = xnat+1; ynat= ynat+1; znat = znat+1;
    
    xnat_update =  xnat;
    ynat_update =  ynat;
    znat_update =  znat;
    
end


%%%%%%%% Spectral calculation functions %%%%%%%%%


%%%% Spectral Projection

function [ d ] = multD_proj( c, range)
%#codegen


n = length(c);
k= (1:n)';
theta_k  = 2*pi*(k-1)/n;

theta_left  =  range(1);
theta_right =  range(2);

if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected  =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected  =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end
c(not(selected)) = 0;

d = c;

end

function [Proj_observable] = update_SpectralProj(Proj_observable, c, traj_trace, range_circle, n)
%#codegen


     %%% compute projection on trajectory %%%
     
     Proj_traj = ifft(multD_proj( c, range_circle),[],1);
     
     
     
     %%% update entries %%%
     
     %find those entries which lie inside
     linindex = sub2ind( [n,n,n], traj_trace(:,2), traj_trace(:,1), traj_trace(:,3));
     
     %update entries
     Proj_observable( linindex )  =  Proj_traj;

     
     
end


%%%%% Spectral density

function [omega_l] = findomega(l, cyclelength, tau)
%#codegen

omega_l = (-pi + (2*pi/cyclelength)*(l-1) + mod(cyclelength,2) * pi/cyclelength)/tau;     
     
end

function [l] = findl(omega, cyclelength, tau)
%#codegen

l = 1 + cyclelength/(2*pi) * (omega*tau + pi -  mod(cyclelength,2) * pi/cyclelength);  

end

function [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau)
%#codegen


l_left = ceil(findl(omega - spect_res,cyclelength, tau));
l_right = floor(findl(omega + spect_res,cyclelength, tau));

if l_left>cyclelength || l_right<1
    l_left = -1;
    l_right = -1;
elseif l_left<1
    l_left = 1;
elseif l_right>cyclelength
    l_right = cyclelength;
end

end

function [out] = convolvewithbump(c,omega, spect_res, cyclelength, tau)
%#codegen


    [l_left, l_right] = findlrange(omega, spect_res, cyclelength, tau);

    if l_left < 0
        out = 0;
    else

        eig_omega = findomega(l_left:l_right, cyclelength, tau);
        dist_norm = abs(eig_omega-omega(1))/spect_res;
        output =  (2.2523*exp(-1./(1-(dist_norm).^2))/spect_res)';

        out = dot( output , c(l_left:l_right) );
    end

end

function [Energydensity] = update_spectraldensity(Energydensity, c, cyclelength, omega, spect_res, tau)
%#codegen


% shift all entries so that range is from [-pi,pi) (or equivalently [-pi*tau,pi*tau)) instead of [0,2pi)
c = circshift(c,floor(length(c)/2));
% normalize and square the entries
c = (abs(c/sqrt(cyclelength))).^2;

% update entries by excecuting convolution
for l=1:length(omega)
    Energydensity(l) = Energydensity(l) + convolvewithbump(c,omega(l), spect_res, cyclelength, tau);
end


     
     
end



