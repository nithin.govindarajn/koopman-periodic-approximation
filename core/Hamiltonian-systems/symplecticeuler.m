function [ Xout ] = symplecticeuler(Xin,t,dt)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global A epsilon Omega


x = Xin(:,1);
y = Xin(:,2);




%%%% update x %%%%%

% initialize with explicit euler..
f1 =4*epsilon*sin(2*pi*Omega*t).* x.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*x;
f2 =4*epsilon*sin(2*pi*Omega*t).* y.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*y;
df1 = 8*epsilon*sin(2*pi*Omega*t).*x + 2*(1-2*epsilon*sin(2*pi*Omega*t));
df2 = 8*epsilon*sin(2*pi*Omega*t).*y + 2*(1-2*epsilon*sin(2*pi*Omega*t));

xout = x - dt*pi*A*sin(pi*f1).*cos(pi*f2).*df2;


%newton iterations
for l=1:10
    f1 =4*epsilon*sin(2*pi*Omega*t).* xout.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*xout;
    %f2 =4*epsilon*sin(2*pi*Omega*t).* y.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*y;
    df1 = 8*epsilon*sin(2*pi*Omega*t).*xout + 2*(1-2*epsilon*sin(2*pi*Omega*t));
    %df2 = 8*epsilon*sin(2*pi*Omega*t).*y + 2*(1-2*epsilon*sin(2*pi*Omega*t));
    
    %newton update
    h = x - xout - dt*pi*A*sin(pi*f1).*cos(pi*f2).*df2;
    dh = -1-dt*pi^2*A*cos(pi*f1).*cos(pi*f2).*df2.*df1;
    %xstore = xout;  
    xout = xout - h ./ dh;
    %max(abs(xstore-xout))
    
end



%%%% update y %%%%%

 f1 =4*epsilon*sin(2*pi*Omega*t).* xout.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*xout;
 %f2 =4*epsilon*sin(2*pi*Omega*t).* y.^2 + 2*(1-2*epsilon*sin(2*pi*Omega*t)).*y;
 df1 = 8*epsilon*sin(2*pi*Omega*t).*xout + 2*(1-2*epsilon*sin(2*pi*Omega*t));
 %df2 = 8*epsilon*sin(2*pi*Omega*t).*y + 2*(1-2*epsilon*sin(2*pi*Omega*t));

yout = y + dt*pi*A*cos(pi*f1).*sin(pi*f2).*df1;


Xout = [xout yout];




% % initialize with explicit euler for x..
% xout = x + dt*2*pi*A*sin(2*pi*x).*cos(2*pi*y);
% 
% %newton iterations
% for l=1:20
%     xout = xout - (x - xout + 2*pi*A*dt*sin(2*pi*xout).*cos(2*pi*y) ) ./ ...
%                      (-1 + (2*pi)^2*dt*A*cos(2*pi*xout).*cos(2*pi*y) ); 
% end
% 
% % update y
% yout = y - dt*2*pi*A*cos(2*pi*xout).* sin(2*pi*y);
%
%
%Xout = [xout yout];

end
