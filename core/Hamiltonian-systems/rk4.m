function [ Xout ] = rk4(Xin,dt)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

x = Xin(:,1);
y = Xin(:,2);
[k1x,k1y] = quadruplegyreODE(x,y);
[k2x,k2y] = quadruplegyreODE(x+k1x*dt/2, y + k1y*dt/2 );
[k3x,k3y] = quadruplegyreODE(x+k2x*dt/2, y + k2y*dt/2);
[k4x,k4y] = quadruplegyreODE(x+k3x*dt, y + k3y*dt);

xout = x + (k1x +2*(k2x+k3x) + k4x)*dt/6;
yout = y + (k1y +2*(k2y+k3y) + k4y)*dt/6;



Xout = [xout yout];
end

