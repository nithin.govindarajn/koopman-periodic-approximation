function [ Xout ] = expliciteuler(Xin,dt)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

x = Xin(:,1);
y = Xin(:,2);

[xdot,ydot] = quadruplegyreODE(x,y);

xout = x + xdot*dt;
yout = y + ydot*dt;

Xout = [xout yout];
end

