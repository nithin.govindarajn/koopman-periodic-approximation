function [ x,t ] = quadruplegyreflowapprx(x,t, dt,noiter)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


timestep = dt/noiter;
for k=1:noiter
    [ x,~] = rk4(x,t+(k-1)*timestep,timestep);
end
t = t +dt;
