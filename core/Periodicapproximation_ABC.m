function [ DiscreteMap] = Periodicapproximation_ABC(A,B,C)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

global  tau N 

%%%%  STEP 1: Construct the discrete map %%%%


% Form grid, evaluate map, project onto grid
[Xnat, Ynat,Znat] = meshgrid(1:N,1:N,1:N); 
X = (Xnat-1)*(1/N); Y = (Ynat-1)*(1/N); Z = (Znat-1)*(1/N);
x = [X(:),Y(:),Z(:)];


% ABCmap
 Xnat = mod((Xnat-1) + round((tau*(A*sin(2*pi*(Znat-1)/N) + C*cos(2*pi*(Ynat-1)/N)))*N), N)+1;
 Ynat = mod((Ynat-1) + round((tau*(B*sin(2*pi*(Xnat-1)/N) + A*cos(2*pi*(Znat-1)/N)))*N), N)+1; 
 Znat = mod((Znat-1) + round((tau*(C*sin(2*pi*(Ynat-1)/N) + B*cos(2*pi*(Xnat-1)/N)))*N), N)+1;
    
 Un = sub2ind( [N,N,N],Ynat(:),Xnat(:),Znat(:));

tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = x;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));


end

