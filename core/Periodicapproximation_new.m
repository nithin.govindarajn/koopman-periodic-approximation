function [ DiscreteMap] = Periodicapproximation_new()
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

global T  N dimension

%%%%  STEP 1: Construct the discrete map %%%%


% Form grid, evaluate map, project onto grid
x = FormGRID(); graphsize = length(x);
Tx = (mod(T(x)+1/(2*N),1)*N);
% Describe the grid on which the discrete map is defined
IDX_f = floor(Tx); IDX_f(IDX_f == N+1) = 1; IDX_f(IDX_f == 0)   = N;
IDX_c = ceil(Tx); IDX_c(IDX_c == N+1) = 1; IDX_c(IDX_c == 0)   = N;

Un = []; dilfact = 1;
while isempty(Un)
  
    % Append edge list
    tic
    neigbors = [-dilfact:-1, 1:dilfact];
    list = permn(neigbors,dimension);
    edges = get_edges(IDX_f, IDX_c, list, dimension, N,graphsize );   
         
    disp('Time needed to construct list of edges:')
    toc
              
    % Solve the bipartite graph problem to form the discrete Koopman operator
    tic
    [ Un ] = bipartitematch(edges, graphsize);
    disp('Time needed to solve max-flow problem:')
    toc
    
    if isempty(Un)        
        disp('Dilate bipartite graph')
        dilfact = dilfact+1;
    end

end



tic
%%%% STEP 2: Compute the cycle decomposition %%%%
[ cycledecomp, inverse_cycledecomp, cycle_lengths ] = fun_cycledecomp_mex(Un);
disp('Time needed to compute cycle decomposition:')
toc


% The discrete map
DiscreteMap.x = x;
DiscreteMap.Un = Un;
DiscreteMap.cycledecomp = cycledecomp;
DiscreteMap.inverse_cycledecomp = transpose(inverse_cycledecomp);
DiscreteMap.cycle_lengths = cycle_lengths;
DiscreteMap.Periodicity   = 0;%lcms(unique(DiscreteMap.cycle_lengths));


end

