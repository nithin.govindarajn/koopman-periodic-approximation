function [ sumofeigenvectors ] = func_sumofeigenvectors( theta_k, v_k, c_k, range  )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here


theta_left = range(1);
theta_right = range(2);

if theta_left < 0
    selected  =    or( theta_k >= mod(theta_left, 2*pi) , theta_k <= theta_right);
elseif  theta_right > 2*pi
    selected =    or( theta_k >= theta_left ,  theta_k <= mod(theta_right,2*pi) );
else
    selected =    and( theta_k >= theta_left ,  theta_k <=  theta_right );
end

% all other cases

selected_c_k = c_k(selected);
selected_v_k = cell2mat(v_k(selected));

if ~isempty(selected_c_k)
sumofeigenvectors = sum(bsxfun(@times, selected_v_k, selected_c_k),2);
else
    sumofeigenvectors = zeros(size(v_k{1}));
end

end

