function [ IDX ] = findnearestneighbors( xinv , dilfact )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global dimension N


switch dimension
    case 1
        IDX = [floor(xinv), ceil(xinv)];
        IDX(IDX == N+1) = 1;
        IDX(IDX == 0)   = N;
        IDX = IDX';
    case 2
        IDX = [floor(xinv(:,1)), ceil(xinv(:,1)), floor(xinv(:,2)), ceil(xinv(:,2)) ];
        IDX(IDX == N+1) = 1;
        IDX(IDX == 0)   = N;
        IDX = [ (IDX(:,1) -1)*N + IDX(:,3) ,  (IDX(:,1) -1)*N + IDX(:,4) , (IDX(:,2) -1)*N + IDX(:,3)   , (IDX(:,2) -1)*N + IDX(:,4)  ];              
        IDX = IDX';
    case 3
        IDX = [floor(xinv(:,1)), ceil(xinv(:,1)), floor(xinv(:,2)), ceil(xinv(:,2)), floor(xinv(:,3)), ceil(xinv(:,3)) ];
        IDX(IDX == N+1) = 1;
        IDX(IDX == 0)   = N;
        IDX = [ (IDX(:,5) -1)*N^2 + (IDX(:,1) -1)*N + IDX(:,3)   ,  (IDX(:,5) -1)*N^2 + (IDX(:,2) -1)*N + IDX(:,3) , (IDX(:,6) -1)*N^2 + (IDX(:,1) -1)*N + IDX(:,3) , (IDX(:,6) -1)*N^2 + (IDX(:,2) -1)*N + IDX(:,3)...
                (IDX(:,5) -1)*N^2 + (IDX(:,1) -1)*N + IDX(:,4)   ,  (IDX(:,5) -1)*N^2 + (IDX(:,2) -1)*N + IDX(:,4) , (IDX(:,6) -1)*N^2 + (IDX(:,1) -1)*N + IDX(:,4) , (IDX(:,6) -1)*N^2 + (IDX(:,2) -1)*N + IDX(:,4) ];              
        IDX = IDX';
        
       
end




end

