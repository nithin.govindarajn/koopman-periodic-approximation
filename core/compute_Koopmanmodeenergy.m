function [EnergyDensity, theta, ranges ] = compute_Koopmanmodeenergy( resolution, g, DiscreteMap )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here


x = DiscreteMap.x;
cycledecomp = DiscreteMap.cycledecomp;
cycle_lengths = DiscreteMap.cycle_lengths;


% discrete representation of the observable
gdiscrete = g(x);

% Denote the eigendecomposition by  Un = V D V^(-1), define c = V^(-1) * gdiscrete
test =  mat2cell(gdiscrete(cycledecomp)*(1/sqrt(length(gdiscrete))), cycle_lengths,1); 
c = cellfun(@(x)(fft(x)/sqrt(length(x))), test, 'UniformOutput', 0); c = cell2mat(c);

% Set the resolution 
k = (1:resolution)';
ranges = [ 2*pi*( (k-1)/resolution - 1/(2*resolution)) , 2*pi*( (k-1)/resolution + 1/(2*resolution) )];
theta = 2*pi*( (k-1)/resolution ); 
EnergyDensity = (resolution/(2*pi))*func_computeEnergyDensity_mex(c, ranges, resolution,cycle_lengths );

end


