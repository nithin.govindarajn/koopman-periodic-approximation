function [ T, dimension ] = anzaitypemap(alpha)
%UNTITLED2 Summary of this function goes here
%  Pick alpha irrational

T = @(x)(  [ mod(x(:,1) + alpha , 1)  ,...
             mod( x(:,1) + x(:,2), 1)] );

dimension = 2;
end
