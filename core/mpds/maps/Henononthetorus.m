function [ T,  dimension ] = Henononthetorus(K)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  [ mod( x(:,1) + K*( x(:,2).^2 - x(:,2) + 1/6 ),1) , ...
             mod(x(:,2) + mod( x(:,1) + K*( x(:,2).^2 - x(:,2) + 1/6 ),1)- 0.5, 1)   ] );

  dimension = 2;
  
end

