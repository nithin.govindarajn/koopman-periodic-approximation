function [ T,  dimension ] = SO2map(theta)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  [ mod( cos(theta)*x(:,1) -sin(theta)*x(:,2) ,1) ,  ...
             mod( sin(theta)*x(:,1) + cos(theta)*x(:,2) , 1)  ] );

dimension = 2;
         
end
