function [ T, Tinv, dimension ] = simplemap(K)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  [ x(:,1) - K*x(:,2).*(1-x(:,2)), ...
             mod( x(:,2) + K*x(:,1) - K*x(:,2).*(1-x(:,2)) , 1)    ] );
% Pre-image
Tinv = @(x)( {inversemap(x,K)});

dimension = 2;
  
end


function xinv = inversemap(x,K)

x2inv = mod(x(:,2)-K*x(:,1),1);
x1inv = mod(x(:,1)+K*x2inv.*(1-x2inv),1);

xinv = [x1inv,x2inv];
end
