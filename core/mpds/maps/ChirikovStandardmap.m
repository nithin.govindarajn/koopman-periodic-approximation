function [ T, dimension ] = ChirikovStandardmap(K)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  [ mod(x(:,1) + mod( x(:,2) + K*sin(2*pi*x(:,1)),1) , 1)  ,...
             mod( x(:,2) + K*sin(2*pi*x(:,1)),1)] );

dimension = 2;
end
