function [ T, Tinv, dimension ] = ABCmap(A,B,C)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)( forwardmap(x,A,B,C) );

dimension = 3;
  
end



function Xnext = forwardmap(X,A,B,C)

A = A/(2*pi);
B = B/(2*pi);
C = C/(2*pi);

x = X(:,1);
y = X(:,2);
z = X(:,3);

xplus = mod(x + A*sin(2*pi*z)     +  C*cos(2*pi*y),1);
yplus = mod(y + B*sin(2*pi*xplus) +  A*cos(2*pi*z),1); 
zplus = mod(z + C*sin(2*pi*yplus) +  B*cos(2*pi*xplus),1);


Xnext = [xplus, yplus, zplus];

end
