function [ T ] = Integrablemapon3sphere(  omega1, omega2 )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  shift(omega1, omega2,x)  );
         
         
end




function [ X_prime ] = shift(omega1, omega2,X)


[theta1,r1] = cart2pol(X(:,1),X(:,2));
[theta2,r2] = cart2pol(X(:,3),X(:,4));

X_prime = [r1 .* cos(theta1+omega1), r1.* sin(theta1+omega1)  , r2 .* cos(theta2+omega2), r2.* sin(theta2+omega2)];

end

