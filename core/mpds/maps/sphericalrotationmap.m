function [ T ] = sphericalrotationmap( alpha,beta, omega )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  phi_inv( alpha,beta, shift(omega, phi( alpha,beta,x)) )  );
         
         
end



function [ X_prime ] = phi(alpha,beta,X)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

X = transpose(X);

A1 = [1 ,  0, 0;
       0, cos(beta), -sin(beta);
       0, sin(beta), cos(beta) ];
   
A2 =  [cos(alpha), -sin(alpha), 0;
       sin(alpha), cos(alpha),  0;
       0, 0, 1];
   
X_prime = A1 * A2 * X;
X_prime = transpose(X_prime);



end

function [ X_prime ] = shift(omega,X)


[theta,~] = cart2pol(X(:,1),X(:,2));

X_prime = [sqrt(1-X(:,3).^2) .* cos(theta+omega), sqrt(1-X(:,3).^2) .* sin(theta+omega)  , X(:,3)];

end


function [ X_prime ] = phi_inv(alpha,beta,X)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

X = transpose(X);

A1 = [1 ,  0, 0;
       0, cos(-beta), -sin(-beta);
       0, sin(-beta), cos(-beta) ];

A2 =  [cos(-alpha), -sin(-alpha), 0;
       sin(-alpha), cos(-alpha),  0;
       0, 0, 1];
   
  
X_prime = A2 * A1 * X;
X_prime = transpose(X_prime);

end