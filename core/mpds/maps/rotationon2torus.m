function [ T, dimension ] = rotationon2torus(omega1, omega2)
%UNTITLED2 Summary of this function goes here
%  Pick alpha irrational

T = @(x)(  [ mod(x(:,1) + omega1 , 1)  ,...
             mod( x(:,2) + omega2, 1)] );

dimension = 2;
end
