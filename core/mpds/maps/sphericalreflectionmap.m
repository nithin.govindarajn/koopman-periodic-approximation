function [ T ] = sphericalreflectionmap( alpha,beta)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  phi_inv( alpha,beta, reflect( phi( alpha,beta,x)) )  );
         
         
end



function [ X_prime ] = phi(alpha,beta,X)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

X = transpose(X);

A1 = [1 ,  0, 0;
       0, cos(beta), -sin(beta);
       0, sin(beta), cos(beta) ];
   
A2 =  [cos(alpha), -sin(alpha), 0;
       sin(alpha), cos(alpha),  0;
       0, 0, 1];
   
X_prime = A1 * A2 * X;
X_prime = transpose(X_prime);



end

function [ X_prime ] = reflect(X)



X_prime = [X(:,1),X(:,2) , -X(:,3)];

end


function [ X_prime ] = phi_inv(alpha,beta,X)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

X = transpose(X);

A1 = [1 ,  0, 0;
       0, cos(-beta), -sin(-beta);
       0, sin(-beta), cos(-beta) ];

A2 =  [cos(-alpha), -sin(-alpha), 0;
       sin(-alpha), cos(-alpha),  0;
       0, 0, 1];
   
  
X_prime = A2 * A1 * X;
X_prime = transpose(X_prime);

end