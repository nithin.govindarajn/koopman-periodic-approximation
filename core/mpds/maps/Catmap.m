function [ T,  dimension ] = Catmap()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

T = @(x)(  [ mod( 2*x(:,1) + x(:,2) ,1) ,  ...
             mod( x(:,1) + x(:,2) , 1)  ] );

dimension = 2;
         
end
