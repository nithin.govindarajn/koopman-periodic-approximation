function [ T, dimension ] = anzaiflow(dt,gamma)
%UNTITLED2 Summary of this function goes here
%  Pick alpha irrational

T = @(x)(  integratef(x,dt,gamma) );


dimension = 2;
end


function [ xplus ] = integratef(x,dt,gamma)


xplus = mod(x + f(x,gamma)*dt,1);


end


function [ xdot ] = f(x,gamma)


xdot = [ones(size(x(:,1)))*gamma, sin(2*pi*x(:,1))];

end
