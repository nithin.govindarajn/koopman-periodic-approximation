function [ T, dimension ] = flowon2torus(dt,omega1, omega2)
%UNTITLED2 Summary of this function goes here
%  Pick alpha irrational

T = @(x)(  integratef(x,dt,omega1,omega2) );


dimension = 2;
end


function [ xplus ] = integratef(x,dt,omega1,omega2)


xplus = mod(x + f(x,omega1,omega2)*dt,1);


end


function [ xdot ] = f(x,omega1,omega2)


xdot = [ones(size(x(:,1)))*omega1, ones(size(x(:,2)))*omega2];

end