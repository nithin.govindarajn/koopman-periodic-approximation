function [ edges] = formBIPARTITEGRPH(Tx,graphsize, dilfact, edges)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
global dimension


switch dimension
    case 1 
        
        if     dilfact == 0
                % Every inverse point should have 2 neighbors
                dumm = bsxfun(@times, [1:graphsize]', ones(1,2))';
        elseif dilfact == 1
                % Every inverse point should have 2 neighbors
                %dumm = bsxfun(@times, [1:graphsize]', ones(1,2))';
        else
                % not yet supported...
        end
        
    case 2
        
        if     dilfact == 0
                % Every inverse point should have 4 neighbors
                dumm = bsxfun(@times, [1:graphsize]', ones(1,4))';
        elseif dilfact == 1
                % Every inverse point should have 12 neighbors
                %dumm = bsxfun(@times, [1:graphsize]', ones(1,12))';
        else
                % not yet supported...
        end
        
        
    case 3
        
        if     dilfact == 0
                % Every inverse point should have 8 neighbors
                dumm = bsxfun(@times, [1:graphsize]', ones(1,8))';
        elseif dilfact == 1
                % Every inverse point should have 12 neighbors
                %dumm = bsxfun(@times, [1:graphsize]', ones(1,12))';
        else
                % not yet supported...
        end
        
end



IDX = findnearestneighbors( Tx , dilfact);


% update list of edges
edges = [edges;
         [IDX(:) dumm(:)]];



end

