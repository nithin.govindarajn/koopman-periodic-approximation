function [ ] = plotProjection(  x,Phi)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
global dimension N


figure('position',[100 100 850 600]);
switch dimension
    case 1
        
        plot(x,Phi)
        xlabel('x_1'); ylabel('\Phi(x)')
    case 2
        
        surf(reshape(x(:,1),[N,N]), reshape(x(:,2),[N,N]), reshape( real(Phi), [N,N])) ;
        shading interp
        hold off;
        xlabel('x_1'); ylabel('x_2')
        view(0,90)
        colorbar
        
    case 3
        
        vis3d(reshape(real(Phi),[N,N,N]))


end


end

