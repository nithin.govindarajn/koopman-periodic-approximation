function [ g ] = sphericalharmonic_obs( l, m, x  )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here


out = transpose(cart2polar2(transpose(x)));

phi = out(:,1);
theta = out(:,2);

size(theta)
size(phi)


g = real(transpose(compute_Ylm(l,m,transpose(theta),transpose(phi))));   % only consider the real par
end

