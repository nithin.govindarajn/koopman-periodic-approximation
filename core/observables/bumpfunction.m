function [ g ] = bumpfunction( x , center, R )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here


% relative location from center
for k =1 : size(x,2)
 x(:,k) = x(:,k) - center(k);
end

%compute distance from center
d = zeros(size(x,1),1);
for k =1 : size(x,2)
    temp =  min([x(:,k), 1-x(:,k)],[],2);
 d = d + temp.^2;
end
d = sqrt(d);
dnorm = d/R;

% Bumpfunction
g = zeros(size(x,1),1);
g(dnorm<1) = exp(-1./(1-dnorm(dnorm<1)));

end

