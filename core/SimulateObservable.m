function [  ] = SimulateObservable( DiscreteMap, g, no_iter, pausetime )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Un = DiscreteMap.Un;
x = DiscreteMap.x;

global T dimension N
% discrete representation of the observable
gdiscrete = g(x);


figure('position',[100 100 850 600]);



switch dimension
    
    case 1

        x_sim = x;
        for i = 1:no_iter

          subplot(2,1,1)
          plot(x, g(x_sim),'b') 
          xlabel('x');ylabel('g(x)');
          hold on;
          plot(x, gdiscrete,'r')
          xlabel('x');ylabel('g(x)');
          title('Simulation of observable')
          hold off;
          subplot(2,1,2)
          plot(x, abs(gdiscrete-g(x_sim)),'r')
          xlabel('x');ylabel('error');
          title('absolute error')
          axis([0 1 0 0.5])

          pause(pausetime)


          % 
          gdiscrete = PropagateObservable( Un, gdiscrete );
          x_sim = T(x_sim);

        end

    case 2
        
        x_sim = x;
        subplot(2,2,1)
        s = surf(reshape(x(:,1),[N N]), reshape(x(:,2),[N N]), zeros(N,N)) ;
        shading interp
        xlabel('x_1'); ylabel('x_2');
        title('Simulation of observable ("True map")')
        hold off;
        view(0,90)
        subplot(2,2,2)
        h = surf(reshape(x(:,1),[N N]), reshape(x(:,2),[N N]), zeros(N,N)) ;
        shading interp
        xlabel('x_1'); ylabel('x_2');
        title('Simulation of observable ("Discrete map")')
        hold off;
        view(0,90)
        subplot(2,4,6:7)
        m = surf(reshape(x(:,1),[N N]), reshape(x(:,2),[N N]), zeros(N,N)) ;
        shading interp
        xlabel('x_1'); ylabel('x_2');
        
        hold off;
        view(0,90)
        colorbar
        caxis([0 1])
        for ii = 1:no_iter

            
          %update plot
          set(s,'ZData', reshape( g(x_sim), [N,N]))
          set(h,'ZData', reshape( gdiscrete, [N,N]))
          set(m,'ZData', reshape( abs(g(x_sim)- gdiscrete), [N,N]))
          drawnow
          
          
          title(['Error at k=', num2str(ii)])
   
          % Propogate forward
          gdiscrete = PropagateObservable( Un, gdiscrete );
   
          x_sim = T(x_sim);

          pause(pausetime)
          
        end
       
      case 3
          
        x1 = reshape( x(:,1), [N,N,N]);
        x2 = reshape( x(:,2), [N,N,N]);
        x3 = reshape( x(:,3), [N,N,N]);
        
        cmap = colormap; cmap = cmap(1:4:end,:); 
        no_isosurfaces = length(cmap);
        maxdatavalue = max(gdiscrete);
        mindatavalue = min(gdiscrete);
        datarange = mindatavalue:(maxdatavalue-mindatavalue)/no_isosurfaces:maxdatavalue;
        
        subplot(1,2,1)
        daspect([1 1 1]);
        xlabel('x_1');ylabel('x_2');zlabel('x_3')
        view(3); axis tight; grid on;
        title('Simulation of observable ("True map")')
        subplot(1,2,2)
        daspect([1 1 1]);
        xlabel('x_1');ylabel('x_2');zlabel('x_3')
        view(3); axis tight; grid on;
        title('Simulation of observable ("Discrete map")')
        
        x_sim = x;
        
        for ii = 1:no_iter;
            v = reshape( g(x_sim), [N,N,N]);
            subplot(1,2,1)
            for k = 1:no_isosurfaces
                p = patch(isosurface(x1,x2,x3,v,datarange(k)));
                set(p,'facecolor',cmap(k,:),'edgecolor','none');
                alpha(.2)
            end
            subplot(1,2,2)
            for k = 1:no_isosurfaces
                q = patch(isosurface(x1,x2,x3,reshape(gdiscrete,[N,N,N]),datarange(k)));
                set(q,'facecolor',cmap(k,:),'edgecolor','none');
                alpha(.2)
            end
            title(['Simulation of observable ("Discrete map") at k=', num2str(ii)])
            pause(pausetime)
            if ii ~= no_iter
                subplot(1,2,1)
                cla(gca)
                subplot(1,2,2)
                cla(gca)
            end
            x_sim = T(x_sim);
            gdiscrete = PropagateObservable( Un, gdiscrete );
        end
        
 
  
        
        
end

