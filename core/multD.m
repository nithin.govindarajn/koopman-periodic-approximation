function [ d ] = multD( c)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n = length(c);
k= (1:n)';

d= (exp(2*pi*1i*(k-1)/n)) .* c;

end

