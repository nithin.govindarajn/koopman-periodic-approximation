function [ Un ] = bipartitematch( edges, n )
%BIPARTITEMATCH - solves the unweighted bipartite graph problem
%   If the graph satisfies hall's condition the matching will be perfect.
%   In that case, it returns:
%     - the discrete Koopman operator as a sparse matrix


%%% MAX FLOW METHOD %%%%
edge_source = edges(:,1);
edge_sink = edges(:,2);
B = sparse(edge_source, edge_sink, ones(length(edge_source),1) , n, n, length(edge_source)); 
B = B ~= 0;   % convert to logical matrix
[ii, jj, bij] = find(B);


ii = ii + n+1;
jj = jj + 1;

C = sparse(2:n+1,ones(n,1),ones(n,1),2*n+2,2*n+2);
A = sparse(ii,jj,bij,2*n+2,2*n+2) + C + transpose(rot90(C,2));
        


[maxflow, ~, ~, flowmatrix] = max_flow(A,2*n+2,1);




if maxflow == n
    temp = flowmatrix((n+2):(2*n+1) ,2:(n+1));
    [Un,~,~] = find(temp);
else
    disp('Hall condition not satisfied, matching is not perfect')
    Un = [];
end








end

