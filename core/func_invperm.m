function [ inv_perm ] = func_invperm( perm )
%#codegen

inv_perm = zeros(size(perm));
inv_perm(perm) = (1:length(perm));
inv_perm = inv_perm';

end

